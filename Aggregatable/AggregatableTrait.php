<?php

namespace ThreeWebOneEntityBundle\Aggregatable;

trait AggregatableTrait
{
    /**
     * @param string $modifier
     * @return \DateTime
     */
    private function getModifiedDate(string $modifier)
    {
        $date = new \DateTime();

        return $date->modify($modifier);
    }
}

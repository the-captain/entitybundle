<?php

namespace ThreeWebOneEntityBundle\Sonata\Admin;

use Doctrine\ORM\QueryBuilder;

/**
 * Interface AdminQuery.
 */
interface AdminQueryInterface
{
    const PARAM_ADMIN_USER_ID = 'paramAdminUserId';
    const PARAM_OWNER_USER_ID = 'paramOwnerUserId';
    const PARAM_PARENT_CATEGORY_ID = 'paramParentCategoryId';
    const PARAM_SITES = 'paramSites';
    const PARAM_SITES_PRICE = 'paramSitesPrice';

    /**
     * Returns sonata admin list query.
     *
     * @param array $params
     *
     * @return QueryBuilder
     */
    public function getAdminQuery(array $params = []): QueryBuilder;

    /**
     * Returns list of required params for query.
     *
     * @return array
     */
    public function getParamsList(): array;
}

3web1 Entity Bundle
=========

Entity Bundle is the core bundle of AdminApp and SaleSites applications.
It is located in separate repository and loaded through composer in `vendors` folder.

Deployment and Update after major changes:
=========

# Don't forget to update Bundle's version after PR merge
    `git pull origin master` (if you merged your latest changes to master)
    `git tag 1.5.9` (choose new version you need depending on major, minor and patch version)
    `git push origin 1.5.9`

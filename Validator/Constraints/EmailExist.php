<?php

namespace ThreeWebOneEntityBundle\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailExist extends Constraint
{
    /**
     * @var string
     */
    public $message = 'fos_user.email.already_used';

    /**
     * @inheritdoc
     */
    public function validatedBy()
    {
        return 'email_exist_validator';
    }
}

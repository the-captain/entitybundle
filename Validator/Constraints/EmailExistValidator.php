<?php

namespace ThreeWebOneEntityBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use SaleSitesBundle\EventListener\ClientSubscriber;

class EmailExistValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ClientSubscriber
     */
    private $client;

    /**
     * EmailExistValidator constructor.
     *
     * @param EntityManager $entityManager
     * @param ClientSubscriber $client
     */
    public function __construct(EntityManager $entityManager, ClientSubscriber $client)
    {
        $this->entityManager = $entityManager;
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        $customerRepo = $this->entityManager->getRepository(Customer::class);
        $customer = $customerRepo->getCustomerByEmailAndOwnerId($value, $this->client->getUserId());

        if (!is_null($customer)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}

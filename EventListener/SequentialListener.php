<?php

namespace ThreeWebOneEntityBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Order\Order;

class SequentialListener
{
    const CONNECTOR = [
        Order::class => 1,
        InventoryItem::class => 2,
        Customer::class => 3
    ];

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!$object instanceof SequentialInterface || $object->getStoreId()) {
            return;
        }

        $connection = $args->getObjectManager()->getConnection();
        $type = self::CONNECTOR[get_class($object)];
        $ownerId = $object->getOwner()->getId();
        if ($type && $ownerId) {
            $sql = sprintf("SELECT * FROM sequential_next_val WHERE owner_id = %s AND type = %s", $ownerId, $type);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll()[0] ?? null;
            $nextId = 1;
            if ($result) {
                $nextId = (int)$result['next_val'] + 1;
                $sql = sprintf("update sequential_next_val set next_val=%s where id=%s", $nextId, $result['id']);
                $connection->prepare($sql)->execute();
            } else {
                $sql=sprintf("insert into sequential_next_val(owner_id,next_val,type) values(%s,%s,%s)", $ownerId, $nextId, $type);
                $connection->prepare($sql)->execute();
            }

            $object->setStoreId($nextId);
        }
    }
}

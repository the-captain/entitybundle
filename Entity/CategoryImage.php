<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class CategoryImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class CategoryImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="category_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Category", inversedBy="images")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set Category
     *
     * @param null $entity
     *
     * @return CategoryImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof Category)) {
            throw new EntityNotFoundException('Entity should be instance of Category class');
        }
        $this->entity = $entity;

        return $this;
    }
}

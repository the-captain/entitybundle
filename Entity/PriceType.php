<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class PriceType
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="price_types", indexes={@ORM\Index(name="search_idx", columns={"site", "owner_id"})})
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\PriceTypeRepository")
 */
class PriceType extends BaseEntity implements
    StatusInterface,
    SiteTypeInterface,
    OwnerInterface,
    SynchronizeableInterface
{
    use StatusTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="site", type="smallint")
     */
    protected $site;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="priceTypes")
     * @ORM\JoinTable(name="user_price_types")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Price", mappedBy="priceType", cascade={"persist", "remove"})
     */
    private $prices;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userPriceTypes")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="priceType")
     */
    protected $inventoryItems;

    /**
     * Image constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PriceType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return PriceType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set site
     *
     * @param integer $site
     *
     * @return PriceType
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return integer
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $siteName = '';

        if ($this->getSite() == self::SALE) {
            $siteName = 'Sell ';
        }
        if ($this->getSite() == self::BUYBACK) {
            $siteName = 'BuyBack ';
        }
        if ($this->getSite() == self::REPAIR) {
            $siteName = 'Repair ';
        }

        return 'PriceType '.$siteName.$this->getTitle();
    }

    /**
     * Add user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return PriceType
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    /**
     * Add price
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $price
     *
     * @return PriceType
     */
    public function addPrice(Price $price)
    {
        $this->prices[] = $price;
        $price->setPriceType($this);

        return $this;
    }

    /**
     * Remove price
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $price
     */
    public function removePrice(Price $price)
    {
        $this->prices->removeElement($price);
        $price->setPriceType(null);
    }

    /**
     * Get prices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user= null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * PriceType clone
     */
    public function __clone()
    {
        parent::__clone();
        $this->prices = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }
}

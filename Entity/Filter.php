<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\CloneableInterface;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Filter
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="filters")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\FilterRepository")
 */
class Filter extends BaseEntity implements StatusInterface, OwnerInterface, SynchronizeableInterface
{
    use StatusTrait;

    /**
     * Undefined Type
     */
    const TYPE_0 = 0;

    /**
     * Type Capacity
     */
    const TYPE_CAPACITY = 1;
    const TYPE_CAPACITY_NAME = 'capacity';

    /**
     * Type Carrier
     */
    const TYPE_PROVIDER = 2;
    const TYPE_PROVIDER_NAME = 'carrier';

    /**
     * Type Color
     */
    const TYPE_COLOR = 3;
    const TYPE_COLOR_NAME = 'color';

    /**
     * Type brand
     */
    const TYPE_BRAND = 4;
    const TYPE_BRAND_NAME = 'brand';

    /**
     * Type accessory
     */
    const TYPE_ACCESSORY = 5;
    const TYPE_ACCESSORY_NAME = 'accessory';

    const TYPES = [
        self::TYPE_CAPACITY => self::TYPE_CAPACITY_NAME,
        self::TYPE_PROVIDER => self::TYPE_PROVIDER_NAME,
        self::TYPE_COLOR => self::TYPE_COLOR_NAME,
        self::TYPE_BRAND => self::TYPE_BRAND_NAME,
        self::TYPE_ACCESSORY => self::TYPE_ACCESSORY_NAME,
    ];

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $title;

    /**
     * @ORM\OneToMany(targetEntity="Filter", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Filter", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="filters")
     * @ORM\JoinTable(name="filter_inventory_items")
     */
    protected $inventoryItems;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Model", mappedBy="filters")
     * @ORM\JoinTable(name="filter_models")
     */
    protected $models;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="filters")
     * @ORM\JoinTable(name="user_filters")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\FilterImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userFilters")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var int
     *
     * @ORM\Column(name="isMultiSelect", type="smallint", nullable=true)
     */
    protected $isMultiSelect;

    /**
     * Filter constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    /**
     * Gets Image
     *
     * @return Image | false
     */
    public function getImage()
    {
        if (!empty($this->getImages())) {
            return $this->getImages()[0];
        }

        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Filter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add child
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $child
     *
     * @return Filter
     */
    public function addChild(Filter $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $child
     */
    public function removeChild(Filter $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $parent
     *
     * @return Filter
     */
    public function setParent(Filter $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \ThreeWebOneEntityBundle\Entity\Filter
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add inventory item
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     *
     * @return Filter
     */
    public function addInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->add($inventoryItem);

        return $this;
    }

    /**
     * Remove inventory item
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     */
    public function removeInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->removeElement($inventoryItem);
    }

    /**
     * Get inventory items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Add model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     *
     * @return Filter
     */
    public function addModel(Model $model)
    {
        $this->models[] = $model;

        return $this;
    }

    /**
     * Remove model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     */
    public function removeModel(Model $model)
    {
        $this->models->removeElement($model);
    }

    /**
     * Get models
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: 'Filter';
    }

    /**
     * Add user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return Filter
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add image
     *
     * @param \ThreeWebOneEntityBundle\Entity\FilterImage $image
     *
     * @return Filter
     */
    public function addImage(FilterImage $image)
    {
        $this->images->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove image
     *
     * @param \ThreeWebOneEntityBundle\Entity\FilterImage $image
     */
    public function removeImage(FilterImage $image)
    {
        $this->images->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return int
     */
    public function getIsMultiSelect()
    {
        return (bool) $this->isMultiSelect;
    }

    /**
     * @param int $isMultiSelect
     *
     * @return Filter
     */
    public function setIsMultiSelect($isMultiSelect)
    {
        $this->isMultiSelect = $isMultiSelect;

        return $this;
    }

    /**
     * Filter clone
     */
    public function __clone()
    {
        parent::__clone();
        $this->images = new ArrayCollection();
        $this->setParent(null);
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }
}

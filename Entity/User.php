<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;
use ThreeWebOneEntityBundle\Entity\Inventory\Inventory;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\UserConfig\Config;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;
use ThreeWebOneEntityBundle\Entity\UserConfig\Question;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

//TODO: check if available to change this dependency, because this vendor does not have sonata as dependency

/**
 * Class Model
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="fos_user_user",indexes={@ORM\Index(name="storeAddressSearch", columns={"store_address"})})
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("storeName")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Customer
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Customer", mappedBy="user")
     */
    protected $billingCustomer;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Model", mappedBy="users")
     * @ORM\JoinTable(name="user_models")
     */
    protected $models;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Category", mappedBy="users")
     * @ORM\JoinTable(name="user_categories")
     */
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Filter", mappedBy="users")
     * @ORM\JoinTable(name="user_filters")
     */
    protected $filters;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Image", mappedBy="users")
     * @ORM\JoinTable(name="user_images")
     */
    protected $images;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Price", mappedBy="users")
     * @ORM\JoinTable(name="user_prices")
     */
    protected $prices;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\PriceType", mappedBy="users")
     * @ORM\JoinTable(name="user_price_types")
     */
    protected $priceTypes;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\Inventory", mappedBy="owner",
     *                                                                                   cascade={"persist"})
     */
    protected $inventories;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Model", mappedBy="owner", cascade={"persist"})
     */
    protected $userModels;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\PopularSearch", mappedBy="owner", cascade={"persist"})
     */
    protected $popularSearches;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Category", mappedBy="owner", cascade={"persist"})
     */
    protected $userCategories;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Question", mappedBy="owner",
     *                                                                                   cascade={"persist"})
     */
    protected $userQuestions;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Filter", mappedBy="owner", cascade={"persist"})
     */
    protected $userFilters;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Accessory", mappedBy="owner", cascade={"persist"})
     */
    protected $userAccessories;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Image", mappedBy="owner", cascade={"persist"})
     */
    protected $userImages;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Price", mappedBy="owner", cascade={"persist"})
     */
    protected $userPrices;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\PriceType", mappedBy="owner", cascade={"persist"})
     */
    protected $userPriceTypes;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Order\Order", mappedBy="owner", cascade={"persist"})
     */
    protected $userOrders;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\PaymentConfig", mappedBy="owner", cascade={"persist"})
     */
    protected $userPaymentConfigs;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial", mappedBy="owner",
     *                                                                                      cascade={"persist"})
     */
    protected $testimonials;

    /**
     * User config
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", mappedBy="owner")
     */
    protected $config;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Seo", mappedBy="owner",
     *                                                                              cascade={"persist"})
     */
    protected $seoMetas;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Customer\Customer", mappedBy="owner",
     *                                                                                 cascade={"persist"})
     */
    protected $customers;

    /**
     * @var string $storeName
     *
     * @ORM\Column(name="store_name", type="string")
     */
    protected $storeName;

    /**
     * @var string $storeAddress
     *
     * @Assert\Regex(
     *     pattern="/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/",
     *     message="Please provide valid host name."
     * )
     * @ORM\Column(name="store_address", type="string", nullable=true)
     */
    protected $storeAddress;

    /**
     * @var bool
     *
     * @ORM\Column(name="store_has_own_address", type="boolean", nullable=false)
     */
    protected $storeHasOwnAddress = false;

    /**
     * @var Zone
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Domain\Zone", mappedBy="owner", cascade={"persist"})
     */
    protected $domainZone;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="owner",
     *                                                                                       cascade={"persist"})
     */
    protected $inventoryItems;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Question", mappedBy="users")
     * @ORM\JoinTable(name="user_questions")
     */
    protected $questions;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Page\Page", mappedBy="owner")
     */
    protected $pages;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="supervisor")
     */
    private $subs;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Location", mappedBy="owner")
     */
    private $locations;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="subs")
     * @ORM\JoinColumn(name="supervisor_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $supervisor;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Domain\ClientDomain", mappedBy="owner")
     */
    private $clientDomain;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->models = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->priceTypes = new ArrayCollection();
        $this->inventories = new ArrayCollection();
        $this->userModels = new ArrayCollection();
        $this->userCategories = new ArrayCollection();
        $this->userFilters = new ArrayCollection();
        $this->userImages = new ArrayCollection();
        $this->userPrices = new ArrayCollection();
        $this->userPriceTypes = new ArrayCollection();
        $this->userOrders = new ArrayCollection();
        $this->testimonials = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->userQuestions = new ArrayCollection();
        $this->seoMetas = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->subs = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->userAccessories = new ArrayCollection();
        $this->clientDomain = new ArrayCollection();
        $this->userPaymentConfigs = new ArrayCollection();
        $this->popularSearches = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getStoreAddress(): ?string
    {
        return $this->storeAddress;
    }

    /**
     * @return bool
     */
    public function hasStoreAddress(): bool
    {
        return !is_null($this->storeAddress);
    }

    /**
     * @param string|null $storeAddress
     *
     * @return User
     */
    public function setStoreAddress(string $storeAddress = null): User
    {
        $this->storeAddress = $storeAddress;

        return $this;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add Model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     *
     * @return $this
     */
    public function addModel(Model $model)
    {
        $this->models->add($model);

        return $this;
    }

    /**
     * Remove Model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     */
    public function removeModel(Model $model)
    {
        $this->models->removeElement($model);
    }

    /**
     * Get Models
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Add Filter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $filter
     *
     * @return $this
     */
    public function addFilter(Filter $filter)
    {
        $this->filters->add($filter);

        return $this;
    }

    /**
     * Remove Filter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $filter
     */
    public function removeFilter(Filter $filter)
    {
        $this->filters->removeElement($filter);
    }

    /**
     * Get Filters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Add Category
     *
     * @param \ThreeWebOneEntityBundle\Entity\Category $category
     *
     * @return $this
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);

        return $this;
    }

    /**
     * Remove Category
     *
     * @param \ThreeWebOneEntityBundle\Entity\Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get Categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add Price
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $price
     *
     * @return $this
     */
    public function addPrice(Price $price)
    {
        $this->prices->add($price);

        return $this;
    }

    /**
     * Remove Price
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $price
     */
    public function removePrice(Price $price)
    {
        $this->prices->removeElement($price);
    }

    /**
     * Get Prices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Add Image
     *
     * @param \ThreeWebOneEntityBundle\Entity\Image $image
     *
     * @return $this
     */
    public function addImage(Image $image)
    {
        $this->images->add($image);

        return $this;
    }

    /**
     * Remove Image
     *
     * @param \ThreeWebOneEntityBundle\Entity\Image $image
     */
    public function removeImage(Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get Images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add PriceType
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $priceType
     *
     * @return $this
     */
    public function addPriceType(PriceType $priceType)
    {
        $this->priceTypes->add($priceType);

        return $this;
    }

    /**
     * Remove PriceType
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $priceType
     */
    public function removePriceType(PriceType $priceType)
    {
        $this->priceTypes->removeElement($priceType);
    }

    /**
     * Get PriceTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceTypes()
    {
        return $this->priceTypes;
    }

    /**
     * Add inventory
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\Inventory $inventory
     *
     * @return User
     */
    public function addInventory(Inventory $inventory)
    {
        $this->inventories->add($inventory);
        $inventory->setOwner($this);

        return $this;
    }

    /**
     * Remove inventory
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\Inventory $inventory
     */
    public function removeInventory(Inventory $inventory)
    {
        $this->inventories->removeElement($inventory);
        $inventory->setOwner(null);
    }

    /**
     * Get inventories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventories()
    {
        return $this->inventories;
    }

    /**
     * Add userModel
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $userModel
     *
     * @return User
     */
    public function addUserModel(Model $userModel)
    {
        $this->userModels[] = $userModel;
        $userModel->setOwner($this);

        return $this;
    }

    /**
     * Remove userModel
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $userModel
     */
    public function removeUserModel(Model $userModel)
    {
        $this->userModels->removeElement($userModel);
        $userModel->setOwner(null);
    }

    /**
     * Get userModels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserModels()
    {
        return $this->userModels;
    }

    /**
     * Add userCategory
     *
     * @param \ThreeWebOneEntityBundle\Entity\Category $userCategory
     *
     * @return User
     */
    public function addUserCategory(Category $userCategory)
    {
        $this->userCategories[] = $userCategory;
        $userCategory->setOwner($this);

        return $this;
    }

    /**
     * Remove userCategory
     *
     * @param \ThreeWebOneEntityBundle\Entity\Category $userCategory
     */
    public function removeUserCategory(Category $userCategory)
    {
        $this->userCategories->removeElement($userCategory);
        $userCategory->setOwner(null);
    }

    /**
     * Get userCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCategories()
    {
        return $this->userCategories;
    }

    /**
     * Add userFilter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $userFilter
     *
     * @return User
     */
    public function addUserFilter(Filter $userFilter)
    {
        $this->userFilters[] = $userFilter;
        $userFilter->setOwner($this);

        return $this;
    }

    /**
     * Remove userFilter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $userFilter
     */
    public function removeUserFilter(Filter $userFilter)
    {
        $this->userFilters->removeElement($userFilter);
        $userFilter->setOwner(null);
    }

    /**
     * Get userFilters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserFilters()
    {
        return $this->userFilters;
    }

    /**
     * Add userImage
     *
     * @param \ThreeWebOneEntityBundle\Entity\Image $userImage
     *
     * @return User
     */
    public function addUserImage(Image $userImage)
    {
        $this->userImages[] = $userImage;
        $userImage->setOwner($this);

        return $this;
    }

    /**
     * Remove userImage
     *
     * @param \ThreeWebOneEntityBundle\Entity\Image $userImage
     */
    public function removeUserImage(Image $userImage)
    {
        $this->userImages->removeElement($userImage);
        $userImage->setOwner(null);
    }

    /**
     * Get userImages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserImages()
    {
        return $this->userImages;
    }

    /**
     * Add userPrice
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $userPrice
     *
     * @return User
     */
    public function addUserPrice(Price $userPrice)
    {
        $this->userPrices[] = $userPrice;
        $userPrice->setOwner($this);

        return $this;
    }

    /**
     * Remove userPrice
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $userPrice
     */
    public function removeUserPrice(Price $userPrice)
    {
        $this->userPrices->removeElement($userPrice);
        $userPrice->setOwner(null);
    }

    /**
     * Get userPrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPrices()
    {
        return $this->userPrices;
    }

    /**
     * Add userPriceType
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $userPriceType
     *
     * @return User
     */
    public function addUserPriceType(PriceType $userPriceType)
    {
        $this->userPriceTypes[] = $userPriceType;
        $userPriceType->setOwner($this);

        return $this;
    }

    /**
     * Remove userPriceType
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $userPriceType
     */
    public function removeUserPriceType(PriceType $userPriceType)
    {
        $this->userPriceTypes->removeElement($userPriceType);
        $userPriceType->setOwner(null);
    }

    /**
     * Get userPriceTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPriceTypes()
    {
        return $this->userPriceTypes;
    }

    /**
     * Add userOrder
     *
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $userOrder
     *
     * @return User
     */
    public function addUserOrder(Order $userOrder)
    {
        $this->userOrders[] = $userOrder;
        $userOrder->setOwner($this);

        return $this;
    }

    /**
     * Remove userOrder
     *
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $userOrder
     */
    public function removeUserOrder(Order $userOrder)
    {
        $this->userOrders->removeElement($userOrder);
        $userOrder->setOwner(null);
    }

    /**
     * Get userOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserOrders()
    {
        return $this->userOrders;
    }

    /**
     * Add Testimonial
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial $testimonial
     *
     * @return User
     */
    public function addTestimonial(Testimonial $testimonial)
    {
        $this->testimonials[] = $testimonial;
        $testimonial->setOwner($this);

        return $this;
    }

    /**
     * Remove Testimonial
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial $testimonial
     */
    public function removeTestimonial(Testimonial $testimonial)
    {
        $this->testimonials->removeElement($testimonial);
        $testimonial->setOwner(null);
    }

    /**
     * Get Testimonials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestimonials()
    {
        return $this->testimonials;
    }

    /**
     * Add Question
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Question $question
     *
     * @return User
     */
    public function addQuestion(Question $question)
    {
        $this->questions[] = $question;
        $question->setOwner($this);

        return $this;
    }

    /**
     * Remove Question
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->questions->removeElement($question);
        $question->setOwner(null);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add location
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Location $location
     *
     * @return User
     */
    public function addLocation(Location $location)
    {
        $this->locations->add($location);
        $location->setOwner($this);

        return $this;
    }

    /**
     * Remove Question
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Location $location
     */
    public function removeLocation(Location $location)
    {
        $this->locations->removeElement($location);
        $location->setOwner(null);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Get config
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set Config
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     *
     * @return $this
     */
    public function setConfig(Config $config = null)
    {
        $this->config = $config;
        $config->setOwner($this);

        return $this;
    }

    /**
     * Add Seo Meta
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Seo $seo
     *
     * @return User
     */
    public function addSeoMetas(Seo $seo)
    {
        $this->seoMetas[] = $seo;
        $seo->setOwner($this);

        return $this;
    }

    /**
     * Remove Seo Meta
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Seo $seo
     */
    public function removeSeoMetas(Seo $seo)
    {
        $this->seoMetas->removeElement($seo);
        $seo->setOwner(null);
    }

    /**
     * Get Seo Meta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeoMetas()
    {
        return $this->seoMetas;
    }

    /**
     * Get IncomeCustomers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Get Seo Meta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Get Seo Meta by site
     *
     * @param string $site
     *
     * @return Seo|null
     */
    public function getSeoMetaBySite(string $site)
    {
        foreach ($this->seoMetas as $meta) {
            if ($meta->getSite() == $site) {
                return $meta;
            }
        }

        return null;
    }

    /**
     * Get Store Name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * Set Store Name
     *
     * @param string $storeName
     *
     * @return $this
     */
    public function setStoreName(string $storeName)
    {
        $this->storeName = $storeName;

        return $this;
    }

    /**
     * Add sub
     *
     * @param User $sub
     *
     * @return User
     */
    public function addSub(User $sub)
    {
        $this->subs[] = $sub;

        return $this;
    }

    /**
     * Remove sub
     *
     * @param User $sub
     */
    public function removeSub(User $sub)
    {
        $this->subs->removeElement($sub);
    }

    /**
     * Get subs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubs()
    {
        return $this->subs;
    }

    /**
     * Get parent
     *
     * @return User
     */
    public function getSupervisor(): ?User
    {
        return $this->supervisor;
    }

    /**
     * Set supervisor
     *
     * @param User $supervisor
     *
     * @return User
     */
    public function setSupervisor(User $supervisor = null): ?User
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Add userQuestion
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Question $userQuestion
     *
     * @return User
     */
    public function addUserQuestion(Question $userQuestion)
    {
        $this->userQuestions[] = $userQuestion;
        $userQuestion->setOwner($this);

        return $this;
    }

    /**
     * Remove userQuestion
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Question $userQuestion
     */
    public function removeUserQuestion(Question $userQuestion)
    {
        $this->userQuestions->removeElement($userQuestion);
        $userQuestion->setOwner(null);
    }

    /**
     * Get userQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserQuestions()
    {
        return $this->userQuestions;
    }

    /**
     * @return Accessory[]
     */
    public function getUserAccessories(): ArrayCollection
    {
        return $this->userAccessories;
    }

    /**
     * @return bool
     */
    public function hasUserAccessories()
    {
        return $this->userAccessories->isEmpty() === false;
    }

    /**
     * @param Accessory $accessory
     *
     * @return User
     */
    public function addUserAccessory($accessory)
    {
        $this->userAccessories->add($accessory);

        return $this;
    }

    /**
     * @return Customer
     */
    public function getBillingCustomer(): Customer
    {
        return $this->billingCustomer;
    }

    /**
     * @return bool
     */
    public function hasBillingCustomer(): bool
    {
        return !empty($this->billingCustomer);
    }

    /**
     * @param Customer $billingCustomer
     *
     * @return User
     */
    public function setBillingCustomer(Customer $billingCustomer): User
    {
        $this->billingCustomer = $billingCustomer;

        return $this;
    }

    /**
     * Add page
     *
     * @param Page $page
     *
     * @return User
     */
    public function addPage(Page $page): self
    {
        $this->pages->add($page);
        $page->setOwner($this);

        return $this;
    }

    /**
     * Remove page
     *
     * @param Page $page
     */
    public function removePage(Page $page)
    {
        $this->pages->removeElement($page);
        $page->setOwner(null);
    }

    /**
     * Get pages
     *
     * @return ArrayCollection
     */
    public function getPages(): ArrayCollection
    {
        return $this->pages;
    }

    /**
     * @param int $type
     *
     * @return null|Page
     */
    public function getPageByType(int $type) {
        /** @var Page $page */
        foreach ($this->pages as $page) {
            if ($page->getPageType() === $type) {

                return $page;
            }
        }

        return null;
    }

    /**
     * @return DomainName|null
     */
    public function getDomainZone(): ?Zone
    {
        return $this->domainZone;
    }

    /**
     * @return bool
     */
    public function hasDomainZone(): bool
    {
        return !is_null($this->domainZone);
    }

    /**
     * @param DomainName|null $domainZone
     *
     * @return User
     */
    public function setDomainZone(Zone $domainZone = null): User
    {
        $this->domainZone = $domainZone;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStoreHasOwnAddress(): bool
    {
        return $this->storeHasOwnAddress;
    }

    /**
     * @param bool $storeHasOwnAddress
     *
     * @return User
     */
    public function setStoreHasOwnAddress(bool $storeHasOwnAddress): User
    {
        $this->storeHasOwnAddress = $storeHasOwnAddress;

        return $this;
    }

    /**
     * @param int $type
     *
     * @return null|PaymentConfig
     */
    public function getPaymentConfigByType(int $type): ?PaymentConfig
    {
        /** @var PaymentConfig $config */
        foreach ($this->userPaymentConfigs as $config) {
            if ($config->getPaymentType() == $type) {
                return $config;
            }
        }
        
        return null;
    }

    public function getClientDomain(): ?ClientDomain
    {
        return $this->clientDomain[0] ?? null;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Domain;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\Index;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;

/**
 * Class Zone
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Domain\ClientDomainQueueRepository")
 * @ORM\Table(name="client_domain_queue")
 * @ORM\Table(name="client_domain_queue", indexes={
 *     @Index(name="domain_name_idx", columns={"domain"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class ClientDomainQueue
{
    /**
     * Domain Ownership Type Constants
     */
    const OWNERSHIP_OPEN_SRS = 1;
    const OWNERSHIP_EXTERNAL_REGISTERER = 2;
    const OWNERSHIP_PENDING = 3;

    use CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/",
     *     message="Please provide valid host name."
     * )
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="domain", type="string", unique=true)
     */
    private $domain;

    /**
     * @ORM\Column(name="is_lock", type="boolean")
     */
    private $isLock = false;

    /**
     * @ORM\Column(name="domain_ownership_type", type="smallint")
     */
    private $domainOwnershipType = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsLock(): bool
    {
        return $this->isLock;
    }

    /**
     * @param $isLock
     * @return $this
     */
    public function setIsLock($isLock)
    {
        $this->isLock = $isLock;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomainOwnershipType(): int
    {
        return $this->domainOwnershipType;
    }

    /**
     * @param $domainOwnershipType
     * @return $this
     */
    public function setDomainOwnershipType($domainOwnershipType)
    {
        $this->domainOwnershipType = $domainOwnershipType;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Domain;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class Zone
 *
 * @ORM\Entity()
 * @ORM\Table(name="domain_zone")
 */
class Zone
{
    /**
     * @var string
     *
     * @ORM\Column(name="distribution_id", type="string")
     */
    private $distributionId;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/",
     *     message="Please provide valid host name."
     * )
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="domain", type="string")
     */
    private $domain;

    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="ns_list", type="json_array")
     */
    private $nsList = [];

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="domainZone", cascade={"persist"})
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="zone_id", type="string")
     */
    private $zoneId;

    /**
     * @return string|null
     */
    public function getDistributionId(): ?string
    {
        return $this->distributionId;
    }

    /**
     * @return bool
     */
    public function hasDistributionId(): bool
    {
        return ! is_null($this->distributionId);
    }

    /**
     * @param string|null $distributionId
     *
     * @return Zone
     */
    public function setDistributionId(string $distributionId = null): Zone
    {
        $this->distributionId = $distributionId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @return bool
     */
    public function hasDomain(): bool
    {
        return ! is_null($this->domain);
    }

    /**
     * @param string|null $domain
     *
     * @return Zone
     */
    public function setDomain(string $domain = null): Zone
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return ! is_null($this->id);
    }

    /**
     * @param int|null $id
     *
     * @return Zone
     */
    public function setId(int $id = null): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getNsList(): ?array
    {
        return $this->nsList;
    }

    /**
     * @return bool
     */
    public function hasNsList(): bool
    {
        return ! is_null($this->nsList);
    }

    /**
     * @param array|null $nsList
     *
     * @return Zone
     */
    public function setNsList(array $nsList = null): Zone
    {
        $this->nsList = $nsList;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @return bool
     */
    public function hasOwner(): bool
    {
        return ! is_null($this->owner);
    }

    /**
     * @param User|null $owner
     *
     * @return Zone
     */
    public function setOwner(User $owner = null): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZoneId(): ?string
    {
        return $this->zoneId;
    }

    /**
     * @return bool
     */
    public function hasZoneId(): bool
    {
        return ! is_null($this->zoneId);
    }

    /**
     * @param string|null $zoneId
     *
     * @return Zone
     */
    public function setZoneId(string $zoneId = null): self
    {
        $zonePrefix = '/hostedzone/';
        if (strpos($zoneId, $zonePrefix) !== false) {
            $zoneId = str_replace($zonePrefix, '', $zoneId);
        }

        $this->zoneId = $zoneId;

        return $this;
    }
}

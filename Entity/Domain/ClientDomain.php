<?php

namespace ThreeWebOneEntityBundle\Entity\Domain;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\Index;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;

/**
 * Client Domain
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Domain\ClientDomainRepository")
 * @UniqueEntity(
 *     fields={"domain", "owner"},
 *     errorPath="domain",
 *     message="This value is already used."
 * )
 * @ORM\Table(name="client_domain", indexes={
        @Index(name="domain_name_idx", columns={"domain"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class ClientDomain
{
    use CreatedAtTrait, UpdatedAtTrait;

    const INACTIVE = 0;
    const ACTIVE = 1;

    const CLIENT_DOMAIN_TYPE = 0;
    const SUB_DOMAIN_TYPE = 1;

    const CLIENT_TLD = '3w1.website';

    const PAYED_WITHOUT_DOMAIN = 1;
    const PAYED_WITH_DOMAIN = 2;
    const ADDED_MANUALLY = 3;
    const ADDED_MANUALLY_APPROVED = 4;
    const DUPLICATE = 5;

    const ACTIVE_LIMIT = 5;

    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="clientDomain", cascade={"persist"})
     * @ORM\JoinColumn(name="owner_id", nullable=false, referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/",
     *     message="Please provide valid host name."
     * )
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="domain", type="string", options={"default" : 1})
     */
    private $domain;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default" : 1})
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="domain_type", type="integer", nullable=false, options={"default" : 0})
     */
    private $domainType;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="integer", nullable=true, options={"default" : 0})
     */
    private $status;

    /**
     * @var array
     *
     * @ORM\Column(name="contacts", type="json_array", nullable=true)
     */
    private $contacts = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|User
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return ClientDomain
     */
    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param $domain
     * @return ClientDomain
     */
    public function setDomain($domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return null|bool
     */
    public function isActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getDomainType(): ?int
    {
        return $this->domainType;
    }

    /**
     * @param bool $domainType
     * @return $this
     */
    public function setDomainType(bool $domainType)
    {
        $this->domainType = $domainType;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return ClientDomain
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    /**
     * @param array $contacts
     * @return ClientDomain
     */
    public function setContacts(array $contacts = null): self
    {
        $this->contacts = $contacts;

        return $this;
    }
}

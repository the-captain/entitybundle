<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AdvertiseCategory.
 *
 *
 * @ORM\Table(name="advertise_category")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\AdvertiseCategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AdvertiseCategory extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Advertise", mappedBy="category")
     */
    private $categoryItems = [];

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = '';

    /**
     * @var int
     *
     * @ORM\Column(name="orderNumber", type="integer", length=5)
     */
    private $orderNumber = 0;

    /**
     * @return string
     */
    public function __toString(): string
    {
        $name = $this->getName();

        return $name ?? 'AdvertiseCategory';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return !empty($this->name);
    }

    /**
     * @param string $name
     *
     * @return AdvertiseCategory
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryItems()
    {
        return $this->categoryItems;
    }

    /**
     * @return bool
     */
    public function hasCategoryItems()
    {
        return !empty($this->categoryItems);
    }

    /**
     * @param mixed $categoryItems
     *
     * @return AdvertiseCategory
     */
    public function setCategoryItems($categoryItems)
    {
        $this->categoryItems = $categoryItems;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return AdvertiseCategory
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderNumber(): int
    {
        return $this->orderNumber;
    }

    /**
     * @return bool
     */
    public function hasOrderNumber(): bool
    {
        return !empty($this->orderNumber);
    }

    /**
     * @param int $orderNumber
     *
     * @return AdvertiseCategory
     */
    public function setOrderNumber(int $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }
}

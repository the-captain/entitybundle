<?php

namespace ThreeWebOneEntityBundle\Entity\Customer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialInterface;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialTrait;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\UniqueConstraint;
use ThreeWebOneEntityBundle\Validator\Constraints as SiteAssert;

/**
 * Class Customer
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="fos_user_customer", indexes={
 *     @Index(name="email_owner_id_idx", columns={"email", "owner_id"})
 *     },
 *     uniqueConstraints={
 *        @UniqueConstraint(name="owner_store_id",
 *            columns={"owner_id", "store_id"})
 *    })
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\CustomerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="emailCanonical",
 *          column=@ORM\Column(
 *              name     = "email_canonical",
 *              type     = "string",
 *              length   = 255,
 *              unique   = false
 *          )
 *      ),
 *      @ORM\AttributeOverride(name="username",
 *          column=@ORM\Column(
 *              name     = "username",
 *              type     = "string",
 *              length   = 255,
 *              unique   = false,
 *              nullable = true
 *          )
 *      ),
 *      @ORM\AttributeOverride(name="usernameCanonical",
 *          column=@ORM\Column(
 *              name     = "username_canonical",
 *              type     = "string",
 *              length   = 255,
 *              unique   = false,
 *              nullable = true
 *          )
 *      )
 * })
 */
class Customer extends BaseUser implements SequentialInterface
{
    use SequentialTrait;

    const ENABLED = 1;
    const DISABLED = 0;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    protected $cart;

    /**
     * @var string
     *
     * @ORM\Column(name="driver_license", type="string", length=255, nullable=true)
     */
    protected $driverLicense;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_1", type="string", length=255, nullable=true)
     */
    protected $addressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_2", type="string", length=255, nullable=true)
     */
    protected $addressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255, nullable=true)
     */
    protected $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    protected $company;

    /**
     * @var string
     *
     * @ORM\Column(name="business_phone", type="string", length=255, nullable=true)
     */
    protected $businessPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    protected $note;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="customers")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Order\Order", mappedBy="customer", cascade={"persist", "remove"})
     */
    protected $orders;

    /**
     * @SiteAssert\EmailExist(groups={"CustomRegistration"})
     */
    protected $email;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param array $cart
     *
     * @return $this
     */
    public function setCart(array $cart)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Add order
     *
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     *
     * @return $this
     */
    public function addOrder(Order $order)
    {
        $this->getOrders()->add($order);
        $order->setUser($this);

        return $this;
    }

    /**
     * Remove order
     *
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     */
    public function removeOrder($order)
    {
        $this->getOrders()->removeElement($order);
        $order->setUser(null);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param string $addressLine1
     *
     * @return Customer
     */
    public function setAddressLine1(string $addressLine1)
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param string $addressLine2
     *
     * @return Customer
     */
    public function setAddressLine2(string $addressLine2)
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return Customer
     */
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return Customer
     */
    public function setState(string $state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return Customer
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return Customer
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function getDriverLicense()
    {
        return $this->driverLicense;
    }

    /**
     * @param string $driverLicense
     *
     * @return Customer
     */
    public function setDriverLicense(string $driverLicense)
    {
        $this->driverLicense = $driverLicense;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     *
     * @return Customer
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessPhone()
    {
        return $this->businessPhone;
    }

    /**
     * @param string $businessPhone
     *
     * @return Customer
     */
    public function setBusinessPhone($businessPhone)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return Customer
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     *
     * @return Customer
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return Order|null
     */
    public function getOrderWithSignatureImage()
    {
        foreach ($this->orders as $order) {
            if ($order->getImageName()) {
                return $order;
            }
        }

        return null;
    }
}

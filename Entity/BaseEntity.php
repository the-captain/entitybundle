<?php

namespace ThreeWebOneEntityBundle\Entity;

use ThreeWebOneEntityBundle\Entity\Helper\CloneableInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class BaseEntity
{
    /**
     * clone
     */
    public function __clone()
    {
        $this->id = null;
        $this->setOwner(null);
        $this->users = new ArrayCollection();
    }
}

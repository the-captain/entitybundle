<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ConfigSliderImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class ConfigSliderImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="config_slider_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider", inversedBy="sliderImages")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Slider settings
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\SliderImageSetting", mappedBy="sliderImage", cascade={"persist"})
     */
    protected $setting;

    /**
     * ConfigSliderImage constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setSetting(new SliderImageSetting());
    }

    /**
     * Set ConfigSlider
     *
     * @param null $entity
     *
     * @return ConfigSliderImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof ConfigSlider)) {
            throw new EntityNotFoundException('Entity should be instance of Config slider class');
        }
        $this->entity = $entity;

        return $this;
    }

    /**
     * Set setting
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\SliderImageSetting $setting
     *
     * @return $this
     */
    public function setSetting(SliderImageSetting $setting = null)
    {
        $this->setting = $setting;
        $setting->setSliderImage($this);

        return $this;
    }

    /**
     * Get setting
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\SliderImageSetting
     */
    public function getSetting()
    {
        return $this->setting;
    }
}

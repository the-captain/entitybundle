<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class DefaultImage
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="default_image")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\DefaultImageRepository")
 */
class DefaultImage
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    protected $content;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo", mappedBy="defaultImage", cascade={"persist"})
     */
    protected $configLogo;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return DefaultImage
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return DefaultImage
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}

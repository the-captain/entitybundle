<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ConfigOfferImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class ConfigOfferImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="config_offer_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", inversedBy="offerImages")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Slider settings
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\OfferImageSetting", mappedBy="offerImage", cascade={"persist"})
     */
    protected $setting;

    /**
     * ConfigOfferImage constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setSetting(new OfferImageSetting());
    }

    /**
     * Set Config
     *
     * @param null $entity
     *
     * @return ConfigOfferImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof Config)) {
            throw new EntityNotFoundException('Entity should be instance of Config class');
        }
        $this->entity = $entity;

        return $this;
    }

    /**
     * Set setting
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\OfferImageSetting $setting
     *
     * @return $this
     */
    public function setSetting(OfferImageSetting $setting = null)
    {
        $this->setting = $setting;
        $setting->setofferImage($this);

        return $this;
    }

    /**
     * Get setting
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\OfferImageSetting
     */
    public function getSetting()
    {
        return $this->setting;
    }
}

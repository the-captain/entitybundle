<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class Testimonial
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="testimonial")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\TestimonialRepository")
 */
class Testimonial implements OwnerInterface
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonial", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $testimonial;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="testimonials")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTestimonial()
    {
        return $this->testimonial;
    }

    /**
     * @param string $testimonial
     *
     * @return Testimonial
     */
    public function setTestimonial(string $testimonial)
    {
        $this->testimonial = $testimonial;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return Testimonial
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return Testimonial
     */
    public function setLocation(string $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return Testimonial
     */
    public function setOwner(User $user= null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }
}

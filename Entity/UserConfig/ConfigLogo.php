<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Image;

/**
 * Class ConfigLogo
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="config_logo")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ConfigLogoRepository")
 */
class ConfigLogo
{
    /**
     * Logo size
     */
    const LARGE = 1;
    const SMALL = 2;
    const MEDIUM = 3;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", inversedBy="configLogo")
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    protected $config;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    private $logoImages;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigFaviconImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    private $faviconImages;

    /**
     * Default Image
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage", inversedBy="configLogo")
     * @ORM\JoinColumn(name="default_image_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $defaultImage;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoDefaultImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    private $renderedDefaultImages;

    /**
     * First color
     *
     * @var string
     *
     * @ORM\Column(name="first_color", type="string", length=6, nullable=true)
     * @Assert\Regex(
     *     pattern="/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/",
     *     match=true,
     *     message="Your property should match standard hex color representation"
     * )
     */
    protected $firstColor;

    /**
     * Second color
     *
     * @var string
     *
     * @ORM\Column(name="second_color", type="string", length=6, nullable=true)
     * @Assert\Regex(
     *     pattern="/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/",
     *     match=true,
     *     message="Your property should match standard hex color representation"
     * )
     */
    protected $secondColor;

    /**
     * Third color
     *
     * @var string
     *
     * @ORM\Column(name="third_color", type="string", length=6, nullable=true)
     * @Assert\Regex(
     *     pattern="/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/",
     *     match=true,
     *     message="Your property should match standard hex color representation"
     * )
     */
    protected $thirdColor;

    /**
     * Is use default image
     *
     * @var boolean
     *
     * @ORM\Column(name="is_use_default_image", type="boolean")
     */
    protected $useDefaultImage;

    /**
     * Is use selected logo as favicon
     *
     * @var boolean
     *
     * @ORM\Column(name="is_use_selected_as_favicon", type="boolean")
     */
    protected $useSelectedAsFavicon;

    /**
     * @var int
     *
     * @ORM\Column(name="logo_size", type="smallint", nullable=true)
     */
    protected $logoSize;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        $this->useSelectedAsFavicon = true;
        $this->useDefaultImage = true;
        $this->logoImages = new ArrayCollection();
        $this->renderedDefaultImages = new ArrayCollection();
        $this->faviconImages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set config
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config = null)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Add logo image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoImage $image
     *
     * @return ConfigLogo
     */
    public function addLogoImage(ConfigLogoImage $image)
    {
        $this->logoImages->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove logo image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoImage $image
     */
    public function removeLogoImage(ConfigLogoImage $image)
    {
        $this->logoImages->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get logo images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogoImages()
    {
        return $this->logoImages;
    }

    /**
     * Add rendered default image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoDefaultImage $image
     *
     * @return ConfigLogo
     */
    public function addRenderedLogoImage(ConfigLogoDefaultImage $image)
    {
        $this->renderedDefaultImages->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove rendered default image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoDefaultImage $image
     */
    public function removeRenderedLogoImage(ConfigLogoDefaultImage $image)
    {
        $this->renderedDefaultImages->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get rendered default image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRenderedLogoImages()
    {
        return $this->renderedDefaultImages;
    }

    /**
     * Add Favicon image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigFaviconImage $image
     *
     * @return ConfigLogo
     */
    public function addFaviconImage(ConfigFaviconImage $image)
    {
        $this->faviconImages->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove Favicon image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigFaviconImage $image
     */
    public function removeFaviconImage(ConfigFaviconImage $image)
    {
        $this->faviconImages->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get Favicon images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFaviconImages()
    {
        return $this->faviconImages;
    }

    /**
     * Set Default Image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage
     *
     * @return $this
     */
    public function setDefaultImage(DefaultImage $defaultImage = null)
    {
        $this->defaultImage = $defaultImage;

        return $this;
    }

    /**
     * Get Default Image
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage
     */
    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * Get is use default image
     *
     * @return bool
     */
    public function isUseDefaultImage(): bool
    {
        return $this->useDefaultImage;
    }

    /**
     * Set is use default image
     *
     * @param bool $useDefaultImage
     *
     * @return ConfigLogo
     */
    public function setUseDefaultImage(bool $useDefaultImage)
    {
        $this->useDefaultImage = $useDefaultImage;

        return $this;
    }

    /**
     * Get is use selected as favicon
     *
     * @return bool
     */
    public function isUseSelectedAsFavicon(): bool
    {
        return $this->useSelectedAsFavicon;
    }

    /**
     * Set is use selected as favicon
     *
     * @param bool $useSelectedAsFavicon
     *
     * @return ConfigLogo
     */
    public function setUseSelectedAsFavicon(bool $useSelectedAsFavicon)
    {
        $this->useSelectedAsFavicon = $useSelectedAsFavicon;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstColor()
    {
        return $this->firstColor;
    }

    /**
     * @param string $firstColor
     *
     * @return ConfigLogo
     */
    public function setFirstColor(string $firstColor)
    {
        $this->firstColor = $firstColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecondColor()
    {
        return $this->secondColor;
    }

    /**
     * @param string $secondColor
     *
     * @return ConfigLogo
     */
    public function setSecondColor(string $secondColor)
    {
        $this->secondColor = $secondColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getThirdColor()
    {
        return $this->thirdColor;
    }

    /**
     * @param string $thirdColor
     *
     * @return ConfigLogo
     */
    public function setThirdColor(string $thirdColor)
    {
        $this->thirdColor = $thirdColor;

        return $this;
    }

    /**
     * Gets Favicon Image
     *
     * @return Image | false
     */
    public function getFaviconImage()
    {
        if (!empty($this->getFaviconImages())) {
            return $this->getFaviconImages()[0];
        }

        return false;
    }

    /**
     * Gets Logo Image
     *
     * @return Image | false
     */
    public function getLogoImage()
    {
        if (!empty($this->getLogoImages())) {
            return $this->getLogoImages()[0];
        }

        return false;
    }

    /**
     * Gets Rendered Logo Image
     *
     * @return Image | false
     */
    public function getRenderedLogoImage()
    {
        if (!empty($this->getRenderedLogoImages())) {
            return $this->getRenderedLogoImages()[0];
        }

        return false;
    }

    /**
     * @return int
     */
    public function getLogoSize(): ?int
    {
        return $this->logoSize;
    }

    /**
     * @param int $logoSize
     * @return ConfigLogo
     */
    public function setLogoSize(?int $logoSize)
    {
        $this->logoSize = $logoSize;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ConfigLogoImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class ConfigLogoImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="config_logo_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo", inversedBy="logoImages")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set ConfigLogo
     *
     * @param null $entity
     *
     * @return ConfigLogoImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof ConfigLogo)) {
            throw new EntityNotFoundException('Entity should be instance of Config class');
        }
        $this->entity = $entity;

        return $this;
    }
}

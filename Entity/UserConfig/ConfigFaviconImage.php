<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ConfigFaviconImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class ConfigFaviconImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="config_favicon_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo", inversedBy="faviconImages")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set Config
     *
     * @param null $entity
     *
     * @return ConfigFaviconImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof ConfigLogo)) {
            throw new EntityNotFoundException('Entity should be instance of Config class');
        }
        $this->entity = $entity;

        return $this;
    }
}

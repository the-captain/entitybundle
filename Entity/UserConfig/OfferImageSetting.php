<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OfferImageSetting
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="offer_image_setting")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\OfferImageSettingRepository")
 */
class OfferImageSetting
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage", inversedBy="setting")
     * @ORM\JoinColumn(name="offer_image_id", referencedColumnName="id")
     */
    protected $offerImage;

    /**
     * Is active
     *
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $active;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * Offer Image Setting constructor.
     */
    public function __construct()
    {
        $this->active = true;
    }

    /**
     * Set config offer image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage $offerImage
     *
     * @return $this
     */
    public function setOfferImage(ConfigOfferImage $offerImage = null)
    {
        $this->offerImage = $offerImage;

        return $this;
    }

    /**
     * Get config offer image
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage
     */
    public function getOfferImage()
    {
        return $this->offerImage;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get is active
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * Set is active
     *
     * @param bool $active
     *
     * @return OfferImageSetting
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return OfferImageSetting
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OfferImageSetting
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Config
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="site_config")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ConfigRepository")
 */
class Config implements OwnerInterface
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config User
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="config")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * Config Site Theme
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Theme", inversedBy="configsRoot")
     * @ORM\JoinColumn(name="theme_root_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $rootTheme;

    /**
     * Config Site Theme
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Theme", inversedBy="configsSell")
     * @ORM\JoinColumn(name="theme_sell_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $sellTheme;

    /**
     * Config Site Theme
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Theme", inversedBy="configsBuy")
     * @ORM\JoinColumn(name="theme_buy_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $buyTheme;

    /**
     * Config Site Theme
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Theme", inversedBy="configsRepair")
     * @ORM\JoinColumn(name="theme_repair_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $repairTheme;

    /**
     * Background color
     *
     * @var string
     *
     * @ORM\Column(name="background_color", type="string", length=6)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/",
     *     match=true,
     *     message="Your property should match standard hex color representation"
     * )
     */
    protected $backgroundColor;

    /**
     * Config texts
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts", mappedBy="config")
     */
    protected $configTexts;

    /**
     * Config logo
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo", mappedBy="config")
     */
    protected $configLogo;

    /**
     * Config slider
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider", mappedBy="config", cascade={"persist"})
     */
    protected $configSlider;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    private $offerImages;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="decimal", scale=0, nullable=true)
     */
    protected $tax;

    /**
     * Config social links
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSocialLink", mappedBy="config")
     */
    protected $configSocial;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->backgroundColor = 'ffffff';
        $this->offerImages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set User
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Set Root Theme
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     *
     * @return $this
     */
    public function setRootTheme(Theme $theme = null)
    {
        $this->rootTheme = $theme;

        return $this;
    }

    /**
     * Get Root theme
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     */
    public function getRootTheme()
    {
        return $this->rootTheme;
    }

    /**
     * Set Sell Theme
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     *
     * @return $this
     */
    public function setSellTheme(Theme $theme = null)
    {
        $this->sellTheme = $theme;

        return $this;
    }

    /**
     * Get Sell theme
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     */
    public function getSellTheme()
    {
        return $this->sellTheme;
    }

    /**
     * Set Buy Theme
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     *
     * @return $this
     */
    public function setBuyTheme(Theme $theme = null)
    {
        $this->buyTheme = $theme;

        return $this;
    }

    /**
     * Get Buy theme
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     */
    public function getBuyTheme()
    {
        return $this->buyTheme;
    }

    /**
     * Set repair Theme
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     *
     * @return $this
     */
    public function setRepairTheme(Theme $theme = null)
    {
        $this->repairTheme = $theme;

        return $this;
    }

    /**
     * Get Repair theme
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Theme
     */
    public function getRepairTheme()
    {
        return $this->repairTheme;
    }


    /**
     * @return string
     */
    public function getBackgroundColor(): string
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     *
     * @return $this
     */
    public function setBackgroundColor(string $backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    /**
     * Set config texts
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts $configTexts
     *
     * @return $this
     */
    public function setConfigTexts(ConfigTexts $configTexts)
    {
        $this->configTexts = $configTexts;
        $configTexts->setConfig($this);

        return $this;
    }

    /**
     * Get config texts
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts
     */
    public function getConfigTexts()
    {
        return $this->configTexts;
    }

    /**
     * Set config logo
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo $configLogo
     *
     * @return $this
     */
    public function setConfigLogo(ConfigLogo $configLogo)
    {
        $this->configLogo = $configLogo;
        $configLogo->setConfig($this);

        return $this;
    }

    /**
     * Get config logo
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo
     */
    public function getConfigLogo()
    {
        return $this->configLogo;
    }

    /**
     * Set config slider
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider $configSlider
     *
     * @return $this
     */
    public function setConfigSlider(ConfigSlider $configSlider)
    {
        $this->configSlider = $configSlider;
        $configSlider->setConfig($this);

        return $this;
    }

    /**
     * Get config slider
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider
     */
    public function getConfigSlider()
    {
        if (!$this->configSlider) {
            $this->setConfigSlider(new ConfigSlider());
        }

        return $this->configSlider;
    }

    /**
     * Add offer image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage $image
     *
     * @return Config
     */
    public function addOfferImage(ConfigOfferImage $image)
    {
        $this->offerImages->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove offer image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage $image
     */
    public function removeOfferImage(ConfigOfferImage $image)
    {
        $this->offerImages->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get offer images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfferImages()
    {
        return $this->offerImages;
    }

    /**
     * Set config social links
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSocialLink $configSocialLink
     *
     * @return $this
     */
    public function setConfigSocial(ConfigSocialLink $configSocialLink = null)
    {
        $this->configSocial = $configSocialLink;
        $configSocialLink->setConfig($this);

        return $this;
    }

    /**
     * Get config social links
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSocialLink
     */
    public function getConfigSocial()
    {
        return $this->configSocial;
    }

    /**
     * @param $image
     */
    public function addLogoImage($image)
    {
        $this->configLogo->addLogoImage($image);
    }

    /**
     * @param $image
     */
    public function addFaviconImage($image)
    {
        $this->configLogo->addFaviconImage($image);
    }

    /**
     * @param $image
     */
    public function addSliderImage($image)
    {
        $this->configSlider->addSliderImage($image);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliderImages()
    {
        return $this->getConfigSlider()->getSliderImages();
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     *
     * @return Config
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Config';
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\BaseEntity;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class Question
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="faq_question")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\QuestionRepository")
 */
class Question extends BaseEntity implements OwnerInterface, SynchronizeableInterface
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     * @Assert\NotBlank()
     */
    protected $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="userQuestions")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="questions")
     * @ORM\JoinTable(name="user_questions")
     */
    protected $users;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion(string $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     *
     * @return Question
     */
    public function setAnswer(string $answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return Question
     */
    public function setCategory(string $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return Question
     */
    public function setOwner(User $user= null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Add user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return Question
     */
    public function addUser(User $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}

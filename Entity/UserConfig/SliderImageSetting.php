<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SliderImageSetting
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="slider_image_setting")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\SliderImageSettingRepository")
 */
class SliderImageSetting
{
    const LANDING = 'landing';
    const BUY = 'buy';
    const SELL = 'sell';
    const REPAIR = 'repair';

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage", inversedBy="setting")
     * @ORM\JoinColumn(name="slider_image_id", referencedColumnName="id")
     */
    protected $sliderImage;

    /**
     * Is display on landing
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_landing", type="boolean")
     */
    protected $displayLanding;

    /**
     * Is display on buy site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_buy", type="boolean")
     */
    protected $displayBuy;

    /**
     * Is display on sell site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_sell", type="boolean")
     */
    protected $displaySell;

    /**
     * Is display on repair site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_repair", type="boolean")
     */
    protected $displayRepair;

    /**
     * link
     *
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;

    /**
     * Slider Image Setting constructor.
     */
    public function __construct()
    {
        $this->displayLanding = true;
        $this->displayBuy = true;
        $this->displaySell = true;
        $this->displayRepair = true;
    }

    /**
     * Set config slider image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage $sliderImage
     *
     * @return $this
     */
    public function setSliderImage(ConfigSliderImage $sliderImage = null)
    {
        $this->sliderImage = $sliderImage;

        return $this;
    }

    /**
     * Get config slider image
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage
     */
    public function getSliderImage()
    {
        return $this->sliderImage;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDisplayLanding(): bool
    {
        return $this->displayLanding;
    }

    /**
     * @param bool $displayLanding
     *
     * @return SliderImageSetting
     */
    public function setDisplayLanding(bool $displayLanding)
    {
        $this->displayLanding = $displayLanding;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplayBuy(): bool
    {
        return $this->displayBuy;
    }

    /**
     * @param bool $displayBuy
     *
     * @return SliderImageSetting
     */
    public function setDisplayBuy(bool $displayBuy)
    {
        $this->displayBuy = $displayBuy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplaySell(): bool
    {
        return $this->displaySell;
    }

    /**
     * @param bool $displaySell
     *
     * @return SliderImageSetting
     */
    public function setDisplaySell(bool $displaySell)
    {
        $this->displaySell = $displaySell;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplayRepair(): bool
    {
        return $this->displayRepair;
    }

    /**
     * @param bool $displayRepair
     *
     * @return SliderImageSetting
     */
    public function setDisplayRepair(bool $displayRepair)
    {
        $this->displayRepair = $displayRepair;

        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     *
     * @return SliderImageSetting
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Method to get visibility on site
     *
     * @param string $site
     *
     * @return bool
     */
    public function isDisplayOnSite(string $site)
    {
        switch ($site) {
            case self::LANDING:
                return $this->displayLanding;
                break;
            case self::BUY:
                return $this->displayBuy;
                break;
            case self::SELL:
                return $this->displaySell;
                break;
            case self::REPAIR:
                return $this->displayRepair;
                break;
        }

        return false;
    }
}

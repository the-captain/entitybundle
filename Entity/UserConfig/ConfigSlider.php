<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ConfigSlider
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="config_slider")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ConfigSliderRepository")
 */
class ConfigSlider
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", inversedBy="configSlider")
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    protected $config;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    private $sliderImages;

    /**
     * Is display on landing
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_landing", type="boolean")
     */
    protected $displayLanding;

    /**
     * Is display on buy site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_buy", type="boolean")
     */
    protected $displayBuy;

    /**
     * Is display on sell site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_sell", type="boolean")
     */
    protected $displaySell;

    /**
     * Is display on repair site
     *
     * @var boolean
     *
     * @ORM\Column(name="is_display_repair", type="boolean")
     */
    protected $displayRepair;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        $this->sliderImages = new ArrayCollection();
        $this->displayLanding = true;
        $this->displayBuy = true;
        $this->displaySell = true;
        $this->displayRepair = true;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set config
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config = null)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Add slider image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage $image
     *
     * @return ConfigSlider
     */
    public function addSliderImage(ConfigSliderImage $image)
    {
        $this->sliderImages->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove slider image
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage $image
     */
    public function removeSliderImage(ConfigSliderImage $image)
    {
        $this->sliderImages->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get slider images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliderImages()
    {
        return $this->sliderImages;
    }

    /**
     * @return bool
     */
    public function isDisplayLanding(): bool
    {
        return $this->displayLanding;
    }

    /**
     * @param bool $displayLanding
     *
     * @return ConfigSlider
     */
    public function setDisplayLanding(bool $displayLanding)
    {
        $this->displayLanding = $displayLanding;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplayBuy(): bool
    {
        return $this->displayBuy;
    }

    /**
     * @param bool $displayBuy
     *
     * @return ConfigSlider
     */
    public function setDisplayBuy(bool $displayBuy)
    {
        $this->displayBuy = $displayBuy;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplaySell(): bool
    {
        return $this->displaySell;
    }

    /**
     * @param bool $displaySell
     *
     * @return ConfigSlider
     */
    public function setDisplaySell(bool $displaySell)
    {
        $this->displaySell = $displaySell;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisplayRepair(): bool
    {
        return $this->displayRepair;
    }

    /**
     * @param bool $displayRepair
     *
     * @return ConfigSlider
     */
    public function setDisplayRepair(bool $displayRepair)
    {
        $this->displayRepair = $displayRepair;

        return $this;
    }

    /**
     * Method to get sliders images for site
     *
     * @param string $site
     *
     * @return array
     */
    public function getSliderImagesForSite(string $site)
    {
        $sliders = [];
        foreach ($this->sliderImages as $item) {
            if ($item->getSetting()->isDisplayOnSite($site)) {
                array_push($sliders, $item);
            }
        }

        return $sliders;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class Seo
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="user_seo")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\SeoRepository")
 */
class Seo implements OwnerInterface
{
    /**
     * Selling Site Type
     */
    const TYPE_SALE = 'sell';

    /**
     * BuyBack Site Type
     */
    const TYPE_BUYBACK = 'buy';

    /**
     * Repair Site Type
     */
    const TYPE_REPAIR = 'repair';

    /**
     * Landing Type
     */
    const TYPE_LANDING = 'landing';

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", length=255, nullable=true)
     */
    protected $keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="site", type="string", length=16)
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="seoMetas")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Config logo
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Model", inversedBy="seoMetas")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    protected $model;

    /**
     * Config User
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Category", inversedBy="seoMeta")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * Config logo
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", inversedBy="seoMeta")
     * @ORM\JoinColumn(name="inventroy_item_id", referencedColumnName="id")
     */
    protected $inventoryItem;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Seo
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     *
     * @return Seo
     */
    public function setKeyword(?string $keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Seo
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     *
     * @return Seo
     */
    public function setSite(string $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return Seo
     */
    public function setOwner(User $user= null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param Model|null $model
     * @return Seo
     */
    public function setModel(Model $model = null)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return null|Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Category|null $category
     * @return Seo
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return null|Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param InventoryItem|null $inventoryItem
     * @return Seo
     */
    public function setInventoryItem(InventoryItem $inventoryItem = null)
    {
        $this->inventoryItem = $inventoryItem;

        return $this;
    }

    /**
     * @return null|Category
     */
    public function getInventoryItem()
    {
        return $this->inventoryItem;
    }
}

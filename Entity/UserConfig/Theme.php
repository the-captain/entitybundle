<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Theme
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="site_theme")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ThemeRepository")
 */
class Theme
{
    /**
     * Type All
     */
    const TYPE_ROOT = 0;

    /**
     * Type Sell
     */
    const TYPE_SELL = 1;

    /**
     * Type Buy
     */
    const TYPE_BUY = 2;

    /**
     * Type Repair
     */
    const TYPE_REPAIR = 3;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", mappedBy="rootTheme", cascade={"persist"})
     */
    protected $configsRoot;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", mappedBy="sellTheme", cascade={"persist"})
     */
    protected $configsSell;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", mappedBy="buyTheme", cascade={"persist"})
     */
    protected $configsBuy;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", mappedBy="repairTheme", cascade={"persist"})
     */
    protected $configsRepair;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        $this->configsRoot = new ArrayCollection();
        $this->configsSell = new ArrayCollection();
        $this->configsBuy = new ArrayCollection();
        $this->configsRepair = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Theme
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Add config root
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function addConfigRoot(Config $config)
    {
        $this->configsRoot[] = $config;
        $config->setRootTheme($this);

        return $this;
    }

    /**
     * Remove config root
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     */
    public function removeConfigRoot(Config $config)
    {
        $this->configsRoot->removeElement($config);
        $config->setRootTheme(null);
    }

    /**
     * Get configs root
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigsRoot()
    {
        return $this->configsRoot;
    }

    /**
     * Add config sell
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function addConfigSell(Config $config)
    {
        $this->configsSell[] = $config;
        $config->setSellTheme($this);

        return $this;
    }

    /**
     * Remove config Sell
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     */
    public function removeConfigSell(Config $config)
    {
        $this->configsSell->removeElement($config);
        $config->setSellTheme(null);
    }

    /**
     * Get configs Sell
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigsSell()
    {
        return $this->configsSell;
    }

    /**
     * Add config Buy
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function addConfigBuy(Config $config)
    {
        $this->configsBuy[] = $config;
        $config->setBuyTheme($this);

        return $this;
    }

    /**
     * Remove config Buy
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     */
    public function removeConfigBuy(Config $config)
    {
        $this->configsBuy->removeElement($config);
        $config->setBuyTheme(null);
    }

    /**
     * Get configs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigsBuy()
    {
        return $this->configsBuy;
    }

    /**
     * Add config Repair
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function addConfigRepair(Config $config)
    {
        $this->configsRepair[] = $config;
        $config->setRepairTheme($this);

        return $this;
    }

    /**
     * Remove config Repair
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     */
    public function removeConfigRepair(Config $config)
    {
        $this->configsRepair->removeElement($config);
        $config->setThemeRepair(null);
    }

    /**
     * Get configs Repair
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfigRepairs()
    {
        return $this->configsRepair;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: 'Theme';
    }
}

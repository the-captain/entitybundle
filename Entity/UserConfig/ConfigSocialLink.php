<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ConfigSocialLink
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="config_social_links")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ConfigSocialLinkRepository")
 */
class ConfigSocialLink
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", inversedBy="configSocial")
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    protected $config;

    /**
     * Twitter link
     *
     * @var string
     *
     * @ORM\Column(name="twitter_link", type="string", length=64, nullable=true)
     * @Assert\Url()
     */
    protected $twitterLink;

    /**
     * Facebook link
     *
     * @var string
     *
     * @ORM\Column(name="facebook_link", type="string", length=64, nullable=true)
     * @Assert\Url()
     */
    protected $facebookLink;

    /**
     * Instagram link
     *
     * @var string
     *
     * @ORM\Column(name="instagram_link", type="string", length=64, nullable=true)
     * @Assert\Url()
     */
    protected $instagramLink;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set config
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config = null)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getTwitterLink()
    {
        return $this->twitterLink;
    }

    /**
     * @param string $twitterLink
     *
     * @return ConfigSocialLink
     */
    public function setTwitterLink(string $twitterLink)
    {
        $this->twitterLink = $twitterLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     *
     * @return ConfigSocialLink
     */
    public function setFacebookLink(string $facebookLink)
    {
        $this->facebookLink = $facebookLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstagramLink()
    {
        return $this->instagramLink;
    }

    /**
     * @param string $instagramLink
     *
     * @return ConfigSocialLink
     */
    public function setInstagramLink(string $instagramLink)
    {
        $this->instagramLink = $instagramLink;

        return $this;
    }
}

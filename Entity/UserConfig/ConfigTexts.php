<?php

namespace ThreeWebOneEntityBundle\Entity\UserConfig;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ConfigTexts
 *
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="config_texts")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\UserConfig\ConfigTextsRepository")
 */
class ConfigTexts
{
    /**
     * fonts
     */
    const CRILLEE_REGULAR = 'crillee_regular';
    const FURORE_REGULAR = 'furore_regular';
    const HELVETICA_ROUND = 'Helvetica round';
    const BITTER_REGULAR = 'bitter_regular';
    const RALEWAY_REGULAR = 'raleway_regular';
    const SLICKER_REGULAR = 'slicker_regular';
    const MYRIAD_PRO_REGULAR = 'myriad_pro_regular';
    const VIRTUE_REGULAR = 'virtue_regular';
    const ORBITRON_REGULAR = 'orbitron_regular';
    const GAYATRI_REGULAR = 'gayatri_regular';
    const DUKE_REGULAR = 'duke_regular';
    const MONTSERRAT_REGULAR = 'montserrat_regular';
    const NEO_SANS_BOLD = 'neo_sans_bold';
    const CHARLIE_BROWN_M54_REGULAR = 'charlie_brown_m54_regular';
    const QUICKSAND_REGULAR = 'quicksand_regular';
    const ARIAL_NARROW = 'Arial narrow';

    /**
     * font size
     */
    const SIZE_SMALL = '20px';
    const SIZE_MEDIUM = '24px';
    const SIZE_LARGE = '35px';

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Config slider image
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Config", inversedBy="configTexts")
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    protected $config;

    /**
     * Visibility of name
     *
     * @var boolean
     *
     * @ORM\Column(name="is_business_name_visible", type="boolean")
     */
    protected $visibleBusinessName;

    /**
     * Name of business
     *
     * @var string
     *
     * @ORM\Column(name="business_name", type="string", length=64, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $businessName;

    /**
     * Font of business name
     *
     * @var string
     *
     * @ORM\Column(name="font", type="string", length=64, nullable=true)
     */
    protected $font;

    /**
     * Color of business name
     *
     * @var string
     *
     * @ORM\Column(name="business_name_color", type="string", length=6, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/",
     *     match=true,
     *     message="Your property should match standard hex color representation"
     * )
     */
    protected $businessNameColor;

    /**
     * Color of business name
     *
     * @var string
     *
     * @ORM\Column(name="business_name_size", type="string", length=16, nullable=true)
     */
    protected $businessNameSize;

    /**
     * @var string
     *
     * @ORM\Column(name="landing_page_title", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $landingPageTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonials_title", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $testimonialsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="buy_title", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $buyTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="sell_title", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $sellTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="repair_title", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $repairTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="buy_description", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=255, minMessage="Description is too short", maxMessage="Title is too long")
     */
    protected $buyDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="sell_description", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=255, minMessage="Description is too short", maxMessage="Title is too long")
     */
    protected $sellDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="repair_description", type="string", length=255, nullable=true)
     * @Assert\Length(min=5, max=255, minMessage="Description is too short", maxMessage="Title is too long")
     */
    protected $repairDescription;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        $this->visibleBusinessName = true;
        $this->businessNameColor = '000000';
        $this->businessNameSize = '25px';
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set config
     *
     * @param \ThreeWebOneEntityBundle\Entity\UserConfig\Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config = null)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return \ThreeWebOneEntityBundle\Entity\UserConfig\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return bool
     */
    public function isVisibleBusinessName()
    {
        return $this->visibleBusinessName;
    }

    /**
     * @param $visibleBusinessName
     *
     * @return $this
     */
    public function setVisibleBusinessName($visibleBusinessName)
    {
        $this->visibleBusinessName = $visibleBusinessName;

        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * @param $businessName
     *
     * @return $this
     */
    public function setBusinessName($businessName)
    {
        $this->businessName = $businessName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFont()
    {
        return $this->font;
    }

    /**
     * @param $font
     *
     * @return $this
     */
    public function setFont($font)
    {
        $this->font = $font;

        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessNameColor()
    {
        return $this->businessNameColor;
    }

    /**
     * @param $businessNameColor
     *
     * @return $this
     */
    public function setBusinessNameColor($businessNameColor)
    {
        $this->businessNameColor = $businessNameColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getLandingPageTitle()
    {
        return $this->landingPageTitle;
    }

    /**
     * @param $landingPageTitle
     *
     * @return $this
     */
    public function setLandingPageTitle($landingPageTitle)
    {
        $this->landingPageTitle = $landingPageTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getTestimonialsTitle()
    {
        return $this->testimonialsTitle;
    }

    /**
     * @param $testimonialsTitle
     *
     * @return $this
     */
    public function setTestimonialsTitle($testimonialsTitle)
    {
        $this->testimonialsTitle = $testimonialsTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getBuyTitle()
    {
        return $this->buyTitle;
    }

    /**
     * @param $buyTitle
     *
     * @return $this
     */
    public function setBuyTitle($buyTitle)
    {
        $this->buyTitle = $buyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getSellTitle()
    {
        return $this->sellTitle;
    }

    /**
     * @param $sellTitle
     *
     * @return $this
     */
    public function setSellTitle($sellTitle)
    {
        $this->sellTitle = $sellTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getRepairTitle()
    {
        return $this->repairTitle;
    }

    /**
     * @param $repairTitle
     *
     * @return $this
     */
    public function setRepairTitle($repairTitle)
    {
        $this->repairTitle = $repairTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getBuyDescription()
    {
        return $this->buyDescription;
    }

    /**
     * @param $buyDescription
     *
     * @return $this
     */
    public function setBuyDescription($buyDescription)
    {
        $this->buyDescription = $buyDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getSellDescription()
    {
        return $this->sellDescription;
    }

    /**
     * @param $sellDescription
     *
     * @return $this
     */
    public function setSellDescription($sellDescription)
    {
        $this->sellDescription = $sellDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getRepairDescription()
    {
        return $this->repairDescription;
    }

    /**
     * @param $repairDescription
     *
     * @return $this
     */
    public function setRepairDescription($repairDescription)
    {
        $this->repairDescription = $repairDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessNameSize()
    {
        return $this->businessNameSize;
    }

    /**
     * @param string $businessNameSize
     *
     * @return $this
     */
    public function setBusinessNameSize($businessNameSize)
    {
        $this->businessNameSize = $businessNameSize;

        return $this;
    }
}

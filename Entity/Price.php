<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;

/**
 * Class Price
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="prices")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\PriceRepository")
 */
class Price implements OwnerInterface, SynchronizeableInterface
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", scale=0, nullable=true)
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Model", inversedBy="prices")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    private $model;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="prices")
     * @ORM\JoinTable(name="user_prices")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="price", cascade={"persist"})
     */
    protected $inventoryItems;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\PriceType", inversedBy="prices")
     * @ORM\JoinColumn(name="price_type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $priceType;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userPrices")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Image constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Price
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     *
     * @return Price
     */
    public function setModel(Model $model = null)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return \ThreeWebOneEntityBundle\Entity\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Price '.$this->getValue()/100;
    }

    /**
     * Add user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return Price
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set priceType
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $priceType
     *
     * @return Price
     */
    public function setPriceType(PriceType $priceType = null)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get priceType
     *
     * @return \ThreeWebOneEntityBundle\Entity\PriceType
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user= null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Add inventory item
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     *
     * @return Price
     */
    public function addInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->add($inventoryItem);
        $inventoryItem->setPrice($this);

        return $this;
    }

    /**
     * Remove inventory item
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     */
    public function removeInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->removeElement($inventoryItem);
        $inventoryItem->setPrice(null);
    }

    /**
     * Get sell items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Get InventoryItem
     *
     * @return InventoryItem|null
     */
    public function getInventoryItem()
    {
        return $this->getInventoryItems()[0] ?? null;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        $count = 0;
        foreach ($this->inventoryItems as $inventoryItem) {
            $count += $inventoryItem->getRealQuantity();
        }

        return $count;
    }
}

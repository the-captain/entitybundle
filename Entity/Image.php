<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Image
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entity", type="string")
 * @ORM\DiscriminatorMap({"image" = "Image",
 *      "model_image" = "ModelImage",
 *      "inventory_item_image" = "ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage",
 *      "category_image" = "CategoryImage",
 *      "filter_image" = "FilterImage",
 *      "accessory_image" = "AccessoryImage",
 *      "advertise_image" = "AdvertiseImage",
 *      "config_logo_image" = "ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoImage",
 *      "config_favicon_image" = "ThreeWebOneEntityBundle\Entity\UserConfig\ConfigFaviconImage",
 *      "config_offer_image" = "ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage",
 *      "config_slider_image" = "ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage",
 *      "config_logo_default_image" = "ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoDefaultImage"
 *     })
 */
class Image implements OwnerInterface
{
    use UpdatedAtTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Entity
     */
    protected $entity;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="images")
     * @ORM\JoinTable(name="user_images")
     */
    protected $users;

    /**
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\Column(name="image_name", type="string", length=255)
     *
     * @var string
     */
    protected $imageName;

    /**
     * @ORM\Column(name="image_size", type="integer", nullable=true)
     *
     * @var integer
     */
    protected $imageSize;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userImages")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Image constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire
     */
    public function refreshUpdated()
    {
        $this->setUpdatedAt(new \DateTime);
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Image
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Image
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * @return integer|null
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Entity
     *
     * @param object $entity
     *
     * @return Image
     */
    public function setEntity($entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get Entity
     *
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getImageName() ?: 'Image';
    }

    /**
     * Add user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return Image
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Category clone
     */
    public function __clone()
    {
        $this->id = null;
        $this->setOwner(null);
        $this->users = new ArrayCollection();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class FilterImage
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class FilterImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="filter_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Filter", inversedBy="images")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set Filter
     *
     * @param null $entity
     *
     * @return FilterImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof Filter)) {
            throw new EntityNotFoundException('Entity should be instance of Filter class');
        }
        $this->entity = $entity;

        return $this;
    }
}

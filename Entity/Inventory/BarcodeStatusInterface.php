<?php

namespace ThreeWebOneEntityBundle\Entity\Inventory;

interface BarcodeStatusInterface
{
    const NEW_BARCODE = 0;
    const LISTED = 1;
    const SOLD = 2;

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param integer $status
     * @return $this
     */
    public function setStatus($status);
}

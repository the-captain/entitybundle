<?php

namespace ThreeWebOneEntityBundle\Entity\Inventory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Image;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ModelImage
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\InventoryItemImageRepository")
 * @Vich\Uploadable()
 */
class InventoryItemImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="inventory_item_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", inversedBy="images")
     * @ORM\JoinTable(
     *     name="inventory_items_images",
     *     joinColumns={@ORM\JoinColumn(name="inventory_item_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="inventory_item_image_id", referencedColumnName="id")}
     *     )
     */
    protected $entities;

    /**
     * @var bool
     */
    public $isMain;

    /**
     * InventoryItemImage constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->entities = new ArrayCollection();
    }

    /**
     * @param $entity
     *
     * @return $this
     */
    public function addEntity($entity)
    {
        if (($entity !== null) && !($entity instanceof InventoryItem)) {
            throw new EntityNotFoundException('Entity should be instance of Inventory Item class');
        }
        $this->entities->add($entity);

        return $this;
    }

    /**
     * @param $entity
     *
     * @return $this
     */
    public function removeEntity($entity)
    {
        if (($entity !== null) && !($entity instanceof InventoryItem)) {
            throw new EntityNotFoundException('Entity should be instance of Inventory Item class');
        }
        $this->entities->removeElement($entity);

        return $this;
    }
}

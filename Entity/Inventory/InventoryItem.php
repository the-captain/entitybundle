<?php

namespace ThreeWebOneEntityBundle\Entity\Inventory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\FilterTrait;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialInterface;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialTrait;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Image;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Sluggable\Sluggable;
use ThreeWebOneEntityBundle\Sluggable\SluggableInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Inventory Item
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="inventory_items",
 *      uniqueConstraints={
 *        @UniqueConstraint(name="owner_store_id",
 *            columns={"owner_id", "store_id"})
 *    })
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Inventory\InventoryItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InventoryItem implements SiteTypeInterface, OwnerInterface, SluggableInterface, SequentialInterface
{
    use StatusTrait, CreatedAtTrait, UpdatedAtTrait, FilterTrait, Sluggable, SequentialTrait;

    /**
     * Status Active
     */
    const STATUS_ACTIVE = 1;

    const ACTIVE_NAME = 'Active';

    /**
     * Status InActive
     */
    const STATUS_INACTIVE = 0;

    const INACTIVE_NAME = 'Inactive';

    const STATUS_ARRAY = [
        self::ACTIVE_NAME => self::STATUS_ACTIVE,
        self::INACTIVE_NAME => self::STATUS_INACTIVE,
    ];

    const STATUS_PREPARED = 2;

    const STATUS_COMPLETED_HIDDEN = 3;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=true)
     */
    protected $type;

    /**
     * @var float
     *
     * @ORM\Column(name="purchase_price", type="decimal", scale=0, nullable=true)
     */
    protected $purchasePrice;

    /**
     * @var float
     *
     * @ORM\Column(name="price_value", type="decimal", scale=0, nullable=true)
     */
    protected $priceValue;

    /**
     * @var float
     *
     * @ORM\Column(name="shipping_price", type="decimal", scale=0, nullable=true)
     */
    protected $shippingPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="main_image_id", type="integer", scale=0, nullable=true)
     */
    protected $mainImageId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Model", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    private $model;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\Inventory", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="inventory_id", referencedColumnName="id")
     */
    private $inventory;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Order\Order", inversedBy="items")
     * @ORM\JoinTable(name="orders_inventory_items", joinColumns={@ORM\JoinColumn(name="inventoryitem_id", referencedColumnName="id", onDelete="cascade")})
     */
    protected $orders;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Category", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode", mappedBy="inventoryItem", orphanRemoval=true, cascade={"persist"})
     */
    protected $inventoryItemBarcodes;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Order\OrderItem", mappedBy="inventoryItem", orphanRemoval=true, cascade={"persist"})
     */
    protected $OrderItems;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\PriceType", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="price_type_id", referencedColumnName="id")
     */
    protected $priceType;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Price", inversedBy="inventoryItems")
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id")
     */
    protected $price;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Filter", inversedBy="inventoryItems")
     * @ORM\JoinTable(name="filter_inventory_items", joinColumns={@ORM\JoinColumn(name="inventoryitem_id", referencedColumnName="id")})
     */
    protected $filters;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage", mappedBy="entities", cascade={"all"})
     */
    protected $images;

    /**
     * @var int
     *
     * @ORM\Column(name="asked_quantity", type="integer", nullable=true)
     */
    protected $quantity;

    /**
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Seo", mappedBy="inventoryItem")
     */
    protected $seoMeta;

    /**
     * Inventory constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->inventoryItemBarcodes = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->OrderItems = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->status = false;
    }

    /**
     * @ORM\PrePersist
     */
    public function createdAtUpdate()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedAtUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return InventoryItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set price type
     *
     * @param \ThreeWebOneEntityBundle\Entity\PriceType $priceType
     *
     * @return InventoryItem
     */
    public function setPriceType(PriceType $priceType = null)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get price type
     *
     * @return \ThreeWebOneEntityBundle\Entity\PriceType
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set price
     *
     * @param \ThreeWebOneEntityBundle\Entity\Price $price
     *
     * @return InventoryItem
     */
    public function setPrice(Price $price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return \ThreeWebOneEntityBundle\Entity\Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set model
     *
     * @param \ThreeWebOneEntityBundle\Entity\Model $model
     *
     * @return InventoryItem
     */
    public function setModel(Model $model = null)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return \ThreeWebOneEntityBundle\Entity\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set inventory
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\Inventory $inventory
     *
     * @return InventoryItem
     */
    public function setInventory(Inventory $inventory = null)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory
     *
     * @return \ThreeWebOneEntityBundle\Entity\Inventory\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     *
     * @return InventoryItem
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set price
     *
     * @param string $priceValue
     *
     * @return InventoryItem
     */
    public function setPriceValue($priceValue)
    {
        $this->priceValue = $priceValue;

        return $this;
    }

    /**
     * Get price value
     *
     * @return string
     */
    public function getPriceValue()
    {
        return $this->priceValue;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return InventoryItem
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \ThreeWebOneEntityBundle\Entity\Order\Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     *
     * @return InventoryItem
     */
    public function addOrder(Order $order)
    {
        $this->orders->add($order);

        return $this;
    }

    /**
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     *
     * @return InventoryItem
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);

        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     *
     * @return InventoryItem
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Add inventory item barcode
     *
     * @param InventoryItemBarcode $inventoryItemBarcode
     *
     * @return InventoryItem
     */
    public function addInventoryItemBarcode(InventoryItemBarcode $inventoryItemBarcode)
    {
        $this->inventoryItemBarcodes->add($inventoryItemBarcode);
        $inventoryItemBarcode->setInventoryItem($this);

        return $this;
    }

    /**
     * Remove inventory item barcode
     *
     * @param InventoryItemBarcode $inventoryItemBarcode
     */
    public function removeInventoryItemBarcode(InventoryItemBarcode $inventoryItemBarcode)
    {
        $this->inventoryItemBarcodes->removeElement($inventoryItemBarcode);
        $inventoryItemBarcode->setInventoryItem(null);
    }

    /**
     * Get inventory item barcode
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItemBarcodes()
    {
        return $this->inventoryItemBarcodes;
    }

    /**
     * Set owner
     *
     * @param \ThreeWebOneEntityBundle\Entity\User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->inventoryItemBarcodes->count() ?: $this->quantity;
    }

    /**
     * @return int
     */
    public function getEnteredQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getRealQuantity()
    {
        $count = 0;
        foreach ($this->inventoryItemBarcodes as $barcode) {
            if ($barcode->getStatus() == BarcodeStatusInterface::NEW_BARCODE) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @param int $quantity
     *
     * @return InventoryItem
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'InventoryItem';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return InventoryItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add filter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $filter
     *
     * @return InventoryItem
     */
    public function addFilter(Filter $filter)
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * Remove filter
     *
     * @param \ThreeWebOneEntityBundle\Entity\Filter $filter
     */
    public function removeFilter(Filter $filter)
    {
        $this->filters->removeElement($filter);
    }

    /**
     * Get filters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    /**
     * @param float $shippingPrice
     *
     * @return InventoryItem
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    /**
     * @return OrderItem|null
     */
    public function getOrderItem()
    {
        return $this->OrderItems[0] ?? null;
    }

    /**
     * Add image
     *
     * @param InventoryItemImage $image
     *
     * @return InventoryItem
     */
    public function addImage(InventoryItemImage $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->addEntity($this);
        }

        return $this;
    }

    /**
     * Remove image
     *
     * @param InventoryItemImage $image
     *
     * @return InventoryItem
     */
    public function removeImage(InventoryItemImage $image)
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            $image->removeEntity($this);
        }

        return $this;
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Gets Image
     *
     * @return Image | false
     */
    public function getImage()
    {
        if (!empty($this->getImages())) {
            if ($this->mainImageId) {
                foreach ($this->getImages() as $image) {
                    if ($this->mainImageId == $image->getId()) {
                        return $image;
                    }
                }
            }

            return $this->getImages()[0];
        }

        return false;
    }

    public function createSlug(): string
    {
        return $this->title ?? '';
    }

    /**
     * @return integer
     */
    public function getMainImageId()
    {
        return $this->mainImageId;
    }

    /**
     * @param $mainImageId
     *
     * @return InventoryItem
     */
    public function setMainImageId($mainImageId)
    {
        $this->mainImageId = $mainImageId;

        return $this;
    }

    /**
     * @return Seo
     */
    public function getSeoMeta()
    {
        return $this->seoMeta;
    }

    /**
     * @param Seo|null $seo
     * @return InventoryItem
     */
    public function setSeoMeta(Seo $seo = null)
    {
        $this->seoMeta = $seo;
        if ($seo) {
            $seo->setInventoryItem($this);
        }

        return $this;
    }
}

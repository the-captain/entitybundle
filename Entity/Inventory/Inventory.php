<?php

namespace ThreeWebOneEntityBundle\Entity\Inventory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Inventory
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="inventories")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Inventory\InventoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Inventory implements StatusInterface, SiteTypeInterface, OwnerInterface
{
    use StatusTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="inventory", cascade={"persist"})
     */
    private $inventoryItems;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="inventories")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Inventory constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->inventoryItems = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function createdAtUpdate()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedAtUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Inventory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add inventoryItem
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     *
     * @return Inventory
     */
    public function addInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems[] = $inventoryItem;
        $inventoryItem->setInventory($this);

        return $this;
    }

    /**
     * Remove inventoryItem
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     */
    public function removeInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->removeElement($inventoryItem);
        $inventoryItem->setInventory(null);
    }

    /**
     * Get inventoryItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Set user
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     *
     * @return Inventory
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ThreeWebOneEntityBundle\Entity\User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->getType() == self::SALE) {
            return 'Sale Inventory';
        }

        if ($this->getType() == self::REPAIR) {
            return 'Repair Inventory';
        }

        if ($this->getType() == self::BUYBACK) {
            return 'BuyBack Inventory';
        }

        return 'Inventory';
    }
}

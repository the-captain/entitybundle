<?php

namespace ThreeWebOneEntityBundle\Entity\Inventory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Order\Order;

/**
 * Class Inventory Item Barcode
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="inventory_item_barcodes")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Inventory\InventoryItemBarcodeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InventoryItemBarcode implements BarcodeStatusInterface
{
    use UpdatedAtTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=255, nullable=true)
     */
    protected $barcode;

    /**
     * @var int
     * @ORM\Column(name="purchase_price", type="integer", nullable=true)
     */
    protected $purchasePrice;

    /**
     * @var int
     * @ORM\Column(name="sell_price", type="integer", nullable=true)
     */
    protected $sellPrice;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", inversedBy="inventoryItemBarcodes")
     * @ORM\JoinColumn(name="inventory_item_id", referencedColumnName="id")
     */
    protected $inventoryItem;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Order\Order", inversedBy="saleBarcodes")
     * @ORM\JoinColumn(name="sale_order_id", referencedColumnName="id")
     */
    protected $saleOrder;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Order\Order", inversedBy="purchaseBarcodes")
     * @ORM\JoinColumn(name="purchase_order_id", referencedColumnName="id")
     */
    protected $purchaseOrder;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Order\OrderItem", mappedBy="barcode", orphanRemoval=true, cascade={"persist"})
     */
    protected $OrderItems;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime
     */
    protected $createdAt;

    /**
     * Inventory constructor.
     */
    public function __construct()
    {
        $this->status = self::NEW_BARCODE;
        $this->OrderItems = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function createdAtUpdate()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedAtUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return InventoryItemBarcode
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return InventoryItemBarcode
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set inventory item
     *
     * @param \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem $inventoryItem
     *
     * @return InventoryItemBarcode
     */
    public function setInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItem = $inventoryItem;

        return $this;
    }

    /**
     * Get inventory item
     *
     * @return \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem
     */
    public function getInventoryItem()
    {
        return $this->inventoryItem;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     *
     * @return InventoryItemBarcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     *
     * @return InventoryItemBarcode
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set SellPrice
     *
     * @param string $sellPrice
     *
     * @return InventoryItemBarcode
     */
    public function setSellPrice($sellPrice)
    {
        $this->sellPrice = $sellPrice;

        return $this;
    }

    /**
     * Get SellPrice
     *
     * @return string
     */
    public function getSellPrice()
    {
        return $this->sellPrice;
    }

    /**
     * @return \ThreeWebOneEntityBundle\Entity\Order\Order
     */
    public function getSaleOrder()
    {
        return $this->saleOrder;
    }

    /**
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     *
     * @return InventoryItemBarcode
     */
    public function setSaleOrder($order)
    {
        $this->saleOrder = $order;

        return $this;
    }

    /**
     * @return \ThreeWebOneEntityBundle\Entity\Order\Order
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param \ThreeWebOneEntityBundle\Entity\Order\Order $order
     *
     * @return InventoryItemBarcode
     */
    public function setPurchaseOrder($order)
    {
        $this->purchaseOrder = $order;

        return $this;
    }

    /**
     * @return null
     */
    public function getOrderItem()
    {
        return $this->OrderItems[0] ?? null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getBarcode() . ' ' . $this->inventoryItem->getModel() ?? '';
    }
}

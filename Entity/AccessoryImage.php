<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class FilterImage.
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class AccessoryImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="accessory_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Accessory", inversedBy="images")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set Filter.
     *
     * @param null $entity
     *
     * @return AccessoryImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if ((null !== $entity) && !($entity instanceof Accessory)) {
            throw new EntityNotFoundException('Entity should be instance of Accessory class');
        }
        $this->entity = $entity;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Advertise.
 *
 *
 * @ORM\Table(name="advertise")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\AdvertiseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Advertise extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\AdvertiseImage", mappedBy="entity",
     *                                                                             cascade={"persist"},
     *                                                                             orphanRemoval=true)
     */
    protected $images;

    /**
     * Order Item.
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\AdvertiseCategory", inversedBy="categoryItems")
     *
     * @ORM\JoinColumn(name="advertise_category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = '';

    /**
     * @var int
     *
     * @ORM\Column(name="orderNumber", type="integer",length=5)
     */
    private $orderNumber = 0;

    /**
     * @var string
     *
     * @Assert\Url()
     *
     * @ORM\Column(name="url", type="string", length=500)
     */
    private $url = '';

    /**
     * Advertise constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $name = $this->getName();

        return $name ?? 'Advertise';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return !empty($this->name);
    }

    /**
     * @param string $name
     *
     * @return Advertise
     */
    public function setName(string $name = null): self
    {
        if (!is_null($name)) {
            $this->name = $name;
        }

        return $this;
    }

    /**
     * Add image.
     *
     * @param AdvertiseImage $image
     *
     * @return Category
     */
    public function addImage(AdvertiseImage $image)
    {
        $this->images->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return bool
     */
    public function hasCategory()
    {
        return !empty($this->category);
    }

    /**
     * @param mixed $category
     *
     * @return Advertise
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return !empty($this->description);
    }

    /**
     * @param string $description
     *
     * @return Advertise
     */
    public function setDescription(string $description = null): self
    {
        if (!is_null($description)) {
            $this->description = $description;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return Advertise
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets Image.
     *
     * @return Image | false
     */
    public function getImage()
    {
        $images = $this->getImages();

        return !empty($images) ? $images[0] : false;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return bool
     */
    public function hasImages()
    {
        return !empty($this->images);
    }

    /**
     * @return int
     */
    public function getOrderNumber(): int
    {
        return $this->orderNumber;
    }

    /**
     * @return bool
     */
    public function hasOrderNumber(): bool
    {
        return !empty($this->orderNumber);
    }

    /**
     * @param int $orderNumber
     *
     * @return Advertise
     */
    public function setOrderNumber(int $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function hasUrl(): bool
    {
        return !empty($this->url);
    }

    /**
     * @param string $url
     *
     * @return Advertise
     */
    public function setUrl(string $url = null): self
    {
        if (!is_null($url)) {
            $this->url = $url;
        }

        return $this;
    }

    /**
     * Remove image.
     *
     * @param \ThreeWebOneEntityBundle\Entity\AdvertiseImage $image
     */
    public function removeImage(AdvertiseImage $image)
    {
        $this->images->removeElement($image);
        $image->setEntity(null);
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Queue\Message;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Synchronize.
 */
class Synchronize
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $synchronizeWith;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $entityClass;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $entityId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices={"synchronize","desynchronize"})
     */
    private $task;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'synchronizeWith' => $this->hasSynchronizeWith() ? $this->getSynchronizeWith() : null,
                'task' => $this->hasTask() ? $this->getTask() : null,
                'entityClass' => $this->hasEntityClass() ? $this->getEntityClass() : null,
                'entityId' => $this->hasEntityId() ? $this->getEntityId() : null,
            ]
        );
    }

    /**
     * @return int
     */
    public function getSynchronizeWith(): int
    {
        return $this->synchronizeWith;
    }

    /**
     * @return bool
     */
    public function hasSynchronizeWith(): bool
    {
        return !empty($this->synchronizeWith);
    }

    /**
     * @param int $synchronizeWith
     *
     * @return Synchronize
     */
    public function setSynchronizeWith(int $synchronizeWith): self
    {
        $this->synchronizeWith = $synchronizeWith;

        return $this;
    }

    /**
     * @return string
     */
    public function getTask(): string
    {
        return $this->task;
    }

    /**
     * @return bool
     */
    public function hasTask(): bool
    {
        return !empty($this->task);
    }

    /**
     * @param string $task
     *
     * @return Synchronize
     */
    public function setTask(string $task): self
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * @return bool
     */
    public function hasEntityClass(): bool
    {
        return !empty($this->entityClass);
    }

    /**
     * @param string $entityClass
     *
     * @return Synchronize
     */
    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @return bool
     */
    public function hasEntityId(): bool
    {
        return !empty($this->entityId);
    }

    /**
     * @param int $entityId
     *
     * @return Synchronize
     */
    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }
}

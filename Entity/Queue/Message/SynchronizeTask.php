<?php

namespace ThreeWebOneEntityBundle\Entity\Queue\Message;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class SynchronizeTask.
 */
class SynchronizeTask
{
    /**
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $synchronizeFrom;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    private $query;

    /**
     * @var array
     *
     * @Assert\Type(type="array")
     */
    private $queryParams;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $synchronizeWith;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $entityClass;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices={"synchronize","desynchronize"})
     */
    private $task;

    /**
     * @Assert\Callback()
     *
     * @param ExecutionContext $context
     * @param mixed            $payload
     */
    public function validate(ExecutionContext $context, $payload)
    {
        if (false === $this->hasQuery() && false === $this->hasSynchronizeFrom()) {
            $context->buildViolation('Must be fulfilled when query is empty!')
                ->atPath('synchronizeFrom')
                ->addViolation();
        }

        if (true === $this->hasQuery() && false === $this->hasQueryParams()) {
            $context->buildViolation('Must be fulfilled when query is not empty!')
                ->atPath('queryParams')
                ->addViolation();
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode(
            [
                'synchronizeFrom' => $this->hasSynchronizeFrom() ? $this->getSynchronizeFrom() : null,
                'synchronizeWith' => $this->hasSynchronizeWith() ? $this->getSynchronizeWith() : null,
                'task' => $this->hasTask() ? $this->getTask() : null,
                'query' => $this->hasQuery() ? $this->getQuery() : null,
                'queryParams' => $this->hasQueryParams() ? $this->getQueryParams() : null,
                'entityClass' => $this->hasEntityClass() ? $this->getEntityClass() : null,
            ]
        );
    }

    /**
     * @return int
     */
    public function getSynchronizeFrom(): int
    {
        return $this->synchronizeFrom;
    }

    /**
     * @return bool
     */
    public function hasSynchronizeFrom(): bool
    {
        return !empty($this->synchronizeFrom);
    }

    /**
     * @param int|User $synchronizeFrom
     *
     * @return SynchronizeTask
     */
    public function setSynchronizeFrom(int $synchronizeFrom): self
    {
        $this->synchronizeFrom = $synchronizeFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getSynchronizeWith(): int
    {
        return $this->synchronizeWith;
    }

    /**
     * @return bool
     */
    public function hasSynchronizeWith(): bool
    {
        return !empty($this->synchronizeWith);
    }

    /**
     * @param int|User $synchronizeWith
     *
     * @return SynchronizeTask
     */
    public function setSynchronizeWith(int $synchronizeWith): self
    {
        $this->synchronizeWith = $synchronizeWith;

        return $this;
    }

    /**
     * @return string
     */
    public function getTask(): string
    {
        return $this->task;
    }

    /**
     * @return bool
     */
    public function hasTask(): bool
    {
        return !empty($this->task);
    }

    /**
     * @param string $task
     *
     * @return SynchronizeTask
     */
    public function setTask(string $task): self
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @return bool
     */
    public function hasQuery(): bool
    {
        return !empty($this->query);
    }

    /**
     * @param string $query
     *
     * @return SynchronizeTask
     */
    public function setQuery(string $query): self
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @return bool
     */
    public function hasQueryParams(): bool
    {
        return !empty($this->queryParams);
    }

    /**
     * @param array|string $queryParams
     *
     * @return SynchronizeTask
     */
    public function setQueryParams(array $queryParams): self
    {
        $this->queryParams = $queryParams;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * @return bool
     */
    public function hasEntityClass(): bool
    {
        return !empty($this->entityClass);
    }

    /**
     * @param string $entityClass
     *
     * @return SynchronizeTask
     */
    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

/**
 * Interface SubscriptionInterface.
 */
interface SubscriptionInterface
{
    const STATUS_FUTURE = 10;

    const STATUS_TRIAL = 11;

    const STATUS_ACTIVE = 12;

    const STATUS_ENDING = 13;

    const STATUS_CANCELLED = 14;

    const STATUSES_LIST = [
        self::STATUS_CANCELLED,
        self::STATUS_ENDING,
        self::STATUS_ACTIVE,
        self::STATUS_FUTURE,
        self::STATUS_TRIAL,
    ];

    const ITEM_SELL = 'sell';

    const ITEM_BUY = 'buy';

    const ITEM_REPAIR = 'repair';

    const ITEMS_LIST = [
        self::ITEM_BUY,
        self::ITEM_REPAIR,
        self::ITEM_SELL,
    ];
}

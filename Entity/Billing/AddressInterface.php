<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

/**
 * Interface AddressInterface.
 */
interface AddressInterface
{
    const VALIDATION_STATUS_NOT_VALIDATED = 'not_validated';

    const VALIDATION_STATUS_VALID = 'valid';

    const VALIDATION_STATUS_PARTIAL_VALID = 'partially_valid';

    const VALIDATION_STATUS_INVALID = 'invalid';
}

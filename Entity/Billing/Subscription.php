<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Subscription.
 *
 * @ORM\Table(name="billing_subscription", indexes={
 *     @Index(name="subscription_status_idx", columns={"status"})
 * })
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Billing\SubscriptionRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class Subscription implements SubscriptionInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="charge_bee_id", type="string", unique=true, length=50, nullable=false)
     */
    private $chargeBeeId;

    /**
     * @var Plan
     *
     * @ORM\ManyToOne(
     *     targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Plan",
     *     inversedBy="subscriptions",
     *     cascade={"persist"}
     *     )
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     */
    private $plan;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Customer", inversedBy="subscriptions")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var array
     *
     * @ORM\Column(name="items", type="json_array", nullable=false)
     * @Assert\Count(min="1", max="3")
     */
    private $items;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      SubscriptionInterface::STATUS_ENDING,
     *      SubscriptionInterface::STATUS_ACTIVE,
     *      SubscriptionInterface::STATUS_TRIAL,
     *      SubscriptionInterface::STATUS_FUTURE,
     *      SubscriptionInterface::STATUS_CANCELLED
     *     }
     * )
     */
    private $status = SubscriptionInterface::STATUS_TRIAL;

    /**
     * @var int
     *
     * @ORM\Column(name="charge_interval", type="integer", nullable=false)
     */
    private $chargeInterval;

    /**
     * @var int
     *
     * @ORM\Column(name="charge_period", type="integer", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      PlanInterface::PERIOD_MONTH,
     *      PlanInterface::PERIOD_YEAR
     *     }
     * )
     */
    private $chargeIntervalUnit;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="next_charge_at", type="datetime", nullable=true)
     * @Assert\Type(type="datetime")
     */
    private $nextChargeAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_card", type="boolean", nullable=false)
     */
    private $hasCard = false;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="trial_start", type="datetime", nullable=true)
     */
    private $trialStart;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="trial_end", type="datetime", nullable=true)
     */
    private $trialEnd;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="current_period_start", type="datetime", nullable=true)
     */
    private $currentPeriodStart;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="current_period_end", type="datetime", nullable=true)
     */
    private $currentPeriodEnd;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="activated_at", type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="cancelled_at", type="datetime", nullable=true)
     */
    private $cancelledAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return DateTime
     */
    public function getCancelledAt()
    {
        return $this->cancelledAt;
    }

    /**
     * @return bool
     */
    public function hasCancelledAt(): bool
    {
        return !empty($this->cancelledAt);
    }

    /**
     * @param DateTime|int|null $cancelledAt
     *
     * @return Subscription
     */
    public function setCancelledAt($cancelledAt = null): Subscription
    {
        $this->cancelledAt = $this->convertValueToDateTimeOrThrow($cancelledAt);

        return $this;
    }

    /**
     * @param null|int|DateTime $value
     *
     * @return DateTime|null
     *
     * @throws InvalidArgumentException
     */
    protected function convertValueToDateTimeOrThrow($value = null)
    {
        $dateValue = null;

        if ($value !== null) {
            if ($value instanceof DateTime) {
                $dateValue = $value;
            } elseif (is_int($value)) {
                $dateValue = new DateTime();
                $dateValue->setTimestamp($value);
            } else {
                throw new InvalidArgumentException('$value can be initialized only as int or DateTime');
            }
        }

        return $dateValue;
    }

    /**
     * @return int
     */
    public function getChargeInterval()
    {
        return $this->chargeInterval;
    }

    /**
     * @return bool
     */
    public function hasChargeInterval(): bool
    {
        return !empty($this->chargeInterval);
    }

    /**
     * @param int $chargeInterval
     *
     * @return Subscription
     */
    public function setChargeInterval(int $chargeInterval): Subscription
    {
        $this->chargeInterval = $chargeInterval;

        return $this;
    }

    /**
     * @return int
     */
    public function getChargeIntervalUnit()
    {
        return $this->chargeIntervalUnit;
    }

    /**
     * @return bool
     */
    public function hasChargeIntervalUnit(): bool
    {
        return !empty($this->chargeIntervalUnit);
    }

    /**
     * @param int $chargeIntervalUnit
     *
     * @return Subscription
     */
    public function setChargeIntervalUnit(int $chargeIntervalUnit): Subscription
    {
        $this->chargeIntervalUnit = $chargeIntervalUnit;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTrialStart(): DateTime
    {
        return $this->trialStart;
    }

    /**
     * @return bool
     */
    public function hasTrialStart(): bool
    {
        return !empty($this->trialStart);
    }

    /**
     * @param DateTime|int $trialStart
     *
     * @return Subscription
     */
    public function setTrialStart($trialStart): Subscription
    {
        $this->trialStart = $this->convertValueToDateTimeOrThrow($trialStart);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTrialEnd(): DateTime
    {
        return $this->trialEnd;
    }

    /**
     * @return bool
     */
    public function hasTrialEnd(): bool
    {
        return !empty($this->trialEnd);
    }

    /**
     * @param DateTime|int $trialEnd
     *
     * @return Subscription
     */
    public function setTrialEnd($trialEnd): Subscription
    {
        $this->trialEnd = $this->convertValueToDateTimeOrThrow($trialEnd);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCurrentPeriodStart()
    {
        return $this->currentPeriodStart;
    }

    /**
     * @return bool
     */
    public function hasCurrentPeriodStart(): bool
    {
        return !empty($this->currentPeriodStart);
    }

    /**
     * @param DateTime|int $currentPeriodStart
     *
     * @return Subscription
     */
    public function setCurrentPeriodStart($currentPeriodStart): Subscription
    {
        $this->currentPeriodStart = $this->convertValueToDateTimeOrThrow($currentPeriodStart);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCurrentPeriodEnd()
    {
        return $this->currentPeriodEnd;
    }

    /**
     * @return bool
     */
    public function hasCurrentPeriodEnd(): bool
    {
        return !empty($this->currentPeriodEnd);
    }

    /**
     * @param DateTime|int $currentPeriodEnd
     *
     * @return Subscription
     */
    public function setCurrentPeriodEnd($currentPeriodEnd): Subscription
    {
        $this->currentPeriodEnd = $this->convertValueToDateTimeOrThrow($currentPeriodEnd);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @return bool
     */
    public function hasStartedAt(): bool
    {
        return !empty($this->startedAt);
    }

    /**
     * @param DateTime|int $startedAt
     *
     * @return Subscription
     */
    public function setStartedAt($startedAt): Subscription
    {
        $this->startedAt = $this->convertValueToDateTimeOrThrow($startedAt);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * @return bool
     */
    public function hasActivatedAt(): bool
    {
        return !empty($this->activatedAt);
    }

    /**
     * @param DateTime|int $activatedAt
     *
     * @return Subscription
     */
    public function setActivatedAt($activatedAt): Subscription
    {
        $this->activatedAt = $this->convertValueToDateTimeOrThrow($activatedAt);

        return $this;
    }

    /**
     * @return bool
     */
    public function isHasCard(): bool
    {
        return $this->hasCard;
    }

    /**
     * @return bool
     */
    public function hasHasCard(): bool
    {
        return !empty($this->hasCard);
    }

    /**
     * @param bool $hasCard
     *
     * @return Subscription
     */
    public function setHasCard(bool $hasCard): Subscription
    {
        $this->hasCard = $hasCard;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getNextChargeAt()
    {
        return $this->nextChargeAt;
    }

    /**
     * @return bool
     */
    public function hasNextChargeAt(): bool
    {
        return !empty($this->nextChargeAt);
    }

    /**
     * @param DateTime|int $nextChargeAt
     *
     * @return Subscription
     */
    public function setNextChargeAt($nextChargeAt): Subscription
    {
        $this->nextChargeAt = $this->convertValueToDateTimeOrThrow($nextChargeAt);

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return bool
     */
    public function hasCustomer(): bool
    {
        return !empty($this->customer);
    }

    /**
     * @param Customer $customer
     *
     * @return Subscription
     */
    public function setCustomer(Customer $customer): Subscription
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function hasItems(): bool
    {
        return !empty($this->items);
    }

    /**
     * @param array $items
     *
     * @return Subscription
     */
    public function setItems(array $items): Subscription
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasStatus(): bool
    {
        return !empty($this->status);
    }

    /**
     * @param int $status
     *
     * @return Subscription
     */
    public function setStatus(int $status): Subscription
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function hasCreatedAt(): bool
    {
        return !empty($this->createdAt);
    }

    /**
     * @param DateTime $createdAt
     *
     * @return Subscription
     */
    public function setCreatedAt(DateTime $createdAt): Subscription
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function hasUpdatedAt(): bool
    {
        return !empty($this->updatedAt);
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return Subscription
     */
    public function setUpdatedAt(DateTime $updatedAt): Subscription
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->hasPlan()) {
            return $this->getPlan()->getName() ?? 'Subscription';
        } else {
            return 'Subscription';
        }
    }

    /**
     * @return Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return bool
     */
    public function hasPlan(): bool
    {
        return !empty($this->plan);
    }

    /**
     * @param Plan $plan
     *
     * @return Subscription
     */
    public function setPlan(Plan $plan): Subscription
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * @return string
     */
    public function getChargeBeeId()
    {
        return $this->chargeBeeId;
    }

    /**
     * @return bool
     */
    public function hasChargeBeeId(): bool
    {
        return !empty($this->chargeBeeId);
    }

    /**
     * @param string $chargeBeeId
     *
     * @return Subscription
     */
    public function setChargeBeeId(string $chargeBeeId): Subscription
    {
        $this->chargeBeeId = $chargeBeeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return Subscription
     */
    public function setId(int $id): Subscription
    {
        $this->id = $id;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing\Embed;

use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;

/**
 * Class Upgrade.
 *
 * Upgrade form entity;
 */
class Upgrade
{
    /**
     * @var bool
     *
     * @Assert\Type(type="boolean")
     */
    private $sellingSite = true;

    /**
     * @var bool
     *
     * @Assert\Type(type="boolean")
     */
    private $buyingSite = true;

    /**
     * @var bool
     *
     * @Assert\Type(type="boolean")
     */
    private $repairingSite = true;

    /**
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Choice(
     *     choices={
     *      PlanInterface::PERIOD_MONTH,
     *      PlanInterface::PERIOD_YEAR
     *     }
     * )
     */
    private $billingPeriodUnit = PlanInterface::PERIOD_YEAR;

    /**
     * @var array
     * @Assert\Type(type="array")
     * @Assert\Valid()
     */
    private $user = [];

    /**
     * @return array
     */
    public function getUser(): array
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function hasUser(): bool
    {
        return !empty($this->user);
    }

    /**
     * @param array $user
     *
     * @return Upgrade
     */
    public function setUser(array $user): Upgrade
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSellingSite(): bool
    {
        return $this->sellingSite;
    }

    /**
     * @return bool
     */
    public function hasSellingSite(): bool
    {
        return !empty($this->sellingSite);
    }

    /**
     * @param bool $sellingSite
     *
     * @return Upgrade
     */
    public function setSellingSite(bool $sellingSite): Upgrade
    {
        $this->sellingSite = $sellingSite;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBuyingSite(): bool
    {
        return $this->buyingSite;
    }

    /**
     * @return bool
     */
    public function hasBuyingSite(): bool
    {
        return !empty($this->buyingSite);
    }

    /**
     * @param bool $buyingSite
     *
     * @return Upgrade
     */
    public function setBuyingSite(bool $buyingSite): Upgrade
    {
        $this->buyingSite = $buyingSite;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRepairingSite(): bool
    {
        return $this->repairingSite;
    }

    /**
     * @return bool
     */
    public function hasRepairingSite(): bool
    {
        return !empty($this->repairingSite);
    }

    /**
     * @param bool $repairingSite
     *
     * @return Upgrade
     */
    public function setRepairingSite(bool $repairingSite): Upgrade
    {
        $this->repairingSite = $repairingSite;

        return $this;
    }

    /**
     * @return int
     */
    public function getBillingPeriodUnit(): int
    {
        return $this->billingPeriodUnit;
    }

    /**
     * @return bool
     */
    public function hasBillingPeriodUnit(): bool
    {
        return !empty($this->billingPeriodUnit);
    }

    /**
     * @param int $billingPeriodUnit
     *
     * @return Upgrade
     */
    public function setBillingPeriodUnit(int $billingPeriodUnit): Upgrade
    {
        $this->billingPeriodUnit = $billingPeriodUnit;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Plan.
 *
 * @ORM\Table(name="billing_plan", indexes={
 *     @Index(name="plan_active_state", columns={"active"})
 * })
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Billing\PlanRepository")
 */
class Plan implements PlanInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="charge_bee_id", type="string", unique=true, length=100, nullable=false)
     */
    private $chargeBeeId;

    /**
     * @var Subscription[]
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Subscription", mappedBy="plan")
     */
    private $subscriptions;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = true;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=3, nullable=false)
     */
    private $currencyCode;

    /**
     * @var int
     *
     * @ORM\Column(name="charge_interval", type="integer", nullable=false)
     */
    private $chargeInterval;

    /**
     * @var int
     *
     * @ORM\Column(name="charge_interval_unit", type="integer", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      PlanInterface::PERIOD_YEAR,
     *      PlanInterface::PERIOD_MONTH
     *     }
     * )
     */
    private $chargeIntervalUnit;

    /**
     * @var int
     *
     * @ORM\Column(name="trial_interval", type="integer", nullable=true)
     */
    private $trialInterval;

    /**
     * @var int
     *
     * @ORM\Column(name="trial_interval_unit", type="integer", nullable=true)
     * @Assert\Choice(
     *     choices={
     *      PlanInterface::PERIOD_DAY,
     *      PlanInterface::PERIOD_MONTH
     *     }
     * )
     */
    private $trialIntervalUnit;

    /**
     * @var int
     *
     * @ORM\Column(name="setup_fee", type="integer", nullable=true)
     */
    private $setupFee;

    /**
     * @var array
     *
     * @ORM\Column(name="meta_data", type="json_array", nullable=false)
     */
    private $metaData = [];

    /**
     * @var bool
     *
     * @ORM\Column(name="has_trial", type="boolean", nullable=false)
     * @Assert\Type(type="bool")
     * @Assert\NotNull()
     */
    private $hasTrial = false;

    /**
     * @return int
     */
    public function getTrialInterval(): int
    {
        return $this->trialInterval;
    }

    /**
     * @return bool
     */
    public function hasTrialInterval(): bool
    {
        return !is_null($this->trialInterval);
    }

    /**
     * @param int|null $trialInterval
     *
     * @return Plan
     */
    public function setTrialInterval(int $trialInterval = null): Plan
    {
        $this->trialInterval = $trialInterval;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrialIntervalUnit(): int
    {
        return $this->trialIntervalUnit;
    }

    /**
     * @return bool
     */
    public function hasTrialIntervalUnit(): bool
    {
        return !is_null($this->trialIntervalUnit);
    }

    /**
     * @param int|null $trialIntervalUnit
     *
     * @return Plan
     */
    public function setTrialIntervalUnit(int $trialIntervalUnit = null): Plan
    {
        $this->trialIntervalUnit = $trialIntervalUnit;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHasTrial(): bool
    {
        return $this->hasTrial;
    }

    /**
     * @return bool
     */
    public function hasHasTrial(): bool
    {
        return !empty($this->hasTrial);
    }

    /**
     * @param bool $hasTrial
     *
     * @return Plan
     */
    public function setHasTrial(bool $hasTrial): Plan
    {
        $this->hasTrial = $hasTrial;

        return $this;
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @return bool
     */
    public function hasMetaData(): bool
    {
        return !empty($this->metaData);
    }

    /**
     * @param array $metaData
     *
     * @return Plan
     */
    public function setMetaData(array $metaData): Plan
    {
        $this->metaData = $metaData;

        return $this;
    }

    /**
     * @return int
     */
    public function getSetupFee()
    {
        return $this->setupFee;
    }

    /**
     * @return bool
     */
    public function hasSetupFee(): bool
    {
        return !empty($this->setupFee);
    }

    /**
     * @param int $setupFee
     *
     * @return Plan
     */
    public function setSetupFee(int $setupFee): Plan
    {
        $this->setupFee = $setupFee;

        return $this;
    }

    /**
     * @return int
     */
    public function getChargeInterval()
    {
        return $this->chargeInterval;
    }

    /**
     * @return bool
     */
    public function hasChargeInterval(): bool
    {
        return !empty($this->chargeInterval);
    }

    /**
     * @param int $chargeInterval
     *
     * @return Plan
     */
    public function setChargeInterval(int $chargeInterval): Plan
    {
        $this->chargeInterval = $chargeInterval;

        return $this;
    }

    /**
     * @return int
     */
    public function getChargeIntervalUnit()
    {
        return $this->chargeIntervalUnit;
    }

    /**
     * @return bool
     */
    public function hasChargeIntervalUnit(): bool
    {
        return !empty($this->chargeIntervalUnit);
    }

    /**
     * @param int $chargeIntervalUnit
     *
     * @return Plan
     */
    public function setChargeIntervalUnit(int $chargeIntervalUnit): Plan
    {
        $this->chargeIntervalUnit = $chargeIntervalUnit;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function hasActive(): bool
    {
        return !empty($this->active);
    }

    /**
     * @param bool $active
     *
     * @return Plan
     */
    public function setActive(bool $active): Plan
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return Plan
     */
    public function setId(int $id): Plan
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getChargeBeeId()
    {
        return $this->chargeBeeId;
    }

    /**
     * @return bool
     */
    public function hasChargeBeeId(): bool
    {
        return !empty($this->chargeBeeId);
    }

    /**
     * @param string $chargeBeeId
     *
     * @return Plan
     */
    public function setChargeBeeId(string $chargeBeeId): Plan
    {
        $this->chargeBeeId = $chargeBeeId;

        return $this;
    }

    /**
     * @return Subscription[]
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @return bool
     */
    public function hasSubscriptions(): bool
    {
        return !empty($this->subscriptions);
    }

    /**
     * @param Subscription[] $subscriptions
     *
     * @return Plan
     */
    public function setSubscriptions(array $subscriptions): Plan
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return !empty($this->description);
    }

    /**
     * @param string $description
     *
     * @return Plan
     */
    public function setDescription(string $description): Plan
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function hasPrice(): bool
    {
        return !empty($this->price);
    }

    /**
     * @param int $price
     *
     * @return Plan
     */
    public function setPrice(int $price): Plan
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return bool
     */
    public function hasCurrencyCode(): bool
    {
        return !empty($this->currencyCode);
    }

    /**
     * @param string $currencyCode
     *
     * @return Plan
     */
    public function setCurrencyCode(string $currencyCode): Plan
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function __toString()
    {
        return $this->getName() ?? 'Plan';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return !empty($this->name);
    }

    /**
     * @param string $name
     *
     * @return Plan
     */
    public function setName(string $name): Plan
    {
        $this->name = $name;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

/**
 * Interface PlanInterface.
 */
interface PlanInterface
{
    const PERIOD_DAY = 1;

    const PERIOD_MONTH = 2;

    const PERIOD_YEAR = 3;
}

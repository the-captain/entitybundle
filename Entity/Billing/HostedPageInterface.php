<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

/**
 * Interface HostedPageInterface.
 */
interface HostedPageInterface
{
    /**
     * checkout_new
     * Checkout new Subscription.
     */
    const TYPE_CHECKOUT_NEW = 1;

    /**
     * checkout_existing
     * Checkout existing Subscription.
     */
    const TYPE_CHECKOUT_SUBSCRIPTION = 2;

    /**
     * update_payment_method
     * Update Payment Method for a Customer.
     */
    const TYPE_UPDATE_PAYMENT_METHOD = 3;

    /**
     * manage_payment_sources
     * Manage Payments for a customer.
     */
    const TYPE_MANAGE_PAYMENT_SOURCES = 4;

    /**
     * collect_now
     * Collect Unpaid Invoices for a Customer.
     */
    const TYPE_COLLECT_NOW = 5;

    const TYPES_LIST = [
        self::TYPE_CHECKOUT_SUBSCRIPTION,
        self::TYPE_CHECKOUT_NEW,
        self::TYPE_UPDATE_PAYMENT_METHOD,
        self::TYPE_MANAGE_PAYMENT_SOURCES,
    ];

    /**
     * created
     * Indicates the hosted page is just created.
     */
    const STATE_CREATED = 1;

    /**
     * requested
     * Indicates the hosted page is requested by the website.
     */
    const STATE_REQUESTED = 2;

    /**
     * succeeded
     * Indicates the hosted page is successfully submitted by the user and response is sent to the return url.
     */
    const STATE_SUCCEEDED = 3;

    /**
     * cancelled
     * Indicates the page is cancelled by the end user after requesting it.
     */
    const STATE_CANCELLED = 4;

    /**
     * acknowledged
     * Indicates the succeeded hosted page is acknowledged.
     */
    const STATE_ACKNOWLEDGED = 5;

    const STATES_LIST = [
        self::STATE_CREATED,
        self::STATE_REQUESTED,
        self::STATE_SUCCEEDED,
        self::STATE_CANCELLED,
        self::STATE_ACKNOWLEDGED,
    ];
}

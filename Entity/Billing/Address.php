<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Address.
 *
 * @ORM\Entity()
 * @ORM\Table(name="billing_address")
 * @UniqueEntity({"customer"})
 */
class Address implements AddressInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Customer
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Customer", inversedBy="address")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\Column(nullable=true, name="first_name", type="string", length=150)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=150, nullable=true, name="last_name")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=70, nullable=true, name="email")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Email()
     * @Assert\Length(max="70")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, name="company")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=50, name="phone", nullable=true)
     *
     * @var string
     */
    private $phone;

    /**
     * Line 1 - Name of the Road ( includes block number)
     *
     * @ORM\Column(type="string", length=150, nullable=true, name="line1")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line1;

    /**
     * Line 2- Unit number # - #####
     *
     * @ORM\Column(type="string", length=150, nullable=true, name="line2")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line2;

    /**
     * Line 3 - Town, City or State
     *
     * @ORM\Column(type="string", length=150, nullable=true, name="line3")
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line3;

    /**
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $city;

    /**
     * @ORM\Column(name="state_code", type="string", length=50, nullable=true)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $stateCode;

    /**
     * @ORM\Column(name="state", type="string", length=50, nullable=true)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $state;

    /**
     * @ORM\Column(name="zip", type="string", length=20, nullable=true)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $zip;

    /**
     * @ORM\Column(nullable=true, name="country", type="string", length=50)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     * @Assert\Country()
     */
    private $country;

    /**
     * @ORM\Column(name="validation_status", type="string", nullable=false, length=30)
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(
     *     choices={
     *      AddressInterface::VALIDATION_STATUS_INVALID,
     *      AddressInterface::VALIDATION_STATUS_NOT_VALIDATED,
     *      AddressInterface::VALIDATION_STATUS_PARTIAL_VALID,
     *      AddressInterface::VALIDATION_STATUS_VALID
     *     }
     * )
     */
    private $validationStatus = AddressInterface::VALIDATION_STATUS_NOT_VALIDATED;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return Address
     */
    public function setId(int $id): Address
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return bool
     */
    public function hasCustomer(): bool
    {
        return !empty($this->customer);
    }

    /**
     * @param Customer $customer
     *
     * @return Address
     */
    public function setCustomer(Customer $customer): Address
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @param string $email
     *
     * @return Address
     */
    public function setEmail(string $email): Address
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function hasCompany(): bool
    {
        return !empty($this->company);
    }

    /**
     * @param string $company
     *
     * @return Address
     */
    public function setCompany(string $company): Address
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !empty($this->phone);
    }

    /**
     * @param string $phone
     *
     * @return Address
     */
    public function setPhone(string $phone): Address
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @return bool
     */
    public function hasLine1(): bool
    {
        return !empty($this->line1);
    }

    /**
     * @param string $line1
     *
     * @return Address
     */
    public function setLine1(string $line1): Address
    {
        $this->line1 = $line1;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @return bool
     */
    public function hasLine2(): bool
    {
        return !empty($this->line2);
    }

    /**
     * @param string $line2
     *
     * @return Address
     */
    public function setLine2(string $line2): Address
    {
        $this->line2 = $line2;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @return bool
     */
    public function hasLine3(): bool
    {
        return !empty($this->line3);
    }

    /**
     * @param string $line3
     *
     * @return Address
     */
    public function setLine3(string $line3): Address
    {
        $this->line3 = $line3;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return bool
     */
    public function hasCity(): bool
    {
        return !empty($this->city);
    }

    /**
     * @param string $city
     *
     * @return Address
     */
    public function setCity(string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }

    /**
     * @return bool
     */
    public function hasStateCode(): bool
    {
        return !empty($this->stateCode);
    }

    /**
     * @param string $stateCode
     *
     * @return Address
     */
    public function setStateCode(string $stateCode): Address
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return !empty($this->state);
    }

    /**
     * @param string $state
     *
     * @return Address
     */
    public function setState(string $state): Address
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return bool
     */
    public function hasZip(): bool
    {
        return !empty($this->zip);
    }

    /**
     * @param string $zip
     *
     * @return Address
     */
    public function setZip(string $zip = null): Address
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return bool
     */
    public function hasCountry(): bool
    {
        return !empty($this->country);
    }

    /**
     * @param string $country
     *
     * @return Address
     */
    public function setCountry(string $country = null): Address
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getValidationStatus()
    {
        return $this->validationStatus;
    }

    /**
     * @return bool
     */
    public function hasValidationStatus(): bool
    {
        return !empty($this->validationStatus);
    }

    /**
     * @param string $validationStatus
     *
     * @return Address
     */
    public function setValidationStatus(string $validationStatus = null): Address
    {
        $this->validationStatus = $validationStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $name = $this->getFirstName() . ' ' . $this->getLastName();

        return $name != ' ' ? $name : 'Address';
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !empty($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Address
     */
    public function setFirstName(string $firstName = null): Address
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !empty($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Address
     */
    public function setLastName(string $lastName = null): Address
    {
        $this->lastName = $lastName;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

/**
 * Interface CustomerInterface.
 */
interface CustomerInterface
{
    const CARD_STATUS_NONE = 0;

    const CARD_STATUS_EXPIRING = 5;

    const CARD_STATUS_EXPIRED = 7;

    const CARD_STATUS_VALID = 10;

    const CARD_STATUSES_LIST = [
        self::CARD_STATUS_VALID,
        self::CARD_STATUS_NONE,
        self::CARD_STATUS_EXPIRING,
        self::CARD_STATUS_EXPIRED,
    ];
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class Customer.
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Billing\CustomerRepository")
 * @ORM\Table(name="billing_customer")
 * @UniqueEntity({"user"})
 * @ORM\HasLifecycleCallbacks()
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="charge_bee_id", type="string", unique=true, length=50, nullable=false)
     */
    private $chargeBeeId;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="billingCustomer")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Address
     *
     * @ORM\OneToOne(
     *     targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Address",
     *     mappedBy="customer",
     *     cascade={"persist"}
     * )
     *
     * @Assert\Valid()
     */
    private $address;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Subscription", mappedBy="customer")
     */
    private $subscriptions;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\HostedPage", mappedBy="customer")
     */
    private $hostedPages;

    /**
     * @var int
     *
     * @ORM\Column(name="card_status", type="integer", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      CustomerInterface::CARD_STATUS_EXPIRED,
     *      CustomerInterface::CARD_STATUS_EXPIRING,
     *      CustomerInterface::CARD_STATUS_NONE,
     *      CustomerInterface::CARD_STATUS_VALID
     *     }
     * )
     */
    private $cardStatus = CustomerInterface::CARD_STATUS_NONE;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="trial_started_at", type="datetime", nullable=true)
     */
    private $trialStartedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="trial_ended_at", type="datetime", nullable=true)
     */
    private $trialEndedAt;

    /**
     * @return DateTime
     */
    public function getTrialStartedAt(): DateTime
    {
        return $this->trialStartedAt;
    }

    /**
     * @return bool
     */
    public function hasTrialStartedAt(): bool
    {
        return !is_null($this->trialStartedAt);
    }

    /**
     * @param DateTime|null $trialStartedAt
     *
     * @return Customer
     */
    public function setTrialStartedAt(DateTime $trialStartedAt = null): Customer
    {
        $this->trialStartedAt = $trialStartedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTrialPeriodAvailable(): bool
    {
        if ($this->hasTrialEndedAt() === false) {
            return false;
        }

        return $this->getTrialEndedAt() >= (new DateTime());
    }

    /**
     * @return DateTime
     */
    public function getTrialEndedAt(): DateTime
    {
        return $this->trialEndedAt;
    }

    /**
     * @return bool
     */
    public function hasTrialEndedAt(): bool
    {
        return !is_null($this->trialEndedAt);
    }

    /**
     * @param DateTime|null $trialEndedAt
     *
     * @return Customer
     */
    public function setTrialEndedAt(DateTime $trialEndedAt = null): Customer
    {
        $this->trialEndedAt = $trialEndedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function haveValidCard(): bool
    {
        if (false === $this->hasCardStatus()) {
            return false;
        }

        return $this->getCardStatus() === CustomerInterface::CARD_STATUS_VALID;
    }

    /**
     * @return int
     */
    public function getCardStatus()
    {
        return $this->cardStatus;
    }

    /**
     * @return bool
     */
    public function hasCardStatus(): bool
    {
        return !empty($this->cardStatus);
    }

    /**
     * @param int $cardStatus
     *
     * @return Customer
     */
    public function setCardStatus(int $cardStatus): Customer
    {
        $this->cardStatus = $cardStatus;

        return $this;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return bool
     */
    public function hasAddress(): bool
    {
        return !empty($this->address);
    }

    /**
     * @param Address $address
     *
     * @return Customer
     */
    public function setAddress(Address $address): Customer
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param int $id
     *
     * @return Customer
     */
    public function setId(int $id): Customer
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function hasUser(): bool
    {
        return !empty($this->user);
    }

    /**
     * @param User $user
     *
     * @return Customer
     */
    public function setUser(User $user): Customer
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @return bool
     */
    public function hasSubscriptions(): bool
    {
        return !empty($this->subscriptions);
    }

    /**
     * @param Collection $subscriptions
     *
     * @return Customer
     */
    public function setSubscriptions(Collection $subscriptions): Customer
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getChargeBeeId() ?? 'Customer';
    }

    /**
     * @return string
     */
    public function getChargeBeeId()
    {
        return $this->chargeBeeId;
    }

    /**
     * @return bool
     */
    public function hasChargeBeeId(): bool
    {
        return !empty($this->chargeBeeId);
    }

    /**
     * @param string $chargeBeeId
     *
     * @return Customer
     */
    public function setChargeBeeId(string $chargeBeeId): Customer
    {
        $this->chargeBeeId = $chargeBeeId;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setTrialWhileCreating()
    {
        $this
            ->setTrialStartedAt(new DateTime())
            ->setTrialEndedAt(new DateTime());
    }

    /**
     * @return null|Subscription
     */
    public function getSubscription(): ?Subscription
    {
        /** @var Subscription $subscription */
        foreach ($this->subscriptions as $subscription) {
            return $subscription;
        }

        return null;
    }

    /**
     * @return null|Subscription
     */
    public function getActiveSubscription(): ?Subscription
    {
        /** @var Subscription $subscription */
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->getStatus() === SubscriptionInterface::STATUS_ACTIVE)
            return $subscription;
        }

        return null;
    }

    /**
     * @return null|Subscription
     */
    public function getTrialSubscription()
    {
        /** @var Subscription $subscription */
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->getStatus() === Subscription::STATUS_TRIAL) {
                return $subscription;
            }
        }

        return null;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Billing;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class HostedPage.
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Billing\HostedPageRepository")
 * @ORM\Table(name="billing_hosted_page", indexes={
 *     @Index(name="search_main", columns={"customer_id", "type", "entity_id"})
 *     })
 * @ORM\HasLifecycleCallbacks()
 */
class HostedPage implements HostedPageInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="charge_bee_id", type="string", nullable=false, unique=true)
     */
    private $chargeBeeId;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Billing\Customer", inversedBy="hostedPages")
     * @ORM\JoinColumn(referencedColumnName="id", name="customer_id")
     */
    private $customer;

    /**
     * @var int
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=false)
     */
    private $entityId = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="type", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      HostedPageInterface::TYPE_CHECKOUT_NEW,
     *      HostedPageInterface::TYPE_CHECKOUT_SUBSCRIPTION,
     *      HostedPageInterface::TYPE_UPDATE_PAYMENT_METHOD,
     *      HostedPageInterface::TYPE_MANAGE_PAYMENT_SOURCES,
     *      HostedPageInterface::TYPE_COLLECT_NOW,
     *     }
     * )
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="state", nullable=false)
     * @Assert\Choice(
     *     choices={
     *      HostedPageInterface::STATE_ACKNOWLEDGED,
     *      HostedPageInterface::STATE_CANCELLED,
     *      HostedPageInterface::STATE_SUCCEEDED,
     *      HostedPageInterface::STATE_REQUESTED,
     *      HostedPageInterface::STATE_CREATED
     *     }
     * )
     */
    private $state;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, name="created_at")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true, name="expires_at")
     */
    private $expiresAt;

    /**
     * @var array
     *
     * @ORM\Column(nullable=false, name="payload", type="array")
     */
    private $payload;

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @return bool
     */
    public function hasPayload(): bool
    {
        return !is_null($this->payload);
    }

    /**
     * @param array|null $payload
     *
     * @return HostedPage
     */
    public function setPayload(array $payload = null): HostedPage
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @return bool
     */
    public function hasEntityId(): bool
    {
        return !is_null($this->entityId);
    }

    /**
     * @param int|null $entityId
     *
     * @return HostedPage
     */
    public function setEntityId(int $entityId = null): HostedPage
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return bool
     */
    public function hasCustomer(): bool
    {
        return !is_null($this->customer);
    }

    /**
     * @param Customer|null $customer
     *
     * @return HostedPage
     */
    public function setCustomer(Customer $customer = null): HostedPage
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param int|null $id
     *
     * @return HostedPage
     */
    public function setId(int $id = null): HostedPage
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getChargeBeeId(): string
    {
        return $this->chargeBeeId;
    }

    /**
     * @return bool
     */
    public function hasChargeBeeId(): bool
    {
        return !is_null($this->chargeBeeId);
    }

    /**
     * @param string|null $chargeBeeId
     *
     * @return HostedPage
     */
    public function setChargeBeeId(string $chargeBeeId = null): HostedPage
    {
        $this->chargeBeeId = $chargeBeeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @param int|null $type
     *
     * @return HostedPage
     */
    public function setType(int $type = null): HostedPage
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return !is_null($this->state);
    }

    /**
     * @param int|null $state
     *
     * @return HostedPage
     */
    public function setState(int $state = null): HostedPage
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function hasCreatedAt(): bool
    {
        return !is_null($this->createdAt);
    }

    /**
     * @param DateTime|null $createdAt
     *
     * @return HostedPage
     */
    public function setCreatedAt(DateTime $createdAt = null): HostedPage
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     *
     * @return void
     */
    public function setDates()
    {
        $this->setCreatedAt(new DateTime());
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        if ($this->hasExpiresAt() === false) {
            return false;
        }

        return (new DateTime()) >= $this->getExpiresAt();
    }

    /**
     * @return DateTime
     */
    public function getExpiresAt(): DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @return bool
     */
    public function hasExpiresAt(): bool
    {
        return !is_null($this->expiresAt);
    }

    /**
     * @param DateTime|null $expiresAt
     *
     * @return HostedPage
     */
    public function setExpiresAt(DateTime $expiresAt = null): HostedPage
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }
}

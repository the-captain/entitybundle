<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Sluggable\Sluggable;
use ThreeWebOneEntityBundle\Sluggable\SluggableInterface;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Category
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Category extends BaseEntity implements
    SluggableInterface,
    StatusInterface,
    OwnerInterface,
    SynchronizeableInterface
{
    use StatusTrait, Sluggable;

    /**
     * Undefined Type
     */
    const TYPE_UNDEFINED = 0;

    /**
     * Type Category
     */
    const TYPE_CATEGORY = 1;
    const CATEGORY_NAME = 'category';

    /**
     * Type Product
     */
    const TYPE_PRODUCT = 2;
    const PRODUCT_NAME = '2nd-category';

    /**
     * Type Brand
     */
    const TYPE_PROVIDER = 3;
    const PROVIDER_NAME = '3rd-category';

    const TYPES_ARRAY = [
        self::TYPE_CATEGORY => self::CATEGORY_NAME,
        self::TYPE_PRODUCT => self::PRODUCT_NAME,
        self::TYPE_PROVIDER => self::PROVIDER_NAME,
    ];

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $title;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Model", mappedBy="categories")
     * @ORM\JoinTable(name="model_categories")
     */
    protected $models;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="categories")
     * @ORM\JoinTable(name="user_categories")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\CategoryImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\PopularSearch", mappedBy="category", cascade={"persist"}, orphanRemoval=true)
     */
    protected $popularSearches;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userCategories")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="category")
     */
    protected $inventoryItems;

    /**
     * @ORM\OneToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Seo", mappedBy="category")
     */
    protected $seoMeta;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->popularSearches = new ArrayCollection();
    }

    /**
     * Gets Image
     *
     * @return Image | false
     */
    public function getImage()
    {
        if (!empty($this->getImages())) {
            return $this->getImages()[0];
        }

        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Category
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add child
     *
     * @param Category $child
     *
     * @return Category
     */
    public function addChild(Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param Category $child
     */
    public function removeChild(Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     *
     * @return Category
     */
    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add model
     *
     * @param Model $model
     *
     * @return Category
     */
    public function addModel(Model $model)
    {
        $this->models->add($model);

        return $this;
    }

    /**
     * Remove model
     *
     * @param Model $model
     *
     * @return $this
     */
    public function removeModel(Model $model)
    {
        $this->models->removeElement($model);

        return $this;
    }

    /**
     * Get models
     *
     * @return Collection
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: 'Category';
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Category
     */
    public function addUser(User $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     *
     * @return $this
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * Get users
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add image
     *
     * @param CategoryImage $image
     *
     * @return Category
     */
    public function addImage(CategoryImage $image)
    {
        $this->images->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove image
     *
     * @param CategoryImage $image
     *
     * @return $this
     */
    public function removeImage(CategoryImage $image)
    {
        $this->images->removeElement($image);
        $image->setEntity(null);

        return $this;
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set owner
     *
     * @param User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Creates slug for entity
     *
     * @return string
     */
    public function createSlug() :string
    {
        return $this->title;
    }

    /**
     * get inventory items
     *
     * @return Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * Category clone
     */
    public function __clone()
    {
        parent::__clone();
        $this->images = new ArrayCollection();
        $this->setParent(null);
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->seoMeta = null;
    }

    /**
     * @return Seo
     */
    public function getSeoMeta()
    {
        return $this->seoMeta;
    }

    /**
     * @param Seo|null $seo
     * @return Category
     */
    public function setSeoMeta(Seo $seo = null)
    {
        $this->seoMeta = $seo;
        if ($seo) {
            $seo->setCategory($this);
        }

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;

/**
 * Class Model
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="fos_user_group")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\GroupRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Group extends BaseGroup
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}

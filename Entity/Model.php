<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\CloneableInterface;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\FilterTrait;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Sluggable\Sluggable;
use ThreeWebOneEntityBundle\Sluggable\SluggableInterface;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Model
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="models")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\ModelRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Model extends BaseEntity implements SluggableInterface, StatusInterface, OwnerInterface, SynchronizeableInterface
{
    use StatusTrait, Sluggable, CreatedAtTrait, FilterTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Category", inversedBy="models")
     * @ORM\JoinTable(name="model_categories")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Price", mappedBy="model", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $prices;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Filter", inversedBy="models")
     * @ORM\JoinTable(name="filter_models")
     */
    protected $filters;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\ModelImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="model")
     */
    private $inventoryItems;

    /**
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="models")
     * @ORM\JoinTable(name="user_models")
     */
    protected $users;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userModels")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Seo", mappedBy="model",
     *                                                                              cascade={"persist"})
     */
    protected $seoMetas;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->seoMetas = new ArrayCollection();
    }

    /**
     * Gets Image
     *
     * @return Image | false
     */
    public function getImage()
    {
        if (!empty($this->getImages())) {
            return $this->getImages()[0];
        }

        return false;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function dateUpdate()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Model
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Model
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add category
     *
     * @param Category $category
     *
     * @return Model
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);

        return $this;
    }

    /**
     * Remove category
     *
     * @param Category $category
     *
     * @return Model
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Get categories
     *
     * @return ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param int $type
     * @return null|Category
     */
    public function getCategoryByType(int $type)
    {
        /** @var Category $category */
        foreach ($this->categories as $category) {
            if ($category->getType() === $type) {
                return $category;
            }
        }

        return null;
    }

    /**
     * @param int $type
     * @param null|Category $category
     *
     * @return $this
     */
    public function setCategoryByType(int $type, ?Category $category)
    {
        $oldCategory = $this->getCategoryByType($type);
        if ($oldCategory) {
            $this->categories->removeElement($oldCategory);
        }
        if ($category) {
            $this->categories->add($category);
        }

        return $this;
    }

    /**
     * @return null|Category
     */
    public function getProvider()
    {
        return $this->getCategoryByType(Category::TYPE_PROVIDER);
    }

    /**
     * @return null|Category
     */
    public function getCategory()
    {
        return $this->getCategoryByType(Category::TYPE_CATEGORY);
    }

    /**
     * @return null|Category
     */
    public function getProduct()
    {
        return $this->getCategoryByType(Category::TYPE_PRODUCT);
    }

    /**
     * Add price
     *
     * @param Price $price
     *
     * @return Model
     */
    public function addPrice(Price $price)
    {
        $this->prices->add($price);
        $price->setModel($this);

        return $this;
    }

    /**
     * Remove price
     *
     * @param Price $price
     */
    public function removePrice(Price $price)
    {
        $this->prices->removeElement($price);
        $price->setModel(null);
    }

    /**
     * Get prices
     *
     * @return Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set prices
     *
     * @param Collection $prices
     *
     * @return Model
     */
    public function setPrices(Collection $prices)
    {
        $this->prices = $prices;

        return $this;
    }

    /**
     * Add filter
     *
     * @param Filter $filter
     *
     * @return Model
     */
    public function addFilter(Filter $filter)
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * Remove filter
     *
     * @param Filter $filter
     */
    public function removeFilter(Filter $filter)
    {
        $this->filters->removeElement($filter);
    }

    /**
     * Get filters
     *
     * @return Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Add image
     *
     * @param ModelImage $image
     *
     * @return Model
     */
    public function addImage(ModelImage $image)
    {
        $this->images->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove image
     *
     * @param ModelImage $image
     */
    public function removeImage(ModelImage $image)
    {
        $this->images->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: 'Model';
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Model
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     *
     * @return $this
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * Get users
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set owner
     *
     * @param User
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Creates slug for entity
     *
     * @return string
     */
    public function createSlug() :string
    {
        return $this->title;
    }

    /**
     * Model clone
     */
    public function __clone()
    {
        parent::__clone();
        $this->images = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
        $this->seoMetas = new ArrayCollection();
    }

    /**
     * Add inventoryItem
     *
     * @param InventoryItem $inventoryItem
     *
     * @return Model
     */
    public function addInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->add($inventoryItem);

        return $this;
    }

    /**
     * Remove inventoryItem
     *
     * @param InventoryItem $inventoryItem
     */
    public function removeInventoryItem(InventoryItem $inventoryItem)
    {
        $this->inventoryItems->removeElement($inventoryItem);
    }

    /**
     * Get inventoryItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryItems()
    {
        return $this->inventoryItems;
    }

    /**
     * @param Seo $seo
     * @return Model
     */
    public function addSeoMeta(Seo $seo)
    {
        $this->seoMetas->add($seo);

        return $this;
    }

    /**
     * @param Seo $seo
     * @return Model
     */
    public function removeSeoMeta(Seo $seo)
    {
        $this->seoMetas->removeElement($seo);

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeoMetas()
    {
        return $this->seoMetas;
    }
}

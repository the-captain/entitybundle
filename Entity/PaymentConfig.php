<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\StatusTrait;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;

/**
 * Class Advertise.
 *
 *
 * @ORM\Table(name="payment_configs")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\PaymentConfigRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PaymentConfig implements OrderPaymentTypeInterface, OwnerInterface
{
    use StatusTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_type", type="smallint")
     */
    protected $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=64, nullable=true)
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", nullable=true)
     */
    protected $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="client_secret", type="string", nullable=true)
     */
    protected $clientSecret;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="userPaymentConfigs")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * PaymentConfig constructor.
     */
    public function __construct()
    {
        $this->status = self::STATUS_INACTIVE;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return mixed
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Helper;

interface SiteTypeInterface
{
    /**
     * Selling Site Type
     * @TODO: Change SALE to SELL
     */
    const SALE = 1;

    const SALE_NAME = 'sell';

    /**
     * BuyBack Site Type
     */
    const BUYBACK = 2;

    const BUYBACK_NAME = 'buyback';

    /**
     * Repair Site Type
     */
    const REPAIR = 3;

    const REPAIR_NAME = 'repair';

    const SITE_TYPE_ARRAY = [
        self::SALE_NAME => self::SALE,
        self::BUYBACK_NAME => self::BUYBACK,
        self::REPAIR_NAME => self::REPAIR,
    ];
}

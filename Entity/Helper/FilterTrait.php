<?php

namespace ThreeWebOneEntityBundle\Entity\Helper;

use ThreeWebOneEntityBundle\Entity\Filter;

trait FilterTrait
{
    /**
     * Get provider filter
     *
     * @return Filter|null
     */
    public function getProvider()
    {
        return $this->getFilterByType(Filter::TYPE_PROVIDER);
    }

    /**
     * Get capacity filter
     *
     * @return Filter|null
     */
    public function getCapacity()
    {
        return $this->getFilterByType(Filter::TYPE_CAPACITY);
    }

    /**
     * Get color filter
     *
     * @return Filter|null
     */
    public function getColor()
    {
        return $this->getFilterByType(Filter::TYPE_COLOR);
    }

    /**
     * Get color filter
     *
     * @return Filter|null
     */
    public function getAccessories()
    {
        return $this->getFilterByType(Filter::TYPE_ACCESSORY, true);
    }

    /**
     * Get color filter
     *
     * @return Filter|null
     */
    public function getBrand()
    {
        return $this->getFilterByType(Filter::TYPE_BRAND);
    }

    /**
     * Get Filter by type
     *
     * @param int $type
     * @param bool $all
     *
     * @return Filter|array
     */
    public function getFilterByType(int $type, $all = false)
    {
        $result = [];
        foreach ($this->filters as $filter) {
            if ($filter->getType() === $type) {
                if ($all) {
                    $result[] = $filter;
                } else {
                    return $filter;
                }
            }
        }

        return $result;
    }
}

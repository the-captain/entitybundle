<?php

namespace ThreeWebOneEntityBundle\Entity\Helper;

use ThreeWebOneEntityBundle\Entity\User;

/**
 * Interface OwnerInterface.
 */
interface OwnerInterface
{
    /**
     * @return User
     */
    public function getOwner(): User;

    /**
     * @param User $user
     *
     * @return OwnerInterface
     */
    public function setOwner(User $user);
}

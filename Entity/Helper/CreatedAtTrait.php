<?php

namespace ThreeWebOneEntityBundle\Entity\Helper;

use Symfony\Component\Validator\Constraints as Assert;

trait CreatedAtTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime
     */
    protected $createdAt;

    /**
     * Set creation time.
     *
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $date)
    {
        $this->createdAt = $date;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get creation time.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

}

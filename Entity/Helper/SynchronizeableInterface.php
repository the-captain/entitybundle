<?php

namespace ThreeWebOneEntityBundle\Entity\Helper;

use ThreeWebOneEntityBundle\Entity\User;

/**
 * Interface SynchronizeableInterface.
 */
interface SynchronizeableInterface
{
    /**
     * @return User[]
     */
    public function getUsers();

    /**
     * @param User $user
     *
     * @return SynchronizeableInterface
     */
    public function addUser(User $user);

    /**
     * @param User $user
     *
     * @return bool
     */
    public function removeUser(User $user);
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Helper\Sequential;

/**
 * Interface OwnerInterface.
 */
trait SequentialTrait
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="store_id", type="integer", nullable=true)
     */
    protected $storeId;

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function setStoreId(?int $storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }
}

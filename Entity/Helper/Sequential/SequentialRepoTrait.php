<?php

namespace ThreeWebOneEntityBundle\Entity\Helper\Sequential;

use ThreeWebOneEntityBundle\Entity\User;
use Doctrine\DBAL\LockMode;

/**
 * Interface OwnerInterface.
 */
trait SequentialRepoTrait
{
    /**
     * @param User $user
     * @return null|SequentialInterface
     */
    public function findLastSequentialItem(User $user)
    {
        $qb = $this->createQueryBuilder('item')
            ->where('item.owner = :user')
            ->setParameter('user', $user)
            ->orderBy('item.storeId', 'desc')
            ->setMaxResults(1);

        return $qb->getQuery()->setLockMode(LockMode::PESSIMISTIC_WRITE)->getResult()[0] ?? null;
    }
}

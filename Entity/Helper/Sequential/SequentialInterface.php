<?php

namespace ThreeWebOneEntityBundle\Entity\Helper\Sequential;

/**
 * Interface OwnerInterface.
 */
interface SequentialInterface
{
    /**
     * @param int|null $storeId
     */
    public function setStoreId(?int $storeId);

    /**
     * @return int|null
     */
    public function getStoreId();
}

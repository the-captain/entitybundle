<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class CategoryImage.
 *
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class AdvertiseImage extends Image
{
    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Advertise", inversedBy="images")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * @Vich\UploadableField(mapping="advertise_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * Set Advertise/Item.
     *
     * @param null $entity
     *
     * @return AdvertiseImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if ((null !== $entity) && !($entity instanceof Advertise)) {
            throw new EntityNotFoundException('Entity should be instance of Advertise class');
        }
        $this->entity = $entity;

        return $this;
    }
}

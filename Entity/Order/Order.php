<?php

namespace ThreeWebOneEntityBundle\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\HttpFoundation\File\File;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialInterface;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialTrait;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Helper\StatusTrait;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Order
 *
 * @ORM\Table(name="orders",
 *      uniqueConstraints={
 *        @UniqueConstraint(name="owner_store_id",
 *            columns={"owner_id", "store_id"})
 *    })
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Order\OrderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Order implements
    OrderStatusInterface,
    OrderDeliveryTypeInterface,
    OrderPaymentTypeInterface,
    SiteTypeInterface,
    OwnerInterface,
    SequentialInterface
{
    use CreatedAtTrait, UpdatedAtTrait, StatusTrait, SequentialTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="delivery_type", type="smallint")
     */
    protected $deliveryType;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_type", type="smallint")
     */
    protected $paymentType;

    /**
     * @var int
     *
     * @ORM\Column(name="site_type", type="smallint")
     */
    protected $siteType;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", mappedBy="orders", cascade={"persist"})
     * @ORM\JoinTable(name="orders_inventory_items")
     */
    protected $items;

    /**
     * @var float
     *
     * @ORM\Column(name="amount_paid", type="decimal", scale=0, nullable=true)
     */
    protected $amountPaid;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode", mappedBy="saleOrder", cascade={"persist", "remove"})
     */
    protected $saleBarcodes;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode", mappedBy="purchaseOrder", cascade={"persist", "remove"})
     */
    protected $purchaseBarcodes;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Customer\Customer", inversedBy="orders")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="userOrders")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * @Vich\UploadableField(mapping="signature_image", fileNameProperty="imageName")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\Column(name="signature_image_name", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $imageName;

    /**
     * @var array
     *
     * @ORM\Column(name="asked_quantity", type="array", nullable=true)
     */
    protected $askedQuantity;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_note", type="text", nullable=true)
     */
    protected $adminNote;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_terms_checked", type="boolean", nullable=false)
     */
    protected $termsChecked;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="external_id", type="string", length=255, nullable=true)
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address", type="string", length=255, nullable=true)
     */
    private $shippingAddress;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\UserConfig\Location")
     * @ORM\JoinColumn(name="pickup_location_id", referencedColumnName="id")
     */
    private $pickupLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255, nullable=true)
     */
    private $zip;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->saleBarcodes = new ArrayCollection();
        $this->purchaseBarcodes = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @return bool
     */
    public function hasExternalId(): bool
    {
        return !is_null($this->externalId);
    }

    /**
     * @param string|null $externalId
     *
     * @return Order
     */
    public function setExternalId(string $externalId = null): Order
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function refreshUpdated()
    {
        $this->setUpdatedAt(new \DateTime);
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone.
     *
     * @param int $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get zip.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set zip.
     *
     * @param string $zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     *
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setOwner(User $user)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * @return InventoryItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param InventoryItem $item
     */
    public function addItem(InventoryItem $item)
    {
        $this->items->add($item);
        $item->addOrder($this);
    }

    /**
     * @param InventoryItem $item
     *
     * @return $this
     */
    public function removeItem(InventoryItem $item)
    {
        $this->items->removeElement($item);
        $item->removeOrder($this);

        return $this;
    }

    /**
     * @return InventoryItemBarcode[]
     */
    public function getSaleBarcodes()
    {
        return $this->saleBarcodes;
    }

    /**
     * @param InventoryItemBarcode $item
     */
    public function addSaleBarcode(InventoryItemBarcode $item)
    {
        $this->saleBarcodes->add($item);
        $item->setSaleOrder($this);
    }

    /**
     * @param InventoryItemBarcode $item
     *
     * @return $this
     */
    public function removeSaleBarcode(InventoryItemBarcode $item)
    {
        $this->saleBarcodes->removeElement($item);
        $item->setSaleOrder(null);

        return $this;
    }

    /**
     * @return InventoryItemBarcode[]
     */
    public function getPurchaseBarcodes()
    {
        return $this->purchaseBarcodes;
    }

    /**
     * @param InventoryItemBarcode $item
     */
    public function addPurchaseBarcode(InventoryItemBarcode $item)
    {
        $this->purchaseBarcodes->add($item);
        $item->setPurchaseOrder($this);
        $item->setStatus(InventoryItemBarcode::LISTED);
    }

    /**
     * @param InventoryItemBarcode $item
     *
     * @return $this
     */
    public function removePurchaseBarcode(InventoryItemBarcode $item)
    {
        $this->purchaseBarcodes->removeElement($item);
        $item->setPurchaseOrder(null);
        $item->setStatus(InventoryItemBarcode::NEW_BARCODE);

        return $this;
    }

    /**
     * Get deliveryType.
     *
     * @return int
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * Set deliveryType.
     *
     * @param int $deliveryType
     *
     * @return $this
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;

        return $this;
    }

    /**
     * Get paymentType.
     *
     * @return int
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set paymentType.
     *
     * @param int $paymentType
     *
     * @return $this
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get siteType.
     *
     * @return int
     */
    public function getSiteType()
    {
        return $this->siteType;
    }

    /**
     * Set siteType.
     *
     * @param int $siteType
     *
     * @return $this
     */
    public function setSiteType($siteType)
    {
        $this->siteType = $siteType;

        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param string $shippingAddress
     *
     * @return $this
     */
    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * @return Location
     */
    public function getPickupLocation()
    {
        return $this->pickupLocation;
    }

    /**
     * @param Location $pickupLocation
     * @return $this
     */
    public function setPickupLocation(Location $pickupLocation)
    {
        $this->pickupLocation = $pickupLocation;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Order
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     *
     * @return Order
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return array
     */
    public function getAskedQuantity()
    {
        return $this->askedQuantity;
    }

    /**
     * @param array $askedQuantity
     *
     * @return Order
     */
    public function setAskedQuantity(array $askedQuantity)
    {
        $this->askedQuantity = $askedQuantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdminNote()
    {
        return $this->adminNote;
    }

    /**
     * @param string $adminNote
     *
     * @return Order
     */
    public function setAdminNote($adminNote)
    {
        $this->adminNote = $adminNote;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * @param float $amountPaid
     *
     * @return Order
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Order ' . $this->getId();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Is Terms Checked
     *
     * @return null|bool
     */
    public function isTermsChecked(): ?bool
    {
        return $this->termsChecked;
    }

    /**
     * Set Terms Checked
     *
     * @param bool $flag
     * @return Order
     */
    public function setTermsChecked(bool $flag): Order
    {
        $this->termsChecked = $flag;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Order;

interface OrderPaymentTypeInterface
{
    /**
     * Type cash
     */
    const CASH = 1;
    const CASH_NAME = 'Cash';

    /**
     * Type credit card
     */
    const CREDIT_CARD = 2;
    const CREDIT_CARD_NAME = 'Credit card';

    /**
     * Type check
     */
    const CHECK = 3;
    const CHECK_NAME = 'Check';

    /**
     * Type paypal
     */
    const PAYPAL = 4;
    const PAYPAL_NAME = 'Paypal';

    const PAYMENT_TYPE_ARRAY = [
        self::CASH_NAME => self::CASH,
        self::CREDIT_CARD_NAME => self::CREDIT_CARD,
        self::CHECK_NAME => self::CHECK,
        self::PAYPAL_NAME => self::PAYPAL,
    ];

    /**
     * @return integer
     */
    public function getPaymentType();

    /**
     * @param integer $paymentType
     * @return $this
     */
    public function setPaymentType($paymentType);
}

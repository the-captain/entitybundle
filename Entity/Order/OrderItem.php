<?php

namespace ThreeWebOneEntityBundle\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Order Item
 * @package ThreeWebOneEntityBundle\Entity
 *
 * @ORM\Table(name="order_items")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Order\OrderItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderItem
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_note", type="text", nullable=true)
     */
    protected $adminNote;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem", inversedBy="OrderItems")
     * @ORM\JoinColumn(name="inventory_item_id", referencedColumnName="id")
     */
    private $inventoryItem;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode", inversedBy="OrderItems")
     * @ORM\JoinColumn(name="barcode_id", referencedColumnName="id")
     */
    private $barcode;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return OrderItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdminNote()
    {
        return $this->adminNote;
    }

    /**
     * @param string $adminNote
     *
     * @return OrderItem
     */
    public function setAdminNote($adminNote)
    {
        $this->adminNote = $adminNote;

        return $this;
    }

    /**
     * @return InventoryItem
     */
    public function getInventoryItem()
    {
        return $this->inventoryItem;
    }

    /**
     * @param InventoryItem $inventoryItem
     *
     * @return OrderItem
     */
    public function setInventoryItem($inventoryItem)
    {
        $this->inventoryItem = $inventoryItem;

        return $this;
    }

    /**
     * @return InventoryItemBarcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param InventoryItemBarcode $barcode
     *
     * @return OrderItem
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity\Order;

interface OrderDeliveryTypeInterface
{
    /**
     * Delivery type pick up
     */
    const PICK_UP = 1;
    const PICK_UP_NAME = 'Pick up';

    /**
     * Delivery type shipping
     */
    const SHIPPING = 2;
    const SHIPPING_NAME = 'Shipping';

    /**
     * Delivery type drop off
     */
    const DROP_OFF = 3;
    const DROP_OFF_NAME = 'Drop-off';

    const DELIVERY_TYPE_ARRAY = [
        self::PICK_UP_NAME => self::PICK_UP,
        self::SHIPPING_NAME => self::SHIPPING,
        self::DROP_OFF_NAME => self::DROP_OFF,
    ];

    /**
     * @return integer
     */
    public function getDeliveryType();

    /**
     * @param integer $deliveryType
     * @return $this
     */
    public function setDeliveryType($deliveryType);
}

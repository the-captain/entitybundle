<?php

namespace ThreeWebOneEntityBundle\Entity\Order;

interface OrderStatusInterface
{
    const NEW_ORDER = 0;
    const IN_PROGRESS = 1;
    const COMPLETED = 2;
    const REJECTED = 3;

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param integer $status
     * @return $this
     */
    public function setStatus($status);
}

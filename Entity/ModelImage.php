<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ModelImage
 *
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\ModelImageRepository")
 * @Vich\Uploadable()
 */
class ModelImage extends Image
{
    /**
     * @Vich\UploadableField(mapping="model_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\Model", inversedBy="images")
     * @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     */
    protected $entity;

    /**
     * Set Model
     *
     * @param null $entity
     *
     * @return ModelImage $this
     *
     * @throws EntityNotFoundException
     */
    public function setEntity($entity = null)
    {
        if (($entity !== null) && !($entity instanceof Model)) {
            throw new EntityNotFoundException('Entity should be instance of Model class');
        }
        $this->entity = $entity;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getEntity()->getTitle() ?? parent::__toString();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Accessory.
 *
 * @ORM\Table(name="accessory")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\AccessoryRepository")
 */
class Accessory extends BaseEntity implements StatusInterface, OwnerInterface, SynchronizeableInterface
{
    use StatusTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50, minMessage="Title is too short", maxMessage="Title is too long")
     */
    protected $title;

    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="AccessoryImage", mappedBy="entity", cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;

    /**
     * @ORM\OneToMany(targetEntity="Accessory", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Accessory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userAccessories")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Accessory constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    /**
     * Gets Image.
     *
     * @return Image | false
     */
    public function getImage()
    {
        if (!empty($this->getImages())) {
            return $this->getImages()[0];
        }

        return false;
    }

    /**
     * Get images.
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add child.
     *
     * @param Accessory $child
     *
     * @return Accessory
     */
    public function addChild(self $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param Accessory $child
     */
    public function removeChild(self $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent.
     *
     * @return Accessory
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent.
     *
     * @param Accessory $parent
     *
     * @return Accessory
     */
    public function setParent(self $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle() ?: 'Accessory';
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Accessory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Add user.
     *
     * @param User $user
     *
     * @return Accessory
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users.
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add image.
     *
     * @param AccessoryImage $image
     *
     * @return Accessory
     */
    public function addImage(AccessoryImage $image)
    {
        $this->images->add($image);
        $image->setEntity($this);

        return $this;
    }

    /**
     * Remove image.
     *
     * @param AccessoryImage $image
     */
    public function removeImage(AccessoryImage $image)
    {
        $this->images->removeElement($image);
        $image->setEntity(null);
    }

    /**
     * Get owner.
     *
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Set owner.
     *
     * @param User $user
     *
     * @return $this
     */
    public function setOwner(User $user = null)
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * Accessory clone.
     */
    public function __clone()
    {
        parent::__clone();
        $this->images = new ArrayCollection();
        $this->setParent(null);
        $this->children = new ArrayCollection();
        $this->models = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }
}

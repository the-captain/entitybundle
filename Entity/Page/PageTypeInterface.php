<?php

namespace ThreeWebOneEntityBundle\Entity\Page;

use ThreeWebOneEntityBundle\Entity\Order\Order;

interface PageTypeInterface
{
    const TERMS = 1;

    const TERMS_NAME = 'terms';

    const PRIVACY = 2;

    const PRIVACY_NAME = 'privacy';

    const DISCLAIMER_SALE = 3;

    const DISCLAIMER_SALE_NAME = 'disclaimer sale';

    const DISCLAIMER_BUYBACK = 4;

    const DISCLAIMER_BUYBACK_NAME = 'disclaimer buyback';

    const DISCLAIMER_REPAIR = 5;

    const DISCLAIMER_REPAIR_NAME = 'disclaimer repair';

    const PAGES_ARRAY = [
        self::TERMS => self::TERMS_NAME,
        self::PRIVACY => self::PRIVACY_NAME,
        self::DISCLAIMER_SALE => self::DISCLAIMER_SALE_NAME,
        self::DISCLAIMER_BUYBACK => self::DISCLAIMER_BUYBACK_NAME,
        self::DISCLAIMER_REPAIR => self::DISCLAIMER_REPAIR_NAME,
    ];

    const ORDER_DISCLAIMER_CONNECTOR = [
        self::DISCLAIMER_SALE => Order::SALE,
        self::DISCLAIMER_BUYBACK => Order::BUYBACK,
        self::DISCLAIMER_REPAIR => Order::REPAIR,
    ];
}

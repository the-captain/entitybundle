<?php

namespace ThreeWebOneEntityBundle\Entity\Page;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

/**
 * Class Page.
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\Page\PageRepository")
 */
class Page implements StatusInterface, OwnerInterface, PageTypeInterface
{
    use StatusTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="page_type", type="smallint")
     */
    protected $pageType;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Assert\Length(max=255, maxMessage="Title is too long")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="pages")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPageType(): int
    {
        return $this->pageType;
    }

    /**
     * @param int $pageType
     *
     * @return $this
     */
    public function setPageType(int $pageType): self
    {
        $this->pageType = $pageType;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription(string $description = null): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get owner.
     *
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * Set owner.
     *
     * @param User $user
     *
     * @return $this
     */
    public function setOwner(User $user = null): self
    {
        $this->owner = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return 'Page ' . $this->getTitle() ?: '';
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title = null): self
    {
        $this->title = $title;

        return $this;
    }
}

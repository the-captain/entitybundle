<?php

namespace ThreeWebOneEntityBundle\Entity\ClientNotes;

use Doctrine\ORM\Mapping as ORM;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;

/**
 * Notes
 *
 * @ORM\Table(name="client_notes")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\ClientNotes\ClientNotesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientNotes implements OwnerInterface
{
    use CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User", inversedBy="userOrders")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * ClientNotes constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getOwner() :User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     *
     * @return ClientNotes
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return ClientNotes
     */
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }
}

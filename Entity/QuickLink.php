<?php

namespace ThreeWebOneEntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ThreeWebOneEntityBundle\Entity\Helper\UpdatedAtTrait;
use ThreeWebOneEntityBundle\Entity\Helper\CreatedAtTrait;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;

/**
 * Notes
 *
 * @ORM\Table(name="quick_link")
 * @ORM\Entity(repositoryClass="ThreeWebOneEntityBundle\Repository\QuickLinkRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class QuickLink implements OwnerInterface
{
    use CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ThreeWebOneEntityBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string $content
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string $content
     *
     * @ORM\Column(name="link", type="text")
     */
    private $link;

    /**
     * ClientNotes constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getOwner() :User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     *
     * @return QuickLink
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return QuickLink
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     *
     * @return QuickLink
     */
    public function setLink(string $link)
    {
        $this->link = $link;

        return $this;
    }
}

<?php

namespace ThreeWebOneEntityBundle\Sluggable;

interface SluggableInterface
{
    public function createSlug() : string;

    public function getSlug() : string;
}

<?php

namespace ThreeWebOneEntityBundle\Sluggable;

use Behat\Transliterator\Transliterator;

trait Sluggable
{
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    protected $slug;

    /**
     * get slug
     *
     * @return string
     */
    public function getSlug() : string
    {
        return $this->slug;
    }

    /**
     * Setting slug on create and update
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setSlug()
    {
        $rawSlug = $this->createSlug();
        $this->slug = Transliterator::urlize(Transliterator::transliterate($rawSlug));
    }
}

<?php

namespace ThreeWebOneEntityBundle\Status;

interface StatusInterface
{
    /**
     * Status Active
     */
    const STATUS_ACTIVE = 1;

    const ACTIVE_NAME = 'Active';

    /**
     * Status InActive
     */
    const STATUS_INACTIVE = 0;

    const INACTIVE_NAME = 'Inactive';

    const STATUS_ARRAY = [
        self::ACTIVE_NAME => self::STATUS_ACTIVE,
        self::INACTIVE_NAME => self::STATUS_INACTIVE,
    ];

    public function setStatus($status);

    public function getStatus();
}

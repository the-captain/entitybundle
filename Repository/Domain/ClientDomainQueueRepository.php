<?php

namespace ThreeWebOneEntityBundle\Repository\Domain;

use ThreeWebOneEntityBundle\Entity\Domain\ClientDomainQueue;
use ThreeWebOneEntityBundle\Repository\BaseRepository;
use Doctrine\DBAL\LockMode;

class ClientDomainQueueRepository extends BaseRepository
{
    /**
     * @return null|ClientDomainQueue
     */
    public function getNextDomain(): ?ClientDomainQueue
    {
        $qb = ($this->createQueryBuilder('cdq'))
            ->where('cdq.isLock = :isLock')
            ->andWhere('cdq.updatedAt < :now')
            ->setParameter('isLock', false)
            ->setParameter('now', new \DateTime('-60 second'))
            ->orderBy('cdq.createdAt', 'ASC')
            ->setMaxResults(1);

        return $qb->getQuery()->setLockMode(LockMode::PESSIMISTIC_WRITE)->getOneOrNullResult();
    }

    /**
     * @param string $domainName
     * @return mixed
     */
    public function getByDomainName(string $domainName): ?ClientDomainQueue
    {
        $qb = ($this->createQueryBuilder('cdq'))
            ->where('cdq.domain = :domainName')
            ->setParameter('domainName', $domainName)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array|null
     */
    public function getDomainForUnlock(): ?array
    {
        $qb = ($this->createQueryBuilder('cdq'))
            ->where('cdq.isLock = :isLock')
            ->andWhere('cdq.updatedAt < :now')
            ->setParameter('isLock', true)
            ->setParameter('now', new \DateTime('-1 hour'));

        return $qb->getQuery()->getResult();
    }
}

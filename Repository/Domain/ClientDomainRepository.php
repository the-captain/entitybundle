<?php

namespace ThreeWebOneEntityBundle\Repository\Domain;

use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

class ClientDomainRepository extends BaseRepository
{
    /**
     * Get All User Domains
     *
     * @param User $owner
     * @return ClientDomain[]
     */
    public function getAllUserDomains(User $owner): array
    {
        $qb = ($this->createQueryBuilder('cd'))
            ->where('cd.owner = :owner')
            ->setParameter('owner', $owner);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get User BuDomain
     *
     * @param String $domain
     * @return User|Null
     */
    public function getUserByDomain(String $domain): ?User
    {
        $qb = ($this->createQueryBuilder('cd'))
            ->where('cd.domain = :domain')
            ->andWhere('cd.active = :status')
            ->setParameter('domain', $domain)
            ->setParameter('status', ClientDomain::ACTIVE);

        $this->useResultCacheOnQuery($qb);

        $clientDomain = $qb->getQuery()->getOneOrNullResult();

        return $clientDomain ? $clientDomain->getOwner() : null;
    }

    /**
     * Get Duplicate Domains
     *
     * @param User $user
     * @param String $domain
     * @return mixed
     */
    public function getDuplicateDomains(User $user, String $domain)
    {
        $qb = ($this->createQueryBuilder('cd'))
            ->where('cd.domain = :domain')
            ->andWhere('cd.owner != :owner')
            ->setParameter('domain', $domain)
            ->setParameter('owner', $user);

        return $qb->getQuery()->getResult();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Repository\UserConfig;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * QuestionRepository
 */
class QuestionRepository extends BaseRepository implements AdminQueryInterface
{
    /**
     * Return distinct array of users question categories
     *
     * @param User $user
     *
     * @return array
     */
    public function getQuestionsCategoryForUser(User $user)
    {
        $qb = $this->createQueryBuilder('q')
            ->select('distinct(q.category) as category')
            ->innerJoin('q.users', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $qb = $this->createQueryBuilder('q');
        $qb->addSelect('u')
            ->innerJoin('q.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->setParameter('user_ids', $userId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
        ];
    }
}

<?php

namespace ThreeWebOneEntityBundle\Repository\UserConfig;

use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * ThemeRepository
 */
class ThemeRepository extends BaseRepository
{
    /**
     * Gets themes by type query
     *
     * @param int $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getThemesByTypeQuery(int $type)
    {
        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.type = :type')
            ->setParameter('type', $type);
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }
}

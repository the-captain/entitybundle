<?php

namespace ThreeWebOneEntityBundle\Repository\UserConfig;

use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * LocationRepository
 */
class LocationRepository extends BaseRepository
{
    /**
     * Return distinct array of users location cities
     *
     * @param User $user
     *
     * @return array
     */
    public function getLocationCitiesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('l')
            ->select('distinct(l.city) as city')
            ->innerJoin('l.owner', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserLocationsQueryBuilder(User $user)
    {
        return $this->createQueryBuilder('l')
            ->where('l.owner = :owner')
            ->setParameter('owner', $user);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserLocations(User $user)
    {
        $qb = $this->getUserLocationsQueryBuilder($user);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserMainLocations(User $user)
    {
        $qb = $this->getUserLocationsQueryBuilder($user)
            ->andWhere('l.isMain = :status')
            ->setParameter('status', true);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }
}

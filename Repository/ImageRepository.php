<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage;
use ThreeWebOneEntityBundle\Entity\ModelImage;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * ImageRepository
 */
class ImageRepository extends BaseRepository
{
    /**
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function getUserImagesForInventoryItemQuery(User $user)
    {
        $em = $this->getEntityManager();
        $qb = $this->createQueryBuilder('i')
            ->where('i INSTANCE OF :model_image or i INSTANCE OF :item_image')
            ->setParameter('model_image', $em->getClassMetadata(ModelImage::class))
            ->setParameter('item_image', $em->getClassMetadata(InventoryItemImage::class))
            ->andWhere('i.owner = :owner')
            ->setParameter('owner', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }
}

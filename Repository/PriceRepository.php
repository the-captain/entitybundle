<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * PriceRepository
 */
class PriceRepository extends BaseRepository implements AdminQueryInterface
{
    const GRADE = 'grade';

    /**
     * Get prices of model by user and site
     *
     * @param Model $model
     * @param int $site
     * @param User $user
     *
     * @return array
     */
    public function getPricesByModelUserAndSite(Model $model, int $site, User $user)
    {
        $qb = $this->createQueryBuilder('price')
            ->innerJoin('price.priceType', 'price_type')
            ->innerJoin('price.model', 'm')
            ->innerJoin('price.users', 'u')
            ->where('price_type.site = :price_type_site')
            ->andWhere('m.id = :model_id')
            ->andWhere('u.id = :user_id')
            ->setParameter('price_type_site', $site)
            ->setParameter('model_id', $model->getId())
            ->setParameter('user_id', $user->getId());

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get prices for Cart
     *
     * @param array $ids
     *
     * @return array
     */
    public function getCartPrices(array $ids)
    {
        $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.priceType', 'p_t')
            ->leftJoin('p.model', 'm')
            ->leftJoin('m.images', 'i')
            ->andWhere('p.id IN (:ids)')
            ->setParameter('ids', $ids);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get similar sell item for inventory item
     *
     * @param InventoryItem $inventoryItem
     * @param User $user
     * @return InventoryItem|null
     */
    public function getSimilarPrice(InventoryItem $inventoryItem, User $user)
    {
        $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.inventoryItems', 'ii');
        $qb = $this->addEntityOwnerConditionToQB($qb, $user);
        $qb = $this->getNullOrEqualCondition($qb, $inventoryItem->getPriceValue(), 'priceValue');
        $qb = $this->getNullOrEqualCondition($qb, $inventoryItem->getModel(), 'model');
        $qb = $this->getNullOrEqualCondition($qb, $inventoryItem->getCategory(), 'category');
        $qb = $this->getNullOrEqualCondition($qb, $inventoryItem->getPriceType(), 'priceType');

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param $value
     * @param string $field
     *
     * @return QueryBuilder
     */
    private function getNullOrEqualCondition(QueryBuilder $qb, $value, string $field)
    {
        if ($value) {
            $qb->andWhere(sprintf('ii.%1$s = :%1$s', $field))
                ->setParameter($field, $value);
        } else {
            $qb->andWhere(sprintf('ii.%s is NULL', $field));
        }

        return $qb;
    }

    /**
     * @param User $user
     * @param Category $category
     * @param array $filters
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function getPricesByUserCategory(
        User $user,
        Category $category,
        array $filters,
        $limit = null,
        $offset = null
    ) :array {
        $qb = $this->getPricesByUserCategoryQuery($user, $category, $filters);
        $qb = $this->setOrderByToQuery($qb, $filters)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param Category $category
     * @param array $filters
     *
     * @return string
     */
    public function getCountPricesByUserCategory(
        User $user,
        Category $category,
        array $filters
    ) :string {
        $qb = $this->getPricesByUserCategoryQuery($user, $category, $filters);
        $qb->select('count(price.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }


    /**
     * Get prices by user and category
     *
     * @param User $user
     * @param Category $category
     * @param array $filters
     *
     * @return QueryBuilder
     */
    public function getPricesByUserCategoryQuery(
        User $user,
        Category $category,
        array $filters
    ) {
        $qb = $this->createQueryBuilder('price')
            ->innerJoin('price.inventoryItems', 'ii')
            ->innerJoin('price.priceType', 'price_type')
            ->innerJoin('ii.category', 'c')
            ->where('c.id = :category_id')
            ->setParameter('category_id', $category->getId());
        $qb = $this->addEntityOwnerConditionToQB($qb, $user);

        foreach ($filters as $key => $value) {
            if (is_array($value)) {
                if ($key == self::GRADE) {
                    $qb
                        ->andWhere('price_type.id IN (:ids)')
                        ->setParameter('ids', $value);
                } else {
                    $qb = $this->joinFilterToQuery($qb, $key, $value);
                }
            }
        }

        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param string $type
     * @param array $value
     *
     * @return QueryBuilder
     */
    private function joinFilterToQuery(QueryBuilder $qb, string $type, array $value) : QueryBuilder
    {
        return $qb
            ->innerJoin(
                'ii.filters',
                sprintf('f_%s', $type),
                'WITH',
                sprintf('f_%1$s.id IN (:ids_%1$s)', $type)
            )
            ->setParameter(sprintf('ids_%s', $type), $value);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filters
     *
     * @return QueryBuilder
     */
    private function setOrderByToQuery(QueryBuilder $qb, array $filters) : QueryBuilder
    {
        if (array_key_exists('sorting', $filters) && in_array($filters['sorting'], ['ASC', 'DESC'])) {
            $qb->orderBy('price.value', $filters['sorting']);
        // if sorting direction not exist sort items by recent
        } else {
            $qb->orderBy('price.id', 'DESC');
        }

        return $qb;
    }

    /**
     * @param User $user
     * @param Model $model
     * @return array
     */
    public function getPricesByModelUserPriceType(User $user, Model $model)
    {
        $qb = $this->createQueryBuilder('price')
            ->innerJoin('price.priceType', 'price_type')
            ->where('price.owner = :user')
            ->setParameter('user', $user)
            ->andWhere('price_type.site IN (:sites)')
            ->setParameter('sites', [PriceType::REPAIR, PriceType::BUYBACK])
            ->andWhere('price.model = :model')
            ->setParameter('model', $model)
        ;

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];


        $qb = $this->createQueryBuilder('p');

        $qb->addSelect('u')
            ->innerJoin('p.users', 'u')
            ->innerJoin('p.priceType', 'pt')
            ->where('u.id IN (:user_ids)')
            ->andWhere('pt.site <> ' . SiteTypeInterface::SALE)
            ->setParameter('user_ids', $userId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
        ];
    }
}

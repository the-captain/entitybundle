<?php

namespace ThreeWebOneEntityBundle\Repository\Billing;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * Class Plan.
 */
class PlanRepository extends BaseRepository
{
    /**
     * @return IterableResult
     */
    public function getActive(): IterableResult
    {
        $qb = $this->getActiveQuery();

        return $qb->getQuery()->iterate();
    }

    /**
     * @return QueryBuilder
     */
    public function getActiveQuery(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('bp');

        $qb->andWhere('bp.active = true');

        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @return IterableResult
     */
    public function getActiveWithTrial(): IterableResult
    {
        $qb = $this->getActiveQuery()
                   ->andWhere('bp.hasTrial = 1');

        return $qb->getQuery()->iterate();
    }

    /**
     * @return IterableResult
     */
    public function getActiveWithoutTrial(): IterableResult
    {
        $qb = $this->getActiveQuery()
                   ->andWhere('bp.hasTrial = 1');

        return $qb->getQuery()->iterate();
    }

    /**
     * @return IterableResult
     */
    public function getAll(): IterableResult
    {
        $qb = $this->createQueryBuilder('bp');

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->iterate();
    }
}

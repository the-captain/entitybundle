<?php

namespace ThreeWebOneEntityBundle\Repository\Billing;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription as SubscriptionEntity;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * Class Subscription.
 */
class SubscriptionRepository extends BaseRepository
{
    /**
     * @return IterableResult
     */
    public function getActive(): IterableResult
    {
        $qb = $this->createQueryBuilder('bs');

        $qb
            ->andWhere('bs.status in(:statuses)')
            ->setParameter(
                'statuses',
                [
                    SubscriptionEntity::STATUS_ACTIVE,
                    SubscriptionEntity::STATUS_ENDING,
                    SubscriptionEntity::STATUS_TRIAL,
                ]
            );
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->iterate();
    }

    /**
     * @return IterableResult
     */
    public function getAll(): IterableResult
    {
        $qb = $this->createQueryBuilder('bs');
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->iterate();
    }
}

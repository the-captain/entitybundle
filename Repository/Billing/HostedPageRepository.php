<?php

namespace ThreeWebOneEntityBundle\Repository\Billing;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\HostedPage;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * Class HostedPageRepostiory.
 */
class HostedPageRepository extends BaseRepository
{
    /**
     * @param Subscription $subscription
     *
     * @return QueryBuilder
     * @throws \InvalidArgumentException
     */
    public function getCheckoutSubscriptionQuery(Subscription $subscription): QueryBuilder
    {
        return $this->getHostedPageForCustomerQuery(
            $subscription->getCustomer(),
            HostedPage::TYPE_CHECKOUT_SUBSCRIPTION,
            $subscription->getId()
        );
    }

    /**
     * @param Customer $customer
     * @param int      $type
     * @param int      $entityId
     *
     * @return QueryBuilder
     * @throws \InvalidArgumentException
     */
    public function getHostedPageForCustomerQuery(Customer $customer, int $type, int $entityId = 0): QueryBuilder
    {
        $this->checkValueInAllowedListAndThrow($type, HostedPage::TYPES_LIST);

        $qb = $this->createQueryBuilder('hp');

        $qb
            ->andWhere('hp.customer in (:customer_id)')
            ->andWhere('hp.type = :type')
            ->andWhere('hp.entityId = :entityId')
            ->setParameter('customer_id', $customer->getId())
            ->setParameter('type', $type)
            ->setParameter('entityId', $entityId);

        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param Customer $customer
     *
     * @return QueryBuilder
     * @throws \InvalidArgumentException
     */
    public function getUpdatePaymentMethodQuery(Customer $customer): QueryBuilder
    {
        return $this->getHostedPageForCustomerQuery($customer, HostedPage::TYPE_UPDATE_PAYMENT_METHOD);
    }
}

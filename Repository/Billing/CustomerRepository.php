<?php

namespace ThreeWebOneEntityBundle\Repository\Billing;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * Class Customer.
 */
class CustomerRepository extends BaseRepository
{
    /**
     * @return IterableResult
     */
    public function getAll(): IterableResult
    {
        $qb = $this->createQueryBuilder('bc');
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->iterate();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Repository\Inventory;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Aggregatable\AggregatableTrait;
use ThreeWebOneEntityBundle\Entity\Order\OrderStatusInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\BarcodeStatusInterface;
use ThreeWebOneEntityBundle\Repository\BaseRepository;
use ThreeWebOneEntityBundle\Status\StatusInterface;

/**
 * InventoryItemBarcodeRepository
 */
class InventoryItemBarcodeRepository extends BaseRepository
{
    use AggregatableTrait;

    /**
     * @param User $user
     * @return \ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode
     */
    public function getUserLastBarcodesCode(User $user)
    {
        $qb = $this->createQueryBuilder('bc');
        $qb->innerJoin('bc.inventoryItem', 'ii')
            ->where('ii.owner = :user')
            ->andWhere('bc.barcode IS NOT NULL')
            ->setParameter('user', $user)
            ->orderBy('bc.barcode', 'desc')
            ->setMaxResults(1);

        return $qb->getQuery()->setLockMode(LockMode::PESSIMISTIC_WRITE)->getResult()[0] ?? null;
    }

    /**
     * @param User $owner
     * @return array
     */
    public function getAggregatedInventoryInfo(User $owner) : array
    {
        $qb = $this->createQueryBuilder('bc')
            ->select('COUNT(bc.id) as itemsCount, SUM(bci.purchasePrice) as itemsCost')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->where('bci.owner = :owner')
            ->andWhere('bci.createdAt > :date')
            ->setParameter('date', $this->getModifiedDate('-1 month'))
            ->setParameter('owner', $owner)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600);

        return $qb->getSingleResult();
    }

    /**
     * @param User $owner
     * @return array
     */
    public function getAggregatedSoldItems(User $owner) : array
    {
        $qb = $this->createQueryBuilder('bc')
            ->select('COUNT(bci.id) as itemsCount, SUM(bci.purchasePrice) as itemsCost')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->where('bci.owner = :owner')
            ->andWhere('bc.status = :status')
            ->setParameter('owner', $owner)
            ->setParameter('status', OrderStatusInterface::COMPLETED)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600);

        return $qb->getSingleResult();
    }

    /**
     * @param $owner
     * @return int
     */
    public function getAggregatedUnsoldInventoryCost($owner) : int
    {
        $qb = $this->createQueryBuilder('bc')
            ->select('SUM(bci.purchasePrice)')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->where('bci.owner = :owner')
            ->andWhere('bc.status != :status')
            ->setParameter('owner', $owner)
            ->setParameter('status', OrderStatusInterface::COMPLETED)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600);

        return $qb->getSingleScalarResult() ?? 0;
    }

    /**
     * @param $owner
     * @return int
     */
    public function getAggregatedInventoryPriceActiveInWebSite($owner) : int
    {
        $qb = $this->createQueryBuilder('bc')
            ->select('SUM(bci.priceValue)')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->where('bci.owner = :owner')
            ->andWhere('bc.status != :barcodeStatus')
            ->andWhere('bci.status = :itemStatus')
            ->setParameter('owner', $owner)
            ->setParameter('barcodeStatus', BarcodeStatusInterface::SOLD)
            ->setParameter('itemStatus', StatusInterface::STATUS_ACTIVE)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600);
        return $qb->getSingleScalarResult() ?? 0;
    }

    /**
     * @param User $user
     *
     * @return \Doctrine\ORM\Query
     */
    public function getUserActiveBarcodesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('bc')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->where('bci.owner = :owner')
            ->setParameter('owner', $user)
            ->andWhere('bc.status = :status')
            ->andWhere('bci.status = :bci_status')
            ->setParameter('status', InventoryItemBarcode::NEW_BARCODE)
            ->setParameter('bci_status', InventoryItem::STATUS_ACTIVE);

        $qb->getQuery()->useResultCache(true, 3600)->useQueryCache(true);

        return $qb->getQuery();
    }

    /**
     * @param Order $order
     *
     * @return \Doctrine\ORM\Query
     */
    public function getBarcodesByOrderQuery(Order $order)
    {
        $qb = $this->createQueryBuilder('bc')
            ->leftJoin('bc.inventoryItem', 'bci')
            ->leftJoin('bci.orders', 'o')
            ->where('o.id = :order_id')
            ->setParameter('order_id', $order->getId())
            ->andWhere('bc.status = :status')
            ->setParameter('status', InventoryItemBarcode::NEW_BARCODE)
            ->orWhere('bc.purchaseOrder = :order')
            ->setParameter('order', $order);

        $qb->getQuery()->useResultCache(true, 3600)->useQueryCache(true);

        return $qb->getQuery();
    }


    /**
     * Return distinct array of users inventory items barcodes
     *
     * @param User $user
     *
     * @return array
     */
    public function getInventoryItemsBarcodesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('ib')
            ->select('distinct(ib.barcode) as barcode')
            ->innerJoin('ib.inventoryItem', 'i')
            ->where('i.owner = :user')
            ->setParameter('user', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Return distinct array of users orders barcodes
     *
     * @param User $user
     *
     * @return array
     */
    public function getOrdersBarcodesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('ib')
            ->select('distinct(ib.barcode) as barcode')
            ->innerJoin('ib.purchaseOrder', 'o')
            ->where('o.owner = :user')
            ->andWhere('o.status not in (:status)')
            ->setParameter('user', $user)
            ->setParameter('status', [Order::COMPLETED, Order::REJECTED]);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }
}

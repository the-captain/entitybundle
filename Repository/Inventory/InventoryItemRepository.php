<?php

namespace ThreeWebOneEntityBundle\Repository\Inventory;

use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialRepoTrait;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * InventoryItemRepository
 */
class InventoryItemRepository extends BaseRepository
{
    use SequentialRepoTrait;

    /**
     * Find All User Items With Model And PriceType
     *
     * @param User $user
     * @return array
     */
    public function findAllUserActiveItems(User $user): array
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.owner = :owner')
            ->andWhere('i.status = :status')
            ->setParameter('owner', $user)
            ->setParameter('status', InventoryItem::STATUS_ACTIVE);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Return distinct array of users inventory items titles
     *
     * @param User $user
     *
     * @return array
     */
    public function getInventoryItemsTitlesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('distinct(i.title) as title')
            ->where('i.owner = :user')
            ->setParameter('user', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Return distinct array of users inventory items titles
     *
     * @param User $user
     *
     * @return array
     */
    public function getOrderInventoryItemsTitlesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('distinct(i.title) as title')
            ->leftJoin('i.orders', 'o')
            ->leftjoin('i.inventoryItemBarcodes', 'iib')
            ->leftjoin('iib.purchaseOrder', 'po')
            ->where('(i.owner = :user) or (po.owner = :user)')
            ->andWhere('i.title is not NULL')
            ->setParameter('user', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param string $query
     *
     * @return array
     */
    public function globalSearchUserInventoryItems(User $user, string $query)
    {
        $qb = $this->createQueryBuilder('ii')
            ->where('ii.owner = :user')
            ->andWhere('ii.title LIKE :query')
            ->setParameter('query', sprintf('%%%s%%', $query))
            ->setParameter('user', $user)
            ->orderBy('ii.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}

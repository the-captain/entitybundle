<?php

namespace ThreeWebOneEntityBundle\Repository\Inventory;

use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * InventoryRepository
 */
class InventoryRepository extends BaseRepository
{
    /**
     * Gets User Inventories
     *
     * @param \ThreeWebOneEntityBundle\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserInventoriesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('i');
        $qb->andWhere('i.user = :user');
        $qb->setParameter('user', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }
}

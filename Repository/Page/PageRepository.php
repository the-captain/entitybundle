<?php

namespace ThreeWebOneEntityBundle\Repository\Page;

use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;

/**
 * PageRepository
 */
class PageRepository extends BaseRepository
{
    /**
     * Gets User Page depending on page type
     *
     * @param User $user
     * @param $pageType
     *
     * @return Page | null
     */
    public function getUserPageByPageType(User $user, $pageType)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.owner = :user AND p.pageType = :page_type')
            ->setParameter('user', $user)
            ->setParameter('page_type', $pageType);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

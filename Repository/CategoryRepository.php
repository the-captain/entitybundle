<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * CategoryRepository
 */
class CategoryRepository extends BaseRepository implements AdminQueryInterface
{
    /**
     * Gets Query for User Parent Categories
     *
     * @param $type
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserParentCategoriesQuery($type, User $user)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.users', 'u')
            ->andWhere('c.parent IS NULL AND c.type = :category_type AND u.id = :user_id')
            ->setParameter('category_type', $type)
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets Query for User Categories
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserCategoriesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.users', 'u')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets Users children categories by Category
     *
     * @param Category $category
     * @param User $user
     *
     * @return array
     */
    public function getUserChildrenCategoriesByCategory(Category $category, User $user)
    {
        $qb = $this->getUserCategoriesQuery($user)
            ->andWhere('c.parent = :category_id')
            ->setParameter('category_id', $category->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param int $site
     *
     * @return mixed
     */
    public function getParentCategoryThatHasPriceBuybackOrRepair(User $user, int $site)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.type = :category_type')
            ->setParameter('category_type', Category::TYPE_CATEGORY)
            ->innerJoin('c.models', 'm')
            ->innerJoin('m.prices', 'price')
            ->innerJoin('m.categories', 'c_products', 'WITH', 'c_products.type = :product_type')
            ->innerJoin('m.categories', 'c_provider', 'WITH', 'c_provider.type = :provider_type')
            ->setParameter('product_type', Category::TYPE_PRODUCT)
            ->setParameter('provider_type', Category::TYPE_PROVIDER);
        $qb = $this->addUserPriceAndSiteJoinToQB($qb, $user, $site);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $parentCategoryId = $params[AdminQueryInterface::PARAM_PARENT_CATEGORY_ID];

        $qb = $this->createQueryBuilder('c');

        $qb->addSelect('u')
            ->innerJoin('c.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->andWhere('c.type = :type_category')
            ->setParameter('type_category', $parentCategoryId)
            ->setParameter('user_ids', $userId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
            AdminQueryInterface::PARAM_PARENT_CATEGORY_ID,
        ];
    }

    /**
     * @param User $user
     * @param int $type
     *
     * @return QueryBuilder
     */
    public function getUserCategoriesByTypeQuery(User $user, int $type)
    {
        $qb = $this->getUserCategoriesQuery($user)
            ->andWhere('c.type = :type')
            ->leftJoin('c.children', 'children')
            ->andWhere('children is NULL')
            ->setParameter('type', $type);
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param User $user
     * @param int $type
     *
     * @return array
     */
    public function getUserCategoryByType(User $user, int $type)
    {
        return $this->getUserCategoriesByTypeQuery($user, $type)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @param int $type
     *
     * @return array
     */
    public function getUsersCategoriesHavingPrices(User $user, int $type)
    {
        $qb = $this->getUserCategoriesByTypeQuery($user, $type)
            ->innerJoin('c.inventoryItems', 'ii')
            ->innerJoin('ii.price', 'p')
            ->andWhere('p.owner = :price_user')
            ->setParameter('price_user', $user);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param Category $category
     * @param int $site
     * @return array
     */
    public function getUserProducts(User $user, Category $category, int $site)
    {
        $qb = $this->createQueryBuilder('c')
            ->innerJoin('c.models', 'm')
            ->innerJoin('m.prices', 'price')
            ->innerJoin('m.categories', 'c_category')
            ->andWhere('c.type = :c_type')
            ->setParameter('c_type', Category::TYPE_PRODUCT)
            ->andWhere('c_category.id = :category_id')
            ->setParameter('category_id', $category->getId());
        $qb = $this->addUserPriceAndSiteJoinToQB($qb, $user, $site);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param Category $category
     * @param Category $product
     * @param int $site
     * @return array
     */
    public function getUserProviders(User $user, Category $category, Category $product, int $site)
    {
        $qb = $this->createQueryBuilder('c')
            ->innerJoin('c.models', 'm')
            ->innerJoin('m.prices', 'price')
            ->innerJoin('m.categories', 'c_category')
            ->innerJoin('m.categories', 'c_product')
            ->andWhere('c.type = :c_type')
            ->setParameter('c_type', Category::TYPE_PROVIDER)
            ->andWhere('c_category.id = :category_id')
            ->setParameter('category_id', $category->getId())
            ->andWhere('c_product.id = :product_id')
            ->setParameter('product_id', $product->getId());
        $qb = $this->addUserPriceAndSiteJoinToQB($qb, $user, $site);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * ProductRepository
 */
class UserRepository extends BaseRepository
{
    /**
     * @param string $storeAddress
     *
     * @return null|User
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserByStoreAddress(string $storeAddress): ?User
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where('u.storeAddress = :storeAddress')
            ->setParameter('storeAddress', $storeAddress)
            ->setMaxResults(1);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

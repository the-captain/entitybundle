<?php

namespace ThreeWebOneEntityBundle\Repository\ClientNotes;

use Doctrine\ORM\EntityRepository;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * ClientNotesRepository
 */
class ClientNotesRepository extends EntityRepository
{
    /**
     * @param User $owner
     * @return array
     */
    public function getAllClientNotes(User $owner) : array
    {
        $qb = $this->createQueryBuilder('cn')
            ->where('cn.owner = :owner')
            ->setParameter('owner', $owner)
            ->orderBy('cn.createdAt', 'DESC')
            ->getQuery()
            ->useQueryCache(true);

        return $qb->getResult();
    }
}

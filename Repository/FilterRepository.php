<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Product;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * FilterRepository
 */
class FilterRepository extends BaseRepository implements AdminQueryInterface
{
    /**
     * Gets Query for User's Parent Filters
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserParentFiltersQuery(User $user)
    {
        $qb = $this->createQueryBuilder('f')
            ->leftJoin('f.users', 'u')
            ->andWhere('f.parent IS NULL AND u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets Query for User's Filters
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserFiltersQuery(User $user)
    {
        $qb = $this->createQueryBuilder('f')
            ->innerJoin('f.users', 'u')
            ->andWhere('u.id = :user_id')
            ->leftJoin('f.children', 'children')
            ->andWhere('children is NULL')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets User's Categories Models Filters
     *
     * @param User $user
     * @param Category $category
     *
     * @return array
     */
    public function getUserCategoryModelsFilters(User $user, Category $category)
    {
        $qb = $this->createQueryBuilder('f')
            ->innerJoin('f.inventoryItems', 'ii')
            ->innerJoin('ii.priceType', 'price_type');
        $qb = $this->addEntityOwnerConditionToQB($qb, $user);
        $qb = $this->addCategoryJoinToQB($qb, $category);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Category $product
     * @param User $user
     * @param int $site
     *
     * @return array
     */
    public function getCarrierByProductSiteAndUserPrice(Category $product, User $user, int $site)
    {
        $qb = $this->createQueryBuilder('f')
            ->innerJoin('f.models', 'm')
            ->innerJoin('m.categories', 'c')
            ->innerJoin('m.prices', 'price')
            ->andWhere('c.id = :product_id')
            ->andWhere('f.type = :filter_type')
            ->setParameter('product_id', $product->getId())
            ->setParameter('filter_type', Filter::TYPE_PROVIDER);
        $qb = $this->addUserPriceAndSiteJoinToQB($qb, $user, $site);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $qb = $this->createQueryBuilder('f');

        $qb->addSelect('u')
            ->innerJoin('f.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->setParameter('user_ids', $userId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
        ];
    }
}

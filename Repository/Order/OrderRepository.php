<?php

namespace ThreeWebOneEntityBundle\Repository\Order;

use ThreeWebOneEntityBundle\Aggregatable\AggregatableTrait;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialRepoTrait;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderStatusInterface;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\BaseRepository;
use DateTime;
use Doctrine\ORM\QueryBuilder;

/**
 * OrderRepository
 */
class OrderRepository extends BaseRepository
{
    use AggregatableTrait;
    use SequentialRepoTrait;

    /**
     * Get Aggregated User Sold Orders During Period
     *
     * @param User $owner
     * @param DateTime $from/
     * @param DateTime $to
     * @return array
     */
    public function getAggregatedUserSoldOrdersDuringPeriod(User $owner, DateTime $from, DateTime $to)
    {
        $qb = $this->createQueryBuilder('o')
            ->select('o.createdAt as createdDate, COUNT(o.id) as itemsCount, SUM(b.purchasePrice) as itemsPurchasePrice')
            ->innerJoin('o.purchaseBarcodes', 'b')
            ->where('o.owner = :owner')
            ->andWhere('o.status = :orderStatus')
            ->andWhere('o.siteType = :siteType')
            ->andWhere('b.status = :barcodeStatus')
            ->andWhere('o.createdAt >= :from')
            ->andWhere('o.createdAt <= :to')
            ->setParameter('owner', $owner)
            ->setParameter('orderStatus', OrderStatusInterface::COMPLETED)
            ->setParameter('barcodeStatus', OrderStatusInterface::COMPLETED)
            ->setParameter('siteType', SiteTypeInterface::SALE)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->groupBy('o.createdAt');

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get Aggregated User Buyback Inventory Items During Period
     *
     * @param User $owner
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getAggregatedUserBuybackInventoryItemsDuringPeriod(User $owner, DateTime $from, DateTime $to)
    {
        $qb = $this->createQueryBuilder('o')
            ->select('o.createdAt as createdDate, COUNT(o.id) as itemsCount, SUM(b.purchasePrice) as itemsPurchasePrice')
            ->innerJoin('o.saleBarcodes', 'b')
            ->where('o.owner = :owner')
            ->andWhere('o.status = :orderStatus')
            ->andWhere('o.siteType = :siteType')
            ->andWhere('b.status = :barcodeStatus')
            ->andWhere('o.createdAt > :from')
            ->andWhere('o.createdAt < :to')
            ->setParameter('owner', $owner)
            ->setParameter('orderStatus', OrderStatusInterface::COMPLETED)
            ->setParameter('barcodeStatus', OrderStatusInterface::COMPLETED)
            ->setParameter('siteType', SiteTypeInterface::BUYBACK)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->groupBy('o.createdAt');

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get All Orders By Owner And Customer Ids Query
     *
     * @param int $ownerId
     * @param int $customerId
     * @param int $limit
     * @param int $offset
     * @return QueryBuilder
     */
    public function getAllOrdersByOwnerAndCustomerIdsQuery(
        int $ownerId,
        int $customerId,
        int $limit = 0,
        int $offset = 0
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('o')
            ->select('o.id, o.siteType, o.status, o.createdAt, SUM(b.sellPrice) as sellPrice, SUM(oi.purchasePrice) as repairBuybackPrice')
            ->leftJoin('o.purchaseBarcodes', 'b')
            ->leftJoin('o.items', 'oi')
            ->where('o.owner = :ownerId')
            ->andWhere('o.customer = :customerId')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setParameter('ownerId', $ownerId)
            ->setParameter('customerId', $customerId)
            ->groupBy('o.id');

        return $qb;
    }

    /**
     * Get All Orders By Owner And Customer Ids
     *
     * @param int $ownerId
     * @param int $customerId
     * @param int $offset
     * @param int $limit
     * @return array|mixed
     */
    public function getAllOrdersByOwnerAndCustomerIds(int $ownerId, int $customerId, int $limit = 0, int $offset = 0)
    {
        $qb = $this->getAllOrdersByOwnerAndCustomerIdsQuery($ownerId, $customerId, $limit, $offset);
        $this->useResultCacheOnQuery($qb);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param string $query
     *
     * @return array
     */
    public function globalSearchUserOrders(User $user, string $query)
    {
        $qb = $this->createQueryBuilder('o')
            ->leftJoin('o.purchaseBarcodes', 'b')
            ->leftJoin('b.inventoryItem', 'ii')
            ->leftJoin('o.items', 'i')
            ->where('o.owner = :user')
            ->andWhere('ii.title LIKE :query OR i.title LIKE :query')
            ->setParameter('query', sprintf('%%%s%%', $query))
            ->setParameter('user', $user)
            ->orderBy('o.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}

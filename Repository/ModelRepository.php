<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * ModelRepository
 */
class ModelRepository extends BaseRepository implements AdminQueryInterface
{
    const QUERY_MODEL = 'modelQuery';
    const QUERY_MODEL_PRICE = 'modelPriceQuery';
    const PARAM_SWITCH_QUERY = 'paramSwitchQuery';

    /**
     * Gets User's Models Query
     *
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserModelsQuery(User $user)
    {
        $qb = $this->createQueryBuilder('m')
            ->leftJoin('m.users', 'u')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param Category $category
     * @param Category $product
     * @param Category $provider
     * @param int $priceTypeSite
     * @param User $user
     * @return mixed
     */
    public function getModelsByCategoryProductAndProvider(
        Category $category,
        Category $product,
        Category $provider,
        int $priceTypeSite,
        User $user
    ) {
        $qb = $this->createQueryBuilder('m')
            ->innerJoin('m.categories', 'c_products', 'WITH', 'c_products.id = :product_id')
            ->innerJoin('m.categories', 'c_categories', 'WITH', 'c_categories.id = :category_id')
            ->innerJoin('m.categories', 'c_filters', 'WITH', 'c_filters.id = :filter_id')
            ->innerJoin('m.prices', 'prices')
            ->innerJoin('prices.users', 'u', 'WITH', 'u.id = :user_id')
            ->innerJoin(
                'prices.priceType',
                'price_type',
                'WITH',
                'price_type.site = :site'
            )
            ->setParameter('product_id', $product->getId())
            ->setParameter('filter_id', $provider->getId())
            ->setParameter('category_id', $category->getId())
            ->setParameter('site', $priceTypeSite)
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $qb = $this->createQueryBuilder('m');

        if ($params[self::PARAM_SWITCH_QUERY] == self::QUERY_MODEL) {
            $qb->addSelect('u')
                ->addSelect('c')
                ->addSelect('pr')
                ->addSelect('f')
                ->leftJoin('m.categories', 'c')
                ->leftJoin('m.prices', 'pr')
                ->leftJoin('m.filters', 'f')
                ->innerJoin('m.users', 'u')
                ->where('u.id IN (:user_ids)')
                ->setParameter('user_ids', $userId);
        }
        if ($params[self::PARAM_SWITCH_QUERY] == self::QUERY_MODEL_PRICE) {
            $qb->leftJoin('m.prices', 'prices')
                ->innerJoin('m.users', 'u')
                ->innerJoin('m.categories', 'c_products', 'WITH', 'c_products.type = :product_type')
                ->innerJoin('m.categories', 'c_categories', 'WITH', 'c_categories.type = :category_type')
                ->innerJoin('m.categories', 'c_provider', 'WITH', 'c_provider.type = :provider_type')
                ->where('u.id in (:users)')
                ->setParameter('users', $userId)
                ->setParameter('category_type', Category::TYPE_CATEGORY)
                ->setParameter('product_type', Category::TYPE_PRODUCT)
                ->setParameter('provider_type', Category::TYPE_PROVIDER);
        }

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
            self::PARAM_SWITCH_QUERY,
        ];
    }

    /**
     * Return distinct array of users models titles
     *
     * @param User $user
     *
     * @return array
     */
    public function getModelTitlesForUser(User $user)
    {
        $qb = $this->createQueryBuilder('m')
            ->select('distinct(m.title) as title')
            ->innerJoin('m.users', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param string $query
     *
     * @return array
     */
    public function globalSearchUserModel(User $user, string $query)
    {
        $qb = $this->createQueryBuilder('m')
            ->where('m.owner = :user')
            ->andWhere('m.title LIKE :query')
            ->setParameter('query', sprintf('%%%s%%', $query))
            ->setParameter('user', $user)
            ->orderBy('m.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}

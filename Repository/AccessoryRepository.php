<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * Class AccessoryRepository.
 */
class AccessoryRepository extends BaseRepository implements AdminQueryInterface
{
    /**
     * Gets Query for User's Parent Accessories.
     *
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserParentAccessoriesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.users', 'u')
            ->andWhere('a.parent IS NULL AND u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getAccessoriesForUserQuery(User $user)
    {
        $userId = $user->getId();

        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.users', 'u')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $userId);

        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets Query for User's Accessories.
     *
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserAccessoriesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.users', 'u')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $qb = $this->createQueryBuilder('a');

        $qb->addSelect('u')
            ->innerJoin('a.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->setParameter('user_ids', $userId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
        ];
    }
}

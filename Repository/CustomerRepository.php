<?php

namespace ThreeWebOneEntityBundle\Repository;

use ThreeWebOneEntityBundle\Aggregatable\AggregatableTrait;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialRepoTrait;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * CustomerRepository
 */
class CustomerRepository extends BaseRepository
{
    use AggregatableTrait, SequentialRepoTrait;

    /**
     * Gets User's Customers Query
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserCustomersQuery(User $user)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.owner', 'o')
            ->andWhere('o.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * @param User $owner
     * @return int
     */
    public function getAggregatedCustomersCount(User $owner): int
    {
        $qb = $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.owner = :owner')
            ->andWhere('c.createdAt >= :date')
            ->setParameter('date', $this->getModifiedDate('-1 month'))
            ->setParameter('owner', $owner)
            ->getQuery()
            ->useResultCache(true, 36000)
            ->useQueryCache(true);

        return $qb->getSingleScalarResult();
    }

    /**
     * Get Customer By Email And OwnerId
     *
     * @param string $email
     * @param int $ownerId
     * @return null|Customer
     */
    public function getCustomerByEmailAndOwnerId(string $email, int $ownerId): ?Customer
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.email = :email')
            ->andWhere('c.owner = :ownerId')
            ->setParameter('email', $email)
            ->setParameter('ownerId', $ownerId);
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param string $query
     *
     * @return array
     */
    public function globalSearchUserCustomers(User $user, string $query)
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.owner = :user')
            ->andWhere('c.firstname LIKE :query OR c.firstname LIKE :query OR c.email LIKE :query OR c.username LIKE :query')
            ->setParameter('query', sprintf('%%%s%%', $query))
            ->setParameter('user', $user)
            ->orderBy('c.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}

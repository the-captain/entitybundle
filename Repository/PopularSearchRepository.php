<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\PopularSearch;
use ThreeWebOneEntityBundle\Entity\Product;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * PopularSearchRepository
 */
class PopularSearchRepository extends BaseRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function getOrderedPopularSearches(User $user)
    {
        $qb = $this->createQueryBuilder('ps')
            ->andWhere('ps.owner = :user')
            ->setParameter('user', $user)
            ->andWhere('ps.status = :status')
            ->setParameter('status', PopularSearch::STATUS_ACTIVE)
            ->orderBy('ps.position', 'ASC');
        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }
}

<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use InvalidArgumentException;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * BaseRepository
 */
class BaseRepository extends EntityRepository
{
    const CACHE_TIME = 3600;

    /**
     * @param QueryBuilder $qb
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function addEntityOwnerConditionToQB(QueryBuilder $qb, User $user) : QueryBuilder
    {
        return $qb
            ->andWhere('ii.owner = :user')
            ->setParameter('user', $user);
    }

    /**
     * @param QueryBuilder $qb
     * @param Category $category
     *
     * @return QueryBuilder
     */
    public function addCategoryJoinToQB(QueryBuilder $qb, Category $category) : QueryBuilder
    {
        return $qb
            ->innerJoin('ii.category', 'c', 'WITH', 'c.id = :category_id')
            ->setParameter('category_id', $category->getId());
    }

    /**
     * Adds useResultCache to query builder
     *
     * @param QueryBuilder $qb
     */
    public function useResultCacheOnQuery(QueryBuilder $qb)
    {
        $qb->getQuery()->useResultCache(true, self::CACHE_TIME)->useQueryCache(true);
    }

    /**
     * @param QueryBuilder $qb
     * @param User $user
     * @param int $site
     *
     * @return QueryBuilder
     */
    public function addUserPriceAndSiteJoinToQB(QueryBuilder $qb, User $user, int $site) : QueryBuilder
    {
        return $qb
            ->innerJoin('price.users', 'u')
            ->innerJoin('price.priceType', 'price_type')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->andWhere('price_type.site = :site')
            ->setParameter('site', $site);
    }

    /**
     * Checks that each value of $values that key in $keysIn
     *
     * @param array $values
     * @param array $keysIn
     *
     * @throws InvalidArgumentException
     */
    protected function checkEachValueIsKeyAndThrow(array $values, array $keysIn): void
    {
        foreach ($values as $requiredKey) {
            if (!isset($keysIn[$requiredKey])) {
                throw new InvalidArgumentException('undefined index <![CDATA[' . $requiredKey . ']]>');
            }
        }
    }

    /**
     * Checks that item belongs to list, otherwise throws exception
     *
     * @param string|int|bool $value
     * @param array           $allowedList
     *
     * @return void
     * @throws InvalidArgumentException
     */
    protected function checkValueInAllowedListAndThrow($value, array $allowedList)
    {
        foreach ($allowedList as $standardItem) {
            if ($value === $standardItem) {
                return;
            }
        }

        throw new \InvalidArgumentException(
            sprintf(
                'unknown value: <![CDATA[%s]]>, must be one of these <![CDATA[%s]]>',
                $value,
                implode(', ', $allowedList)
            )
        );
    }
}

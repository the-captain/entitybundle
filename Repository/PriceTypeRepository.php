<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * PriceTypeRepository
 */
class PriceTypeRepository extends BaseRepository implements AdminQueryInterface
{
    /**
     * Gets User's PriceTypes Query
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserPriceTypesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.users', 'u')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $user->getId());
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets User's PriceTypes Query
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserPriceTypesSellQuery(User $user)
    {
        return $this->getUserPriceTypesQueryBySiteType($user, PriceType::SALE);
    }

    /**
     * Gets User's PriceTypes Query by site
     *
     * @param User $user
     * @param int $siteType
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserPriceTypesQueryBySiteType(User $user, int $siteType)
    {
        $qb = $this->getUserPriceTypesQuery($user)
            ->andWhere('p.site = :site')
            ->setParameter('site', $siteType);
        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets User's PriceTypes for Repair and Buyback Query
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserPriceTypesBuybackAndRepairQuery(User $user)
    {
        $qb = $this->getUserPriceTypesQuery($user)
            ->andWhere('p.site IN (:sites)')
            ->setParameter('sites', [PriceType::BUYBACK, PriceType::REPAIR]);

        $this->useResultCacheOnQuery($qb);

        return $qb;
    }

    /**
     * Gets User's PriceTypes for Repair and Buyback
     *
     * @param User $user
     * @return PriceType[]
     */
    public function getUserPriceTypesBuybackAndRepair(User $user)
    {
        return $this->getUserPriceTypesBuybackAndRepairQuery($user)
            ->getQuery()->getResult();
    }

    /**
     * Gets User's Categories Models price types
     *
     * @param User $user
     * @param Category $category
     *
     * @return array
     */
    public function getUserCategoryModelsPriceTypes(User $user, Category $category)
    {
        $qb = $this->createQueryBuilder('price_type')
            ->innerJoin('price_type.inventoryItems', 'ii')
            ->innerJoin('ii.model', 'm');
        $qb = $this->addEntityOwnerConditionToQB($qb, $user);
        $qb = $this->addCategoryJoinToQB($qb, $category);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $userIds
     * @param array $siteTypes
     *
     * @return array
     */
    public function getUsersOwnedPriceTypesBuybackAndRepair(array $userIds, array $siteTypes)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->setParameter('user_ids', $userIds)
            ->andWhere('p.site IN (:sites)')
            ->setParameter('sites', $siteTypes);

        $this->useResultCacheOnQuery($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdminQuery(array $params = []): QueryBuilder
    {
        $this->checkEachValueIsKeyAndThrow($this->getParamsList(), $params);

        $userId = [
            $params[AdminQueryInterface::PARAM_OWNER_USER_ID],
            $params[AdminQueryInterface::PARAM_ADMIN_USER_ID],
        ];

        $sites = $params[AdminQueryInterface::PARAM_SITES];

        $qb = $this->createQueryBuilder('pt');

        $qb->addSelect('u')
            ->innerJoin('pt.users', 'u')
            ->where('u.id IN (:user_ids)')
            ->andWhere('pt.site in (:sites)')
            ->setParameter('user_ids', $userId)
            ->setParameter('sites', $sites);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsList(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID,
            AdminQueryInterface::PARAM_OWNER_USER_ID,
            AdminQueryInterface::PARAM_SITES,
        ];
    }
}

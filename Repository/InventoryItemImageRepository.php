<?php

namespace ThreeWebOneEntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * InventoryItemImageRepository
 */
class InventoryItemImageRepository extends ImageRepository
{
    public function getRelated(InventoryItem $item)
    {
        $qb = $this->createQueryBuilder('iii');
        $qb->innerJoin('iii.entities', 'e')
            ->where('e.id = :id')
            ->setParameter('id', $item->getId());

        return $qb->getQuery()->getResult();
    }
}
